/* gael-loader.c
 * Copyright (C) 2001, 2002 Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string.h>

#include "gael-loader.h"
#include "gael-file-loader.h"
#include "gael-cvs-loader.h"
#include "gael-db-loader.h"
#include "util/gael-intl.h"

/* properties */
enum {
  PROP_NONE,
  PROP_LAST
};

static void gael_loader_init         (GaelLoader *lib);
static void gael_loader_class_init   (GaelLoaderClass *klass);

static void gael_loader_set_property (GObject *object,
				      guint property_id,
				      const GValue *value,
				      GParamSpec *pspec);
static void gael_loader_get_property (GObject *object,
				      guint property_id,
				      GValue *value,
				      GParamSpec *pspec);

static GObjectClass *parent_class = NULL;

GType
gael_loader_get_type (void)
{
  static GtkType gael_loader_type = 0;

  if (!gael_loader_type)
    {
      static const GTypeInfo gael_loader_info =
      {
        sizeof (GaelLoaderClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) gael_loader_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,
        
        sizeof (GaelLoader),
        0, /* n_preallocs */
        (GInstanceInitFunc) gael_loader_init,
      };

      gael_loader_type = g_type_register_static (G_TYPE_OBJECT,
						 "GaelLoader",
						 &gael_loader_info,
						 0);
    }
  return gael_loader_type;
}

static void
gael_loader_class_init (GaelLoaderClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS(klass);
  parent_class = g_type_class_ref(G_TYPE_OBJECT);

  object_class->set_property = gael_loader_set_property;
  object_class->get_property = gael_loader_get_property;
}

static void
gael_loader_init (GaelLoader *lib)
{
}

static void
gael_loader_set_property (GObject *object,
			  guint property_id,
			  const GValue *value,
			  GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
gael_loader_get_property (GObject *object,
			  guint property_id,
			  GValue *value,
			  GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

/**
 * gael_loader_load_library:
 * @path: the complete access path.
 * @name: the library name.
 *
 * This function buffers a library from
 * the path and the name. It relays the
 * processing to the correct access fonction.
 *
 * Return value: the buffered library
 */
guchar *
gael_loader_load_library (guchar *path,
			  guchar *name)
{

  if( !strncmp(path,"file:",5) ){
    path += 5;
    return gael_file_loader_library(path,name);
  }

  if( !strncmp(path,"cvs:",4) ){
    path += 4;
    return NULL;
  }

  if( !strncmp(path,"database:",9) ){
    path += 9;
    return NULL;
  }

  g_message("Load aborted: missing acces type [file:|cvs:|database:]");
  return NULL;
}

/**
 * gael_loader_load_element:
 * @libpath: the library path.
 * @path: the element path within the library.
 * @name: the element name.
 *
 * This function buffers a library element
 * from the library path, the element path and
 * name. It relays the processing to the correct
 * access fonction.
 *
 * Return value: the buffered element
 */
guchar *
gael_loader_load_element (guchar *libpath,
			  guchar *path,
			  guchar *name)
{
  if( !strncmp(libpath,"file:",5) ){
    libpath += 5;
    return gael_file_loader_element(libpath,path,name);
  }

  if( !strncmp(libpath,"cvs:",4) ){
    libpath += 4;
    return NULL;
  }

  if( !strncmp(libpath,"database:",9) ){
    libpath += 9;
    return NULL;
  }

  g_message("Load aborted: missing acces type [file:|cvs:|database:]");
  return NULL;
}
