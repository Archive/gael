/* gael-file-loader.h
 * Copyright (C) 2001, 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GAEL_FILE_LOADER_H__
#define __GAEL_FILE_LOADER_H__

#include <gtk/gtk.h>

#define GAEL_TYPE_FILE_LOADER            (gael_file_loader_get_type ())
#define GAEL_FILE_LOADER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GAEL_TYPE_FILE_LOADER, GaelFileLoader))
#define GAEL_FILE_LOADER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GAEL_TYPE_FILE_LOADER, GaelFileLoaderClass))
#define GAEL_IS_FILE_LOADER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GAEL_TYPE_FILE_LOADER))
#define GAEL_IS_FILE_LOADER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj),    GAEL_TYPE_FILE_LOADER))
#define GAEL_FILE_LOADER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),   GAEL_TYPE_FILE_LOADER, GaelFileLoaderClass))

typedef struct _GaelFileLoader      GaelFileLoader;
typedef struct _GaelFileLoaderClass GaelFileLoaderClass;

struct _GaelFileLoader
{
  GObject object;
};

struct _GaelFileLoaderClass
{
  GObjectClass parent_class;
};

GType gael_file_loader_get_type (void);

guchar *gael_file_loader_library (guchar *path,
				  guchar *name);
guchar *gael_file_loader_element (guchar *libpath,
				  guchar *path,
				  guchar *name);

#endif
