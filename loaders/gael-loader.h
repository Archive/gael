/* gael-loader.h
 * Copyright (C) 2001, 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GAEL_LOADER_H__
#define __GAEL_LOADER_H__

#include <gtk/gtk.h>

#define GAEL_TYPE_LOADER            (gael_loader_get_type ())
#define GAEL_LOADER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GAEL_TYPE_LOADER, GaelLoader))
#define GAEL_LOADER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GAEL_TYPE_LOADER, GaelLoaderClass))
#define GAEL_IS_LOADER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GAEL_TYPE_LOADER))
#define GAEL_IS_LOADER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj),    GAEL_TYPE_LOADER))
#define GAEL_LOADER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),   GAEL_TYPE_LOADER, GaelLoaderClass))

typedef struct _GaelLoader      GaelLoader;
typedef struct _GaelLoaderClass GaelLoaderClass;

struct _GaelLoader
{
  GObject object;
};

struct _GaelLoaderClass
{
  GObjectClass parent_class;
};

GType gael_loader_get_type (void);

guchar *gael_loader_load_library (guchar *path,
				  guchar *name);
guchar *gael_loader_load_element (guchar *libpath,
				  guchar *path,
				  guchar *name);

#endif
