/* gael-db-loader.h
 * Copyright (C) 2001, 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GAEL_DB_LOADER_H__
#define __GAEL_DB_LOADER_H__

#include <gtk/gtk.h>

#define GAEL_TYPE_DB_LOADER            (gael_db_loader_get_type ())
#define GAEL_DB_LOADER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GAEL_TYPE_DB_LOADER, GaelDbLoader))
#define GAEL_DB_LOADER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GAEL_TYPE_DB_LOADER, GaelDbLoaderClass))
#define GAEL_IS_DB_LOADER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GAEL_TYPE_DB_LOADER))
#define GAEL_IS_DB_LOADER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj),    GAEL_TYPE_DB_LOADER))
#define GAEL_DB_LOADER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),   GAEL_TYPE_DB_LOADER, GaelDbLoaderClass))

typedef struct _GaelDbLoader      GaelDbLoader;
typedef struct _GaelDbLoaderClass GaelDbLoaderClass;

struct _GaelDbLoader
{
  GObject object;
};

struct _GaelDbLoaderClass
{
  GObjectClass parent_class;
};

GType gael_db_loader_get_type (void);

#endif
