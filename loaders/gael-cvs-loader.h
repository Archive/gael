/* gael-cvs-loader.h
 * Copyright (C) 2001, 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GAEL_CVS_LOADER_H__
#define __GAEL_CVS_LOADER_H__

#include <gtk/gtk.h>

#define GAEL_TYPE_CVS_LOADER            (gael_cvs_loader_get_type ())
#define GAEL_CVS_LOADER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GAEL_TYPE_CVS_LOADER, GaelCvsLoader))
#define GAEL_CVS_LOADER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GAEL_TYPE_CVS_LOADER, GaelCvsLoaderClass))
#define GAEL_IS_CVS_LOADER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GAEL_TYPE_CVS_LOADER))
#define GAEL_IS_CVS_LOADER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj),    GAEL_TYPE_CVS_LOADER))
#define GAEL_CVS_LOADER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),   GAEL_TYPE_CVS_LOADER, GaelCvsLoaderClass))

typedef struct _GaelCvsLoader      GaelCvsLoader;
typedef struct _GaelCvsLoaderClass GaelCvsLoaderClass;

struct _GaelCvsLoader
{
  GObject object;
};

struct _GaelCvsLoaderClass
{
  GObjectClass parent_class;
};

GType gael_cvs_loader_get_type (void);

#endif
