/* gael-db-loader.c
 * Copyright (C) 2001, 2002 Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "gael-db-loader.h"
#include "util/gael-intl.h"

/* properties */
enum {
  PROP_NONE,
  PROP_LAST
};

static void gael_db_loader_init         (GaelDbLoader *loader);
static void gael_db_loader_class_init   (GaelDbLoaderClass *klass);

static void gael_db_loader_set_property (GObject *object,
					 guint property_id,
					 const GValue *value,
					 GParamSpec *pspec);
static void gael_db_loader_get_property (GObject *object,
					 guint property_id,
					 GValue *value,
					 GParamSpec *pspec);

static GObjectClass *parent_class = NULL;

GType
gael_db_loader_get_type (void)
{
  static GtkType gael_db_loader_type = 0;

  if (!gael_db_loader_type)
    {
      static const GTypeInfo gael_db_loader_info =
      {
        sizeof (GaelDbLoaderClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) gael_db_loader_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,
        
        sizeof (GaelDbLoader),
        0, /* n_preallocs */
        (GInstanceInitFunc) gael_db_loader_init,
      };

      gael_db_loader_type = g_type_register_static (G_TYPE_OBJECT,
						    "GaelDbLoader",
						    &gael_db_loader_info,
						    0);
    }
  return gael_db_loader_type;
}

static void
gael_db_loader_class_init (GaelDbLoaderClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS(klass);
  parent_class = g_type_class_ref(G_TYPE_OBJECT);

  object_class->set_property = gael_db_loader_set_property;
  object_class->get_property = gael_db_loader_get_property;
}

static void
gael_db_loader_init (GaelDbLoader *loader)
{
}

static void
gael_db_loader_set_property (GObject *object,
			     guint property_id,
			     const GValue *value,
			     GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
gael_db_loader_get_property (GObject *object,
			     guint property_id,
			     GValue *value,
			     GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}
