#!/usr/bin/env python
#
# Copyright (C) 2002 Xavier Ordoquy <mcarkan@users.sourceforge.net>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.
#

import sys, string
import gtk
import gconf

#
# Library DTD
#
# <gael_library_conf>
#   <library name=".." path=".."/>
# </gael_library_conf>
#

## class gael_lib:
##     def __init__(self):
##         self.libraries = {}
##         self.load("lib.xml")
##
##     def load(self,file):
##         #ctxt = libxml2.createFileParserCtxt(file)
##         #ctxt.validate(1)
##         #ctxt.parseDocument()
##         #doc = ctxt.doc()
##         #valid = ctxt.isValid()
##         #print valid
##         #if not valid:
##         #    print "NOT VALID !!!"
##         #    return
##
##         print "TODO: XML/DTD validation test"
##         doc = libxml2.parseFile(file)
##         node = doc.children
##         node = node.children
##         while node != None and node.name == 'text':
##             node = node.next
##         node = node.children
##         while node != None:
##             if node.name == 'text':
##                 node = node.next
##                 continue
##             if node.name == 'file':
##                 name = node.prop('name')
##                 if self.libraries.has_key(name):
##                     print 'WARNING: library %s is already loaded. Overiding it !' % (name)
##                 self.libraries[name] = node.prop('path')
##
##             node = node.next
##         doc.freeDoc()
##
##     def debug(self):
##         print self.libraries

def change_notify(client, cnxn_id, entry, libs):
    # Note that value can be None (unset) or it can have
    # the wrong type! Need to check that to survive
    # gconftool --break-key
    
    #if not entry.value:
    #    label.set_text ('')
    #elif entry.value.type == gconf.VALUE_STRING:
    #    label.set_text (entry.value.to_string ())
    #else:
    #    label.set_text ('!type error!')
    libs.reload()


class GaelLib:
    def __init__(self):
        self.libraries = []
        self.client = gconf.client_get_default ()
        self.client.add_dir ('/apps/gael/libraries',
                             gconf.CLIENT_PRELOAD_NONE)
        libraries = self.client.all_dirs('/apps/gael/libraries')

        self.notify_id = self.client.notify_add('/apps/gael/libraries',
                                                change_notify,
                                                self)

	#self.debug()
        for lib in libraries:
            lib = string.split(lib,'/')
            self.libraries.append(lib[-1])

    def libs(self):
        return self.libraries

    def reload(self):
        libraries = self.client.all_dirs('/apps/gael/libraries')

    def debug(self):
        print 'DEBUG'
        for lib in self.libraries:
            print '  +',lib
            keypath = '/apps/gael/libraries/'+lib+'/'
            type = self.client.get_string(keypath+'type')
            print '    * type:',type

if __name__ == '__main__':
    lib = GaelLib()
    lib.debug()
    #gtk.main()
