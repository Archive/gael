#!/usr/bin/env python
#
# Copyright (C) 2002 Xavier Ordoquy <mcarkan@users.sourceforge.net>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.
#

import sys
import pygtk
pygtk.require('2.0')
import gtk
import bonobo
import bonobo.ui
import gnome
import gnome.canvas
import gnome.ui
import gtk.glade

import schematic
import gael_lib_view
import gael_lib

VERSION = '0.2.0'

MAIN_WINDOW_UI = 'GNOME_Gael_MainWindow.ui'

SHARE_PATH = '/opt/gnome2.2/share'

# Keep a list of all open application windows
app_list = []


def on_file_new (uic, verbname, win):
    gael = gael_new ()
    gael.show_all ()

def on_file_open (uic, verbname, win):
    print 'on_file_open'
def on_file_save (uic, verbname, win):
    print 'on_file_save'

def on_file_saveas (uic, verbname, win):
    print 'on_file_saveas'

def on_file_properties (uic, verbname, win):
    print 'on_file_properties'

def on_file_print_setup (uic, verbname, win):
    print 'on_file_print_setup'

def on_file_print (uic, verbname, win):
    win.views['schematic'].print_()
    print '***** think about finding the active view with bonobo !!!!'

def on_file_print_preview (uic, verbname, win):
    print 'on_file_print_preview'

def on_file_exit (uic, verbname, win):
    sys.exit (0)

def on_file_close (uic, verbname, win):
    app_list.remove (win)
    if not app_list:
	on_file_exit (uic, verbname, win)
    else:
        win.destroy ()

def on_edit_undo (uic, verbname, win):
    print 'on_edit_undo'

def on_edit_redo (uic, verbname, win):
    print 'on_edit_redo'

def on_edit_libs (uic, verbname, win):
    viewer = gael_lib_view.GaelLibViewer()

def on_edit_project_props (uic, verbname, win):
    print 'on_edit_project_props'

def on_help_about (uic, verbname, win):
    about = gnome.ui.About(name='Gael',
                           version=VERSION,
                           copyright='(C) 2002-2003 Xavier Ordoquy',
                           comments='An electronic design application for the GNOME desktop',
                           authors=['Xavier Ordoquy <xordoquy@users.sourceforge.net>'])
    about.show()


# These verb names are standard, see libonobobui/doc/std-ui.xml
# to find a list of standard verb names.
# The menu items are specified in Bonobo_Sample_Hello.xml and
# given names which map to these verbs here.

gael_verbs = [
    ('FileNew',          on_file_new),
    ('FileOpen',         on_file_open),
    ('FileSave',         on_file_save),
    ('FileSaveAs',       on_file_saveas),
    ('FilePrintSetup',   on_file_print_setup),
    ('FilePrint',        on_file_print),
    ('FilePrintPreview', on_file_print_preview),
    ('FileClose',        on_file_close),
    ('FileExit',         on_file_exit),

    ('EditUndo',         on_edit_undo),
    ('EditRedo',         on_edit_redo),
    ('EditProjectProps', on_edit_project_props),
    ('EditLibs',         on_edit_libs),

    ('HelpAbout',        on_help_about)
]

def gael_create_main_window ():
    window = bonobo.ui.Window ('gael', 'gael')

    ui_container = window.get_ui_container ()
    ui_component = bonobo.ui.Component ('gael-ui')
    ui_component.set_container (ui_container.corba_objref ())

    bonobo.ui.util_set_ui (ui_component, SHARE_PATH,
                           'GNOME_Gael_MainWindow.ui',
		           'gael')
    ui_component.add_verb_list (gael_verbs, window)

##     widget = bonobo.ui.Widget ("OAFIID:GNOME_Development_GlimmerFile", ui_container.corba_objref ())
##     frame = widget.get_control_frame()

##     window.set_contents(widget)
##     widget.show_all()
##     frame.control_activate()
##     window.set_default_size(500,500)

    engine = window.get_ui_engine ()
    engine.config_set_path ('/gael/UIConfig/kvps')

    window.connect ('delete_event', delete_event_cb)
    window.show_all ()

    return window


def delete_event_cb (window, event):
    app_list.remove (window)
    window.destroy ()
    if not app_list:
        sys.exit(0)
    return gtk.FALSE


def gael_new ():
    win = gael_create_main_window ()
    win.views = {}
    view = schematic.schematic()
    win.set_contents (view)
    win.views['schematic'] = view
    
    app_list.append (win)
    view.activate(win.get_ui_container ())

    win.libs = gael_lib.GaelLib()
    
    return win


if __name__ == '__main__':
    app = gael_new ()
    app.show_all ()
    bonobo.main ()
