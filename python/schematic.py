#!/usr/bin/env python
#
# Copyright (C) 2002 Xavier Ordoquy <mcarkan@users.sourceforge.net>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.
#

import sys
import gtk
import bonobo
import bonobo.ui
import gnome
import gnome.canvas
import gnome.ui
import gnomeprint
import gnomeprint.ui
import schem_wire

SHARE_PATH = '/opt/gnome2.2/share'

def ZoomToFit (uic, verbname, win):
    print 'ZoomToFit'

def ZoomIn (uic, verbname, win):
    win.zoom = win.zoom*1.2
    win.canvas.set_pixels_per_unit(win.zoom)

def ZoomOut (uic, verbname, win):
    win.zoom = win.zoom/1.2
    win.canvas.set_pixels_per_unit(win.zoom)

def Zoom_100 (uic, verbname, win):
    win.zoom = 1.0
    win.canvas.set_pixels_per_unit(win.zoom)

def Component (uic, verbname, win):
    print 'Component'

def Wire (uic, verbname, win):
    if win.canvas_mode == 0:
        win.canvas_mode = 1
        win.mouse_cursor = gtk.gdk.Cursor(gtk.gdk.CROSSHAIR)
        win.canvas.window.set_cursor(win.mouse_cursor)



gael_schematic_verbs = [
    ('ZoomToFit', ZoomToFit),
    ('ZoomIn',    ZoomIn),
    ('ZoomOut',   ZoomOut),
    ('Zoom 100',  Zoom_100),
    ('Component', Component),
    ('Wire',      Wire)
]


class schematic(gtk.Table):

    # special canvas state (component placement, wire placement, ...)
    # 0 = default mode
    # 1 = 1st wire point
    # 2 = 2nd and more wire point
    canvas_mode  = 0

    def __init__(self,*args):
        gtk.Table.__init__(self,2,2)
        self.ui_comp = bonobo.ui.Component ('schematic-ui')
        self.grid_spacing  = 50.0
        self.current_item  = None
        self.mouse_cursor  = None
        self.wires = []

        # canvas
        self.zoom   = 1.0
        self.canvas = gnome.canvas.Canvas()
        self.canvas.set_size_request(640,400)
        self.canvas.set_scroll_region(0,0,4096,4096)
        self.attach(self.canvas,
                    0,1,0,1,
                    gtk.EXPAND | gtk.FILL | gtk.SHRINK,
                    gtk.EXPAND | gtk.FILL | gtk.SHRINK,
                    0, 0)
        self.canvas.set_flags(gtk.CAN_FOCUS)
        #self.canvas.grab-focus()
        self.canvas.connect('motion_notify_event',  self.motion_move)
        self.canvas.connect('button_press_event',   self.button_press)

        # scrolls
        self.hscroll = gtk.HScrollbar(self.canvas.get_property('hadjustment'))
        self.attach(self.hscroll,
                    0,1,1,2,
                    gtk.FILL, gtk.FILL,
                    0, 0)
        self.vscroll = gtk.VScrollbar(self.canvas.get_property('vadjustment'))
        self.attach(self.vscroll,
                    1,2,0,1,
                    gtk.FILL, gtk.FILL,
                    0, 0)

        # cursor
        self.R=4.0
        self.cursor = self.canvas.root().add('GnomeCanvasEllipse',
                                             x1=self.R, y1=self.R, x2=-self.R, y2=-self.R,
                                             outline_color='black',
                                             width_units=1.0)

    def activate(self, ui_container):
        self.ui_comp.set_container (ui_container.corba_objref ())
        self.ui_comp.freeze()
        bonobo.ui.util_set_ui (self.ui_comp, SHARE_PATH,
                               'GNOME_Gael_SchematicView.ui',
                               'schematic')
        self.ui_comp.add_verb_list (gael_schematic_verbs, self)
        self.ui_comp.thaw()

    def deactivate(self, ui_container):
        self.ui_comp.rm('/')
        self.ui_comp.unset_container()

    def motion_move(self, widget, event=None):
        if event == None or event.is_hint:
            return gtk.TRUE
        x, y = self.cursor.w2i(event.x,event.y)
        grid_spacing = self.grid_spacing
        x = round(x/self.zoom/grid_spacing)*grid_spacing
        y = round(y/self.zoom/grid_spacing)*grid_spacing

        if self.canvas_mode == 0:
            pass
        elif self.canvas_mode == 1:
            pass
        elif self.canvas_mode == 2:
            self.current_item.motion_move(x,y)
        else:
            pass

        R = self.R
        self.cursor.set_property('x1',x+R)
        self.cursor.set_property('y1',y+R)
        self.cursor.set_property('x2',x-R)
        self.cursor.set_property('y2',y-R)
        return gtk.TRUE

    def button_press(self, widget, event):

        # Are we in 1st point of a wire ?
        if self.canvas_mode == 1:
            # Get the cursor in the canvas coordinates after grid effect
            x, y = self.cursor.w2i(event.x,event.y)
            grid_spacing = self.grid_spacing
            x = round(x/self.zoom/grid_spacing)*grid_spacing
            y = round(y/self.zoom/grid_spacing)*grid_spacing

            best_wire = None
            for wire in self.wires:
                if wire.is_on_wire(x,y) != []:
                    best_wire = wire
                    break

            if best_wire == None:
                self.current_item = schem_wire.wire(self.canvas.root())
                self.wires.append(self.current_item)
                self.current_item.mouse_click(x,y)
            else:
                self.current_item = best_wire
                self.current_item.mouse_click(x,y)

            self.canvas_mode = 2
            return gtk.TRUE

        elif self.canvas_mode == 2:
            # notice the change
            x, y = self.cursor.w2i(event.x,event.y)
            grid_spacing = self.grid_spacing
            x = round(x/self.zoom/grid_spacing)*grid_spacing
            y = round(y/self.zoom/grid_spacing)*grid_spacing
            if self.current_item.mouse_click(x,y) == gtk.FALSE:
                # if it's at the same location, clean the current things
                self.canvas_mode = 0
                self.current_item = None
                self.mouse_cursor = None
                self.canvas.window.set_cursor(None)
            else:
                # Need to check whether or not it connects two different wires
                best_wire = None
                for wire in self.wires:
                    if wire != self.current_item and wire.is_on_wire(x,y) != []:
                        best_wire = wire
                        break
                if best_wire != None:
                    for i in range(len(self.wires)):
                        if self.wires[i] == best_wire:
                            del self.wires[i]
                            break
                    self.current_item.merge(best_wire)
            return gtk.TRUE

        return gtk.FALSE

    def print_(self):
        config = gnomeprint.config_default()
        config.set('Printer','GENERIC')
        job = gnomeprint.Job(config)
        job.print_to_file('output.ps')
        context = job.get_context()
        context.beginpage('1')
        for wire in self.wires:
            wire.print_(context)
        context.showpage()
        job.close()
        job.print_()
