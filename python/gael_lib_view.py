#!/usr/bin/env python2.2
#
# Copyright (C) 2002 Xavier Ordoquy <mcarkan@users.sourceforge.net>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.
#

import sys, types, new, string
import gobject
import gtk
import bonobo
import bonobo.ui
import gnome
import gnome.canvas
import gnome.ui
import gtk.glade
import gael_lib
import gconf

TITLE_COLUMN = 0
TYPE_COLUMN  = 1

class GaelLibConf:
    def __init__(self,lib_viewer,name=None):
        self.lib_viewer = lib_viewer
        self.name = name

        # setting up the UI
        self.xml = gtk.glade.XML('gael.glade2','LibConfWin')
        class_methods = self.__class__.__dict__
        callbacks = {}
        for method_name in class_methods.keys():
            method = class_methods[method_name]
            if type(method) == types.FunctionType:
                callbacks[method_name] = new.instancemethod(method, self, self.__class__)
        self.xml.signal_autoconnect(callbacks)

        sizegroup = gtk.SizeGroup(gtk.SIZE_GROUP_HORIZONTAL)
        for i in range(1,6):
            sizegroup.add_widget(self.xml.get_widget('LibConfLabel'+str(i)))

        # gconf
        self.client = gconf.client_get_default ()
        self.client.add_dir ('/apps/gael', gconf.CLIENT_PRELOAD_NONE)

        # filing the fields
        self.old_name = None
        if name != None:
            self.old_name = name
            self.xml.get_widget('LibConfName').set_text(name)
            lib_type = self.client.get_string('/apps/gael/libraries/'+name+'/type')
            libconftype = self.xml.get_widget('LibConfType')
            menu = libconftype.get_menu()
            libconftype.remove_menu()
            if lib_type == 'file':
                filepath = self.client.get_string('/apps/gael/libraries/'+name+'/path')
                self.xml.get_widget('LibConfFileLocation').set_text(filepath)
                menu.set_active(0)
                self.xml.get_widget('LibConfNotebook').set_current_page(0)
            elif lib_type == 'tarball':
                filepath = self.client.get_string('/apps/gael/libraries/'+name+'/path')
                self.xml.get_widget('LibConfTarballLocation').set_text(filepath)
                menu.set_active(1)
                self.xml.get_widget('LibConfNotebook').set_current_page(1)
            elif lib_type == 'database':
                print 'database'
                menu.set_active(2)
                self.xml.get_widget('LibConfNotebook').set_current_page(2)
            else:
                print 'Unknown type of file: %s' % (lib_type)
            libconftype.set_menu(menu)


    def OK_clicked(self,button):
        # get the library name
        name = self.xml.get_widget('LibConfName').get_text()
        # get the type of the library
        menu = self.xml.get_widget('LibConfType').get_menu()
        selected = menu.get_active()
        children = menu.get_children()
        selected_nr = -1
        for i in range(len(children)):
            if children[i] == selected:
                selected_nr = i

        # if the library existed before, clean it
        # Note: this could be fine tuned later to unset only if needed
        if self.old_name != None:
            for entry in self.client.all_entries('/apps/gael/libraries/'+self.old_name):
                self.client.unset(entry.get_key())
            self.client.remove_dir('/apps/gael/libraries/'+self.old_name)

        # save the library
        if selected_nr == 0:
            self.client.set_string('/apps/gael/libraries/'+name+'/type','file')
            self.client.set_string('/apps/gael/libraries/'+name+'/path',self.xml.get_widget('LibConfFileLocation').get_text())
        elif selected_nr == 1:
            self.client.set_string('/apps/gael/libraries/'+name+'/type','tarball')
            self.client.set_string('/apps/gael/libraries/'+name+'/path',self.xml.get_widget('LibConfTarballLocation').get_text())
        elif selected_nr == 2:
            self.client.set_string('/apps/gael/libraries/'+name+'/type','database')
            self.client.set_string('/apps/gael/libraries/'+name+'/url',self.xml.get_widget('LibConfDatabaseURL').get_text())
            self.client.set_string('/apps/gael/libraries/'+name+'/basename',self.xml.get_widget('LibConfDatabaseName').get_text())
            self.client.set_string('/apps/gael/libraries/'+name+'/basetype','MySQL')
        else:
            print 'ERROR'
        self.xml.get_widget('LibConfWin').destroy()
        del self

    def Cancel_clicked(self,button):
        self.xml.get_widget('LibConfWin').destroy()
        del self

    def on_file1_activate(self,menuitem):
        self.xml.get_widget('LibConfNotebook').set_current_page(0)

    def on_tarball1_activate(self,menuitem):
        self.xml.get_widget('LibConfNotebook').set_current_page(1)

    def on_database1_activate(self,menuitem):
        self.xml.get_widget('LibConfNotebook').set_current_page(2)

    def on_postgresql1_activate(self,menuitem):
        print 'on_postgresql1_activate'

    def on_mysql1_activate(self,menuitem):
        print 'on_mysql1_activate'

def change_notify(client, cnxn_id, entry, viewer):
    viewer.reload()

class GaelLibViewer:
    def __init__(self):
        # Setting up gconf client        
        self.libraries = []
        self.client = gconf.client_get_default ()
        self.client.add_dir ('/apps/gael/libraries',
                             gconf.CLIENT_PRELOAD_NONE)
        self.notify_id = self.client.notify_add('/apps/gael/libraries',
                                                change_notify,
                                                self)

        # Create the widget from glade
        self.xml = gtk.glade.XML('gael.glade2','LibViewWin')
        class_methods = self.__class__.__dict__
        callbacks = {}
        for method_name in class_methods.keys():
            method = class_methods[method_name]
            if type(method) == types.FunctionType:
                callbacks[method_name] = new.instancemethod(method, self, self.__class__)
        #print callbacks
        self.xml.signal_autoconnect(callbacks)

        # Now care about the list
        model = gtk.ListStore(gobject.TYPE_STRING, gobject.TYPE_STRING)
        tree_view = self.xml.get_widget('LibViewList')
        tree_view.set_model(model)
        selection = tree_view.get_selection()
        selection.set_mode('single')
        #tree_view.set_size_request(200, -1)

        cell = gtk.CellRendererText()
        column = gtk.TreeViewColumn('Library', cell, text=TITLE_COLUMN)
        tree_view.append_column(column)
        #cell = gtk.CellRendererText()
        #column = gtk.TreeViewColumn('type', cell, text=TYPE_COLUMN)
        #tree_view.append_column(column)
        self.model = model
        self.reload()

    def reload(self):
        # Cleans the list
        while self.model.get_iter_first() != None:
            self.model.remove(self.model.get_iter_first())
        # Read all the available libraries from gconf client and
        # enter them in the tree
        self.libraries = []
        libraries = self.client.all_dirs('/apps/gael/libraries')
        for lib in libraries:
            lib = string.split(lib,'/')[-1]
            iter = self.model.append()
            self.model.set_value(iter, TITLE_COLUMN, lib)
            #model.set_value(iter, TYPE_COLUMN,  'type')        

    def help_clicked(self,something):
        print 'help!!!'

    def new_clicked(self,something):
        GaelLibConf(self)

    def edit_clicked(self,something):
        viewlist = self.xml.get_widget('LibViewList')
        selection = viewlist.get_selection()
        iter = selection.get_selected()[-1]
        if iter == None:
            return
        model = viewlist.get_model()
        name = model.get_value(iter,TITLE_COLUMN)
        GaelLibConf(self,name)

    def delete_clicked(self,something):
        print 'delete!!!',self,something

    def close_clicked(self,something):
        self.xml.get_widget('LibViewWin').destroy()
        del self
