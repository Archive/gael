#!/usr/bin/env python
#
# Copyright (C) 2002 Xavier Ordoquy <mcarkan@users.sourceforge.net>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.
#


import sys
import gtk
import bonobo
import bonobo.ui
import gnome
import gnome.canvas
import gnome.ui
import gnomeprint
import gnomeprint.ui

class wire:
    def __init__(self,group):
        self.group = group.add('GnomeCanvasGroup')
        self.segments   = []
        self.points     = []
        self.curr_seg   = None
        self.curr_point = None
        self.R = 5.0

    def debug(self):
        print '========== wire =========='
        print 'Segments:'
        for seg in self.segments:
            points = seg.get_property('points')
            print '  -',points
        print 'Points:'
        for point in self.points:
            x = (point.get_property('x1')+point.get_property('x2'))/2
            y = (point.get_property('y1')+point.get_property('y2'))/2
            print '  - (%f,%f)' % (x,y)
        print '===================='

    def print_(self,context):
        for point in self.points:
            x = (point.get_property('x1'))
            y = (point.get_property('y1'))
            width  = point.get_property('x2') - point.get_property('x1')
            height = point.get_property('y2') - point.get_property('y1')
            context.rect_filled(x,y,width,height)
        for seg in self.segments:
            points = seg.get_property('points')
            context.line_stroked(points[0],points[1],points[2],points[3])

    def load(self):
        pass

    def save(self):
        pass

    def merge(self,other):
        for point in other.points:
            x = (point.get_property('x1')+point.get_property('x2'))/2
            y = (point.get_property('y1')+point.get_property('y2'))/2
            self.add_point(x,y)
        for seg in other.segments:
            points = seg.get_property('points')
            self.add_segment(points[0],points[1],points[2],points[3])
        if other.curr_point != None:
            print 'CURRENT POINT'
            x = (point.get_property('x1')+point.get_property('x2'))/2
            y = (point.get_property('y1')+point.get_property('y2'))/2
            self.curr_point = self.add_point(x,y)
            self.curr_seg   = self.group.add('GnomeCanvasLine',
                                             points=(x,y,x,y),
                                             fill_color='black',
                                             width_units=1.0)
            other.curr_point.destroy()
        if other.curr_seg != None:
            other.curr_seg.destroy()
        for point in other.points:
            point.destroy()
        for seg in other.segments:
            seg.destroy()
        del other

    def mouse_click(self,x,y):
	# Get the wires we are over
        segs = self.is_on_wire(x,y)

	# Check wether we are at the beginning of a
	# segment or if it's already started
        if self.curr_seg != None:
            points = self.curr_seg.get_property('points')
            self.curr_seg.destroy()
            self.curr_seg = None
            if self.curr_point != None:
                self.curr_point = None
            if (points[0]==points[2]) and (points[1]==points[3]):
                return gtk.FALSE
            self.add_segment(points[0],points[1],points[2],points[3])
        else:
            self.curr_point = self.add_point(x,y)
        seg = self.group.add('GnomeCanvasLine',
                             points=(x,y,x,y),
                             fill_color='black',
                             width_units=1.0)
        self.curr_seg = seg
        return gtk.TRUE

    def motion_move(self,x,y):
        points = self.curr_seg.get_property('points')
        points[2] = x
        points[3] = y
        self.curr_seg.set_property('points',points)

    def is_on_wire(self,x,y):
        result = []
        for seg in self.segments:
            points = seg.get_property('points')
            # Check (x,y) is on the line
            if ((points[2]-points[0])*(y-points[1])) == \
               ((points[3]-points[1])*(x-points[0])):
                # Now see if it's on the segent
                if points[0] != points[2]:
                    g = (x-points[0])/(points[2]-points[0])
                elif points[1] != points[3]:
                    g = (y-points[1])/(points[3]-points[1])
                else:
                    g = 0
                if (0 <= g) and (g <= 1):
                    result.append(seg)
        return result

    def add_point(self,x,y):
        # If the point already exists, returns it
        for point in self.points:
            pos_x = (point.get_property('x1')+point.get_property('x2'))/2
            pos_y = (point.get_property('y1')+point.get_property('y2'))/2
            if (pos_x == x) and (pos_y == y):
                return point

        # Create the point
        point = self.group.add('GnomeCanvasRect',
                               x1 = x+self.R, y1 = y+self.R,
                               x2 = x-self.R, y2 = y-self.R,
                               fill_color='black',
                               width_units=1.0)
        self.points.append(point)

        # Check that the point isn't on the segment
        wires = self.is_on_wire(x,y)
        # If it is, split the segment it's on on two sub segments
        for wire in wires:
            pos = wire.get_property('points')
            if ((pos[0] != x) or (pos[1] != y)) and ((pos[2] != x) or (pos[3] != y)):
                pos1 = (pos[0],pos[1],x,y)
                pos2 = (x,y,pos[2],pos[3])
                wire.set_property('points',pos1)
                self.segments.append(self.group.add('GnomeCanvasLine',
                                                    points=pos2,
                                                    fill_color='black',
                                                    width_units=1.0))
        return point

    def del_point(self,x,y):
        # Don't forget to merge segments splitted by the point !!!
        pass

    def add_segment(self,x1,y1,x2,y2):
        self.segments.append(self.group.add('GnomeCanvasLine',
                                           points=(x1,y1,x2,y2),
                                           fill_color='black',
                                           width_units=1.0))
        self.add_point(x1,y1)
        self.add_point(x2,y2)
