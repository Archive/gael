#!/usr/bin/env python
#
# Copyright (C) 2002 Xavier Ordoquy <mcarkan@users.sourceforge.net>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.
#


import sys
import gtk
import bonobo
import bonobo.ui
import gnome
import gnome.canvas
import gnome.ui
import libxml2


class schem_symbol:
    def __init__(self,group):
        self.group = group.add('GnomeCanvasGroup')

    def load(self):
        print 'schem_symbol::load'

    def save(self):
        print 'schem_symbol::save'

class lib_schem_symbol:
    def __init__(self,filename):
        self.filename = filename
        self.elements = []

    def load(self):
        doc = libxml2.parseFile(self.filename)
        node = doc.children
        node = node.children
        while node != None:
            if node.name == "text":
                node = node.next
                continue
            if node.name == 'rectangle':
                self.elements.append(('rectangle', node.prop('filled'),
                                      node.prop('left'),  node.prop('top'),
                                      node.prop('right'), node.prop('bottom')))

            if node.name == 'arc':
                self.elements.append(('arc', node.prop('filled'),
                                      node.prop('x'), node.prop('y'),
                                      node.prop('width'), node.prop('height'),
                                      node.prop('angle1'), node.prop('angle2')))

            if node.name == 'line':
                self.elements.append(('line',
                                      node.prop('x1'), node.prop('y1'),
                                      node.prop('x2'), node.prop('y2')))

            if node.name == 'connection':
                self.elements.append(('connection', node.prop('id'),
                                      node.prop('x'), node.prop('y')))

            if node.name == 'property':
                self.elements.append(('property',
                                      node.prop('x'), node.prop('y'),
                                      node.prop('name'), node.prop('value')))

            node = node.next
        doc.freeDoc()

    def debug(self):
        for elem in self.elements:
            if elem[0] == 'rectangle':
                print 'rectangle'
                print '  filled =', elem[1]
                print '  left   =', elem[2]
                print '  top    =', elem[3]
                print '  right  =', elem[4]
                print '  bottom =', elem[5]

            if elem[0] == 'arc':
                print 'arc'
                print '  filled=', elem[1]
                print '  x     =', elem[2]
                print '  y     =', elem[3]
                print '  width =', elem[4]
                print '  height=', elem[5]
                print '  angle1=', elem[6]
                print '  angle2=', elem[7]

            if elem[0] == 'line':
                print 'line'
                print '  x1=', elem[1]
                print '  y1=', elem[2]
                print '  x2=', elem[3]
                print '  y2=', elem[4]

            if elem[0] == 'connection':
                print 'connection'
                print '  id=', elem[1]
                print '  x =', elem[2]
                print '  y =', elem[3]

            if elem[0] == 'property':
                print 'property'
                print '  x     =', elem[1]
                print '  y     =', elem[2]
                print '  name  =', elem[3]
                print '  value =', elem[4]


if __name__ == '__main__':
    symbol = lib_schem_symbol('/opt/gnome2/share/gael/lib/schematic/gluelogic/AND.xml')
    symbol.load()
    symbol.debug()
