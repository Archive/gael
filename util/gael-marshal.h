
#ifndef __gael_marshal_MARSHAL_H__
#define __gael_marshal_MARSHAL_H__

#include	<glib-object.h>

G_BEGIN_DECLS

/* BOOLEAN:VOID (gael-marshal.list:1) */
extern void gael_marshal_BOOLEAN__VOID (GClosure     *closure,
                                        GValue       *return_value,
                                        guint         n_param_values,
                                        const GValue *param_values,
                                        gpointer      invocation_hint,
                                        gpointer      marshal_data);

/* VOID:BOOLEAN (gael-marshal.list:2) */
#define gael_marshal_VOID__BOOLEAN	g_cclosure_marshal_VOID__BOOLEAN

/* VOID:POINTER,POINTER (gael-marshal.list:3) */
extern void gael_marshal_VOID__POINTER_POINTER (GClosure     *closure,
                                                GValue       *return_value,
                                                guint         n_param_values,
                                                const GValue *param_values,
                                                gpointer      invocation_hint,
                                                gpointer      marshal_data);

/* VOID:VOID (gael-marshal.list:4) */
#define gael_marshal_VOID__VOID	g_cclosure_marshal_VOID__VOID

/* BOOLEAN:OBJECT (gael-marshal.list:5) */
extern void gael_marshal_BOOLEAN__OBJECT (GClosure     *closure,
                                          GValue       *return_value,
                                          guint         n_param_values,
                                          const GValue *param_values,
                                          gpointer      invocation_hint,
                                          gpointer      marshal_data);

G_END_DECLS

#endif /* __gael_marshal_MARSHAL_H__ */

