/* gael-librarian-chooser.c
 * Copyright (C) 2001, 2002 Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "gael-librarian.h"
#include "general/gael-general.h"
#include "util/gael-intl.h"

#include <string.h>
#include <math.h>

static void gael_librarian_chooser_init         (GaelLibrarianChooser *chooser);
static void gael_librarian_chooser_class_init   (GaelLibrarianChooserClass *klass);
static void gael_librarian_chooser_finalize     (GObject *object);

static void gael_librarian_chooser_set_property (GObject *object,
						 guint property_id,
						 const GValue *value,
						 GParamSpec *pspec);
static void gael_librarian_chooser_get_property (GObject *object,
						 guint property_id,
						 GValue *value,
						 GParamSpec *pspec);

static void gael_callback_librarian_chooser_OK      (GtkButton *button,
						     gpointer user_data);
static void gael_callback_librarian_chooser_cancel  (GtkButton *button,
						     gpointer user_data);

static void gael_callback_librarian_chooser_show    (GtkWidget *widget,
						     gpointer user_data);

static void gael_callback_librarian_chooser_hide    (GtkWidget *widget,
						     gpointer user_data);

static void gael_librarian_chooser_selection_changed (GtkTreeSelection *treeselection,
						      gpointer user_data);

static gboolean gael_librarian_chooser_expose_drawing_area1 (GtkWidget *widget,
							     GdkEventExpose *event,
							     gpointer user_data);

static gboolean gael_callback_librarian_chooser_delete  (GtkWidget *widget,
							 GdkEvent *event,
							 gpointer user_data);


static GObjectClass *parent_class = NULL;

GType
gael_librarian_chooser_get_type (void)
{
  static GtkType gael_librarian_chooser_type = 0;

  if (!gael_librarian_chooser_type)
    {
      static const GTypeInfo gael_librarian_chooser_info =
      {
        sizeof (GaelLibrarianChooserClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) gael_librarian_chooser_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,
        
        sizeof (GaelLibrarianChooser),
        0, /* n_preallocs */
        (GInstanceInitFunc) gael_librarian_chooser_init,
      };

      gael_librarian_chooser_type =
	g_type_register_static (G_TYPE_OBJECT,
				"GaelLibrarianChooser",
				&gael_librarian_chooser_info,
				0);
    }
  return gael_librarian_chooser_type;
}

static void
gael_librarian_chooser_class_init (GaelLibrarianChooserClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS(klass);
  parent_class = g_type_class_ref(G_TYPE_OBJECT);

  object_class->finalize     = gael_librarian_chooser_finalize;

  object_class->set_property = gael_librarian_chooser_set_property;
  object_class->get_property = gael_librarian_chooser_get_property;
}

static void
gael_librarian_chooser_init (GaelLibrarianChooser *chooser)
{
  GtkWidget         *widget;
  GtkTreeViewColumn *column;
  GtkTreeSelection *selection;

  chooser->xml = glade_xml_new(GAEL_DATADIR "/gael/glade/gael2.glade", "GaelLibrarianChooser", NULL);

  chooser->window = glade_xml_get_widget(chooser->xml,"GaelLibrarianChooser");
  g_signal_connect_object(chooser->window, "delete_event", G_CALLBACK(gael_callback_librarian_chooser_delete), chooser, 0);
  g_signal_connect_object(chooser->window, "show",   G_CALLBACK(gael_callback_librarian_chooser_show), chooser, 0);
  g_signal_connect_object(chooser->window, "hide",   G_CALLBACK(gael_callback_librarian_chooser_hide), chooser, 0);

  widget = glade_xml_get_widget(chooser->xml,"GaelLibrarianChooserOK");
  g_signal_connect_object(widget,"clicked", G_CALLBACK(gael_callback_librarian_chooser_OK), chooser, 0);

  widget = glade_xml_get_widget(chooser->xml,"GaelLibrarianChooserCancel");
  g_signal_connect_object(widget,"clicked", G_CALLBACK(gael_callback_librarian_chooser_cancel), chooser, 0);

  chooser->model = gtk_tree_store_new (4, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, NULL);

  widget = glade_xml_get_widget(chooser->xml,"GaelLibrarianChooserElementList");
  gtk_tree_view_set_model(GTK_TREE_VIEW(widget),GTK_TREE_MODEL(chooser->model));

  column = gtk_tree_view_column_new_with_attributes (NULL, gtk_cell_renderer_text_new(), "text", 0, NULL);
  gtk_tree_view_append_column(GTK_TREE_VIEW(widget), column);

  selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(widget));
  g_signal_connect_object(G_OBJECT(selection), "changed", G_CALLBACK(gael_librarian_chooser_selection_changed), chooser, 0);

  chooser->symbol = NULL;
  widget = glade_xml_get_widget(chooser->xml,"GaelLibrarianChooserDrawingSymbol");
  g_signal_connect_object(widget,"expose_event", G_CALLBACK(gael_librarian_chooser_expose_drawing_area1), chooser, 0);
}

static void
gael_librarian_chooser_finalize (GObject *object)
{
  GaelLibrarianChooser *chooser = GAEL_LIBRARIAN_CHOOSER(object);

  g_object_unref(chooser->xml);
}

static void
gael_librarian_chooser_set_property (GObject *object,
				     guint property_id,
				     const GValue *value,
				     GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
gael_librarian_chooser_get_property (GObject *object,
				     guint property_id,
				     GValue *value,
				     GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
gael_callback_librarian_chooser_OK (GtkButton *button,
				    gpointer user_data)
{
  GtkTreeIter           iter;
  guchar               *name, *lib;
  GtkWidget            *widget;
  GtkTreeSelection     *selection;
  GtkTreeModel         *model;
  GaelLibrarianChooser *chooser;

  chooser = GAEL_LIBRARIAN_CHOOSER(user_data);

  widget = glade_xml_get_widget(chooser->xml,"GaelLibrarianChooserElementList");

  if (! (selection = gtk_tree_view_get_selection (GTK_TREE_VIEW(widget))) ) {
    gtk_widget_hide(GAEL_LIBRARIAN_CHOOSER(user_data)->window);
    return;
  }

  if (! gtk_tree_selection_get_selected (selection, NULL, &iter)) {
    gtk_widget_hide(GAEL_LIBRARIAN_CHOOSER(user_data)->window);
    return;
  }

  model = gtk_tree_view_get_model(GTK_TREE_VIEW(widget));

  gtk_tree_model_get (model, &iter,
		      1, &name,
		      2, &lib,
		      -1);

  if( (lib  != NULL) &&
      (name != NULL) ) {
    g_signal_emit_by_name(chooser->librarian,"comp_selected",lib,name);
  }

  gtk_widget_hide(GAEL_LIBRARIAN_CHOOSER(user_data)->window);
}

static void
gael_callback_librarian_chooser_cancel (GtkButton *button,
					gpointer user_data)
{
  gtk_widget_hide(GAEL_LIBRARIAN_CHOOSER(user_data)->window);
}

static void
gael_callback_librarian_chooser_show (GtkWidget *widget,
				      gpointer user_data)
{
  GaelLibrarianChooser *chooser = GAEL_LIBRARIAN_CHOOSER(user_data);

  g_signal_emit_by_name(chooser->librarian,"chooser_visible",TRUE);
}

static void
gael_callback_librarian_chooser_hide (GtkWidget *widget,
				      gpointer user_data)
{
  GaelLibrarianChooser *chooser = GAEL_LIBRARIAN_CHOOSER(user_data);

  g_signal_emit_by_name(chooser->librarian,"chooser_visible",FALSE);
}

static gboolean
gael_callback_librarian_chooser_delete (GtkWidget *widget,
					GdkEvent *event,
					gpointer user_data)
{
  gtk_widget_hide(GAEL_LIBRARIAN_CHOOSER(user_data)->window);
  return TRUE;
}

static void
_gael_librarian_chooser_add_element (GtkTreeModel *tree_model,
				     GtkTreeIter *base_iter,
				     guchar *library,
				     guchar *fullname,
				     guchar *type,
				     guchar *name)
{
  GtkTreeIter  iter;
  guchar      *start, *end;

  /*
   * Get this nodes' fullname
   */
  if( name == NULL ) {
    return;
  }

  start = name;
  end = strchr(start,'/');
  if( end != NULL ) {
    *end = 0;
    end++;
  }

  if( base_iter != NULL ) {
    if( gtk_tree_model_iter_children (tree_model, &iter, base_iter) ) {
      /*
       * There's already an item.
       */
      guchar *buffer;
      gtk_tree_model_get(tree_model, &iter,
			  0, &buffer, -1);
      while( strcmp(buffer,start) ) {
	if( !gtk_tree_model_iter_next(tree_model, &iter) ) {
	  break;
	}
	gtk_tree_model_get(tree_model, &iter,
			   0, &buffer, -1);
      }
      if( strcmp(buffer,start) ) {
	gtk_tree_store_append (GTK_TREE_STORE(tree_model), &iter, base_iter);
	if( end == NULL ) {
	  gtk_tree_store_set (GTK_TREE_STORE(tree_model), &iter,
			      0, strdup(start),
			      1, strdup(fullname),
			      2, strdup(library),
			      3, strdup(type),
			      -1);
	} else {
	  gtk_tree_store_set (GTK_TREE_STORE(tree_model), &iter,
			      0, strdup(start),
			      1, NULL,
			      2, NULL,
			      3, NULL,
			      -1);
	}
      }
    } else {
      /*
       * There's no item.
       */
      gtk_tree_store_append (GTK_TREE_STORE(tree_model), &iter, base_iter);
      if( end == NULL ) {
	gtk_tree_store_set (GTK_TREE_STORE(tree_model), &iter,
			    0, strdup(start),
			    1, strdup(fullname),
			    2, strdup(library),
			    3, strdup(type),
			    -1);
      } else {
	gtk_tree_store_set (GTK_TREE_STORE(tree_model), &iter,
			    0, strdup(start),
			    1, NULL,
			    2, NULL,
			    3, NULL,
			    -1);
      }
    }

  } else {

    if( gtk_tree_model_get_iter_first (tree_model, &iter) ) {
      /*
       * There's already a root item.
       */
      guchar *buffer;
      gtk_tree_model_get(tree_model, &iter,
			  0, &buffer, -1);
      while( strcmp(buffer,start) ) {
	if( !gtk_tree_model_iter_next(tree_model, &iter) ) {
	  break;
	}
	gtk_tree_model_get(tree_model, &iter,
			   0, &buffer, -1);
      }
      if( strcmp(buffer,start) ) {
	gtk_tree_store_append (GTK_TREE_STORE(tree_model), &iter, NULL);
	
	if( end == NULL ) {
	  gtk_tree_store_set (GTK_TREE_STORE(tree_model), &iter,
			      0, strdup(start),
			      1, strdup(fullname),
			      2, strdup(library),
			      3, strdup(type),
			      -1);
	} else {
	  gtk_tree_store_set (GTK_TREE_STORE(tree_model), &iter,
			      0, strdup(start),
			      1, NULL,
			      2, NULL,
			      3, NULL,
			      -1);
	}
      }
    } else {
      /*
       * There's no root item.
       */
      gtk_tree_store_append (GTK_TREE_STORE(tree_model), &iter, NULL);
      if( end == NULL ) {
	gtk_tree_store_set (GTK_TREE_STORE(tree_model), &iter,
			    0, strdup(start),
			    1, strdup(fullname),
			    2, strdup(library),
			    3, strdup(type),
			    -1);
      } else {
	gtk_tree_store_set (GTK_TREE_STORE(tree_model), &iter,
			    0, strdup(start),
			    1, NULL,
			    2, NULL,
			    3, NULL,
			    -1);
      }
    }
  }
  _gael_librarian_chooser_add_element(tree_model,&iter,library,fullname,type,end);
}

void
gael_librarian_chooser_add_element (GaelLibrarianChooser *chooser,
				    guchar *library,
				    guchar *fullname,
				    guchar *type)
{
  guchar *buffer;

  if( fullname[0] == '/' ) {
    buffer = strdup(fullname+1);
  } else {
    buffer = strdup(fullname);
  }

  _gael_librarian_chooser_add_element(GTK_TREE_MODEL(chooser->model),NULL,library,fullname,type,buffer);

  g_free(buffer);
}

void
gael_librarian_chooser_remove_element (GaelLibrarianChooser *chooser,
				       guchar *fullname)
{
  g_message("%s(%i) gael_librarian_chooser_remove_element TODO",__FILE__,__LINE__);
}

static void
gael_librarian_chooser_selection_changed (GtkTreeSelection *selection,
					  gpointer user_data)
{
  GaelLibrarianChooser *chooser = GAEL_LIBRARIAN_CHOOSER(user_data);
  GtkWidget            *widget;
  GtkTreeIter           iter;
  guchar               *lib, *name;

  /*
   * Get the selected element.
   */

  widget = glade_xml_get_widget(chooser->xml,"GaelLibrarianChooserElementList");

  if (! (selection = gtk_tree_view_get_selection (GTK_TREE_VIEW(widget))) )
    return;

  if (! gtk_tree_selection_get_selected (selection, NULL, &iter))
    return;

  gtk_tree_model_get (GTK_TREE_MODEL(chooser->model), &iter,
		      1, &name,
		      2, &lib,
		      -1);

  chooser->symbol = NULL;

  if( lib != NULL ) {
    chooser->symbol = gael_librarian_get_elem(chooser->librarian,lib,name);
  }

  /*
   * redraw the preview windows
   */
  widget = glade_xml_get_widget(chooser->xml,"GaelLibrarianChooserDrawingSymbol");
  gtk_widget_queue_draw(widget);
}

static gboolean
gael_librarian_chooser_expose_drawing_area1 (GtkWidget *widget, GdkEventExpose *event, gpointer user_data)
{
  GaelLibrarianChooser *chooser;

  chooser = GAEL_LIBRARIAN_CHOOSER(user_data);
  if( chooser->symbol == NULL ) {
    return FALSE;
  }

  return gael_library_element_display(GAEL_LIBRARY_ELEMENT(chooser->symbol),widget);
}
