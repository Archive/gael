/* gael-librarian.c
 * Copyright (C) 2001, 2002 Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "gael-librarian.h"

#include <string.h>

#include "gael-library.h"
#include "gael-library-element.h"

#include "util/gael-marshal.h"

/* properties */
enum {
  PROP_NONE,
  PROP_LAST
};

/* signals */
enum {
  COMP_SELECTED,
  CHOOSER_VISIBLE,
  LAST_SIGNAL
};

static void gael_librarian_init         (GaelLibrarian *librarian);
static void gael_librarian_class_init   (GaelLibrarianClass *klass);
static void gael_librarian_finalize     (GObject *object);

static void gael_librarian_set_property (GObject *object,
					 guint property_id,
					 const GValue *value,
					 GParamSpec *pspec);
static void gael_librarian_get_property (GObject *object,
					 guint property_id,
					 GValue *value,
					 GParamSpec *pspec);

static void gael_librarian_load_library (GaelLibrarian *librarian,
					 guchar *path, guchar *name);

static void gael_librarian_component_selected (GaelLibrarian *librarian, guchar *lib, guchar *name);
static void gael_librarian_chooser_visible    (GaelLibrarian *librarian, gboolean state);


static GObjectClass *parent_class = NULL;
static guint gael_librarian_signals[LAST_SIGNAL] = { 0 };

GType
gael_librarian_get_type (void)
{
  static GtkType gael_librarian_type = 0;

  if (!gael_librarian_type)
    {
      static const GTypeInfo gael_librarian_info =
      {
        sizeof (GaelLibrarianClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) gael_librarian_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,
        
        sizeof (GaelLibrarian),
        0, /* n_preallocs */
        (GInstanceInitFunc) gael_librarian_init,
      };

      gael_librarian_type = g_type_register_static (G_TYPE_OBJECT,
						    "GaelLibrarian",
						    &gael_librarian_info,
						    0);
    }
  return gael_librarian_type;
}

static void
gael_librarian_class_init (GaelLibrarianClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS(klass);
  parent_class = g_type_class_ref(G_TYPE_OBJECT);

  object_class->finalize     = gael_librarian_finalize;

  object_class->set_property = gael_librarian_set_property;
  object_class->get_property = gael_librarian_get_property;

  klass->comp_selected       = gael_librarian_component_selected;
  klass->chooser_visible     = gael_librarian_chooser_visible;

  gael_librarian_signals[COMP_SELECTED] = 
    g_signal_new ("comp_selected",
		  G_OBJECT_CLASS_TYPE(klass),
		  G_SIGNAL_RUN_FIRST,
		  G_STRUCT_OFFSET (GaelLibrarianClass, comp_selected), NULL, NULL,
		  gael_marshal_VOID__POINTER_POINTER,
		  G_TYPE_NONE, 2,
		  G_TYPE_POINTER, G_TYPE_POINTER);
  gael_librarian_signals[CHOOSER_VISIBLE] = 
    g_signal_new ("chooser_visible",
		  G_OBJECT_CLASS_TYPE(klass),
		  G_SIGNAL_RUN_FIRST,
		  G_STRUCT_OFFSET (GaelLibrarianClass, chooser_visible), NULL, NULL,
		  gael_marshal_VOID__BOOLEAN,
		  G_TYPE_NONE, 1,
		  G_TYPE_BOOLEAN);
}

static void
gael_librarian_init (GaelLibrarian *librarian)
{
  librarian->libraries = g_hash_table_new_full( g_str_hash, g_str_equal,
						g_free, g_object_unref );

  librarian->chooser = g_object_new(GAEL_TYPE_LIBRARIAN_CHOOSER, NULL);
  g_message("%s(%i) remove this hack",__FILE__,__LINE__);
  librarian->chooser->librarian = librarian;

  g_message("[librarian]: implement the configuration files");
  gael_librarian_load_library(librarian,
			      "file:" GAEL_DATADIR "/gael/lib/",
			      "default");
}

static void
gael_librarian_finalize (GObject *object)
{
  GaelLibrarian *librarian = GAEL_LIBRARIAN(object);

  g_hash_table_destroy(librarian->libraries);
  g_object_unref(librarian->chooser);
}

static void
gael_librarian_set_property (GObject *object,
			     guint property_id,
			     const GValue *value,
			     GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
gael_librarian_get_property (GObject *object,
			     guint property_id,
			     GValue *value,
			     GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
gael_librarian_load_library (GaelLibrarian *librarian,
			     guchar *path, guchar *name)
{
  GaelLibrary *library;

  /*
   * Create, load and store the library.
   */
  library = g_object_new(GAEL_TYPE_LIBRARY,
			 "name",name,
			 "path",path,
			 "librarian",librarian,
			 NULL);
  if( !gael_library_load(library) )
    g_message("Error loading library");
  g_hash_table_insert(librarian->libraries,strdup(name),library);
}

static void
gael_librarian_component_selected (GaelLibrarian *librarian, guchar *lib, guchar *name)
{
}

static void
gael_librarian_chooser_visible (GaelLibrarian *librarian,
				gboolean state)
{
}

gpointer
gael_librarian_get_elem (GaelLibrarian *librarian,
			 guchar        *library_name,
			 guchar        *element_name)
{
  GaelLibrary *library;

  library = g_hash_table_lookup(librarian->libraries, library_name);
  if( library != NULL ) {
    return gael_library_get_elem(library,element_name);
  }
  g_message("Couldn't find library %s",library_name);
  return NULL;
}

void
gael_librarian_show_chooser (GaelLibrarian *librarian,
			     gboolean show)
{
  if( show ) {
    gtk_widget_show(librarian->chooser->window);
  } else {
    gtk_widget_hide(librarian->chooser->window);
  }
}

void
gael_librarian_add_element (GaelLibrarian *librarian,
			    guchar *library,
			    guchar *fullname,
			    guchar *type)
{
  gael_librarian_chooser_add_element(librarian->chooser,library,fullname,type);
}

void
gael_librarian_remove_element (GaelLibrarian *librarian,
			       guchar *library,
			       guchar *fullname)
{
  g_message("%s(%i)",__FILE__,__LINE__);
}

gchar *
gael_librarian_get_elem_type (GaelLibrarian *librarian,
			      guchar        *library,
			      guchar        *name)
{
  GaelLibrary *lib;

  lib = g_hash_table_lookup(librarian->libraries, library);
  if( lib != NULL ) {
    return gael_library_get_elem_type(lib,name);
  }
  return NULL;
}
