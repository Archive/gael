/* gael-library.c
 * Copyright (C) 2001, 2002 Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "gael-librarian.h"

#include "gael-xml-library.h"
#include <loaders/gael-loader.h>
#include <util/gael-intl.h>
#include <general/gael-module.h>
#include <string.h>

extern GHashTable *modules;


/* properties */
enum {
  PROP_NONE,
  PROP_NAME,
  PROP_PATH,
  PROP_LIBRARIAN,
  PROP_LAST
};

static void gael_library_init         (GaelLibrary *library);
static void gael_library_class_init   (GaelLibraryClass *klass);
static void gael_library_finalize     (GObject *object);

static void gael_library_set_property (GObject *object,
				      guint property_id,
				      const GValue *value,
				      GParamSpec *pspec);
static void gael_library_get_property (GObject *object,
				      guint property_id,
				      GValue *value,
				      GParamSpec *pspec);

static GObjectClass *parent_class = NULL;

GType
gael_library_get_type (void)
{
  static GtkType gael_library_type = 0;

  if (!gael_library_type)
    {
      static const GTypeInfo gael_library_info =
      {
        sizeof (GaelLibraryClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) gael_library_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,
        
        sizeof (GaelLibrary),
        0, /* n_preallocs */
        (GInstanceInitFunc) gael_library_init,
      };

      gael_library_type = g_type_register_static (G_TYPE_OBJECT,
						 "GaelLibrary",
						 &gael_library_info,
						 0);
    }
  return gael_library_type;
}

static void
gael_library_class_init (GaelLibraryClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS(klass);
  parent_class = g_type_class_ref(G_TYPE_OBJECT);

  object_class->finalize     = gael_library_finalize;

  object_class->set_property = gael_library_set_property;
  object_class->get_property = gael_library_get_property;

  g_object_class_install_property (object_class,
				   PROP_NAME,
				   g_param_spec_string ("name", _("Library name"),
							_("The library name"),
							"",
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_PATH,
				   g_param_spec_string ("path", _("Library path"),
							_("The library path"),
							"",
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_LIBRARIAN,
				   g_param_spec_object ("librarian", _("librarian"),
							_("The librarian object"),
							GAEL_TYPE_LIBRARIAN,
							G_PARAM_READWRITE));
}

static void
gael_library_init (GaelLibrary *library)
{
  library->type     = g_hash_table_new_full( g_str_hash,
					     g_str_equal,
					     g_free,
					     g_free );

  library->elements = g_hash_table_new_full( g_str_hash,
					     g_str_equal,
					     g_free,
					     g_object_unref );
}

static void
gael_library_finalize (GObject *object)
{
  GaelLibrary *library = GAEL_LIBRARY(object);

  g_free(library->name);
  g_free(library->path);

  g_hash_table_destroy(library->type);
  g_hash_table_destroy(library->elements);
}

static void
gael_library_set_property (GObject *object,
			   guint property_id,
			   const GValue *value,
			   GParamSpec *pspec)
{
  GaelLibrary *library = GAEL_LIBRARY(object);

  switch (property_id)
    {
    case PROP_NAME:
      if( library->name != NULL ) {
	g_free(library->name);
      }
      library->name = strdup(g_value_get_string(value));
      break;

    case PROP_PATH:
      if( library->path != NULL ) {
	g_free(library->path);
      }
      library->path = strdup(g_value_get_string(value));
      break;

    case PROP_LIBRARIAN:
      if( library->librarian != NULL ) {
	g_free(library->librarian);
      }
      library->librarian = GAEL_LIBRARIAN(g_value_get_object(value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
gael_library_get_property (GObject *object,
			   guint property_id,
			   GValue *value,
			   GParamSpec *pspec)
{
  switch (property_id)
    {
    case PROP_NAME:
    case PROP_PATH:
      g_message("%s(%i) TODO",__FILE__,__LINE__);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
gael_library_load_foreach (gchar         *name,
			   gchar         *type,
			   GaelLibrary   *lib)
{
  gael_librarian_add_element(lib->librarian, lib->name, name, type);
}

gboolean
gael_library_load (GaelLibrary *library)
{  
  guchar *buffer;

  /*
   * Do some check.
   */
  if( (library->name == NULL) ||
      (library->path == NULL) ) {
    g_message("Missing a data to load the library");
    return FALSE;
  }
    
  /*
   * Load the XML.
   */
  buffer = gael_loader_load_library(library->path,library->name);
  if( buffer == NULL )
    return FALSE;

  /*
   * Parse the XML
   */
  gael_xml_library_parse(buffer,library);
  g_free(buffer);

  /*
   * Add the elments to the tree.
   */
  g_hash_table_foreach( library->type,
			(GHFunc)gael_library_load_foreach,
			library );

  return TRUE;
}

gpointer
gael_library_get_elem (GaelLibrary *library, guchar *name)
{
  guchar             *type;
  GaelLibraryElement *element  = NULL;

  /*
   * Try if the element is already loaded.
   */
  element = g_hash_table_lookup(library->elements,name);
  if( element != NULL ) {
    return element;
  }

  /*
   * Get the path
   */
  type = g_hash_table_lookup(library->type, name);
  /*
  type = NULL;
  for( i=0; i<library->summary->len; i++ ) {
    if( strcmp(name,g_ptr_array_index(library->summary,i)) == 0 ) {
      path = g_ptr_array_index(library->summary,i);
      type = g_ptr_array_index(library->type,i);
    }
  }
  */

  if( type == NULL ) {
    g_message("Element %s not found in library %s",name,library->name);
    return NULL;
  }

  /*
   * Load it.
   */

  if( strcmp(type,"link") == 0 ) {
    g_message("%s(%i): create an object with the type: %s.",__FILE__,__LINE__,type);
    return NULL;
  } else {
    GaelModule *module;

    module = g_hash_table_lookup(modules,type);
    if( module == NULL ) {
      g_message("Couldn't create object type: %s",type);
      return NULL;
    }
    element = gael_module_get_new_elem(module,name,name,library->path);
    gael_library_element_load(element);
    g_hash_table_insert(library->elements,strdup(name),element);
  }

  return element;
}

gchar*
gael_library_get_elem_type (GaelLibrary *lib,
			    guchar *name)
{
  return g_hash_table_lookup(lib->type, name);
}
