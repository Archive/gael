/* gael-xml-library.c
 * Copyright (C) 2001, 2002 Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <gtk/gtk.h>
#include <stdlib.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlmemory.h>
#include <string.h>

#include "gael-xml-library.h"
#include <general/gael-general.h>

typedef enum {
  GXL_START,
  GXL_LIBRARY,
  GXL_SUMMARY,
  GXL_ELEM,
  GXL_LINK,
  GXL_ERROR
} GaelXmlLibraryState;

typedef struct _GaelXmlLibraryParserData GaelXmlLibraryParserData;

struct _GaelXmlLibraryParserData {
  GaelXmlLibraryState state;
  GList *state_list;

  GaelLibrary *library;
  guchar      *name;
  guchar      *type;
};

static void gael_xml_library_start_document (GaelXmlLibraryParserData *ctx);
static void gael_xml_library_end_document   (GaelXmlLibraryParserData *ctx);
static void gael_xml_library_start_element  (GaelXmlLibraryParserData *ctx,
					     const xmlChar *name,
					     const xmlChar **atts);
static void gael_xml_library_end_element    (GaelXmlLibraryParserData *ctx,
					     const xmlChar *name);
static void gael_xml_library_characters     (GaelXmlLibraryParserData *ctx,
					     const xmlChar *ch,
					     int len);

xmlSAXHandler GaelXmlLibraryParserStruct = {
  NULL, //internalSubsetDebug,
  NULL, //isStandaloneDebug,
  NULL, //hasInternalSubsetDebug,
  NULL, //hasExternalSubsetDebug,
  NULL, //resolveEntityDebug,
  NULL, //getEntityDebug,
  NULL, //entityDeclDebug,
  NULL, //notationDeclDebug,
  NULL, //attributeDeclDebug,
  NULL, //elementDeclDebug,
  NULL, //unparsedEntityDeclDebug,
  NULL, //setDocumentLocatorDebug,
  (startDocumentSAXFunc)gael_xml_library_start_document,
  (endDocumentSAXFunc)gael_xml_library_end_document,
  (startElementSAXFunc)gael_xml_library_start_element,
  (endElementSAXFunc)gael_xml_library_end_element,
  NULL, //referenceDebug,
  (charactersSAXFunc)gael_xml_library_characters,
  NULL, //ignorableWhitespaceDebug,
  NULL, //processingInstructionDebug,
  NULL, //commentDebug,
  NULL, //warningDebug,
  NULL, //errorDebug,
  NULL, //fatalErrorDebug,
  NULL, //getParameterEntityDebug,
};

xmlSAXHandlerPtr GaelXmlLibraryParserHandler = &GaelXmlLibraryParserStruct;

void
gael_xml_library_parse(guchar *buffer,GaelLibrary *library)
{
  GaelXmlLibraryParserData ctx;

  g_return_if_fail(buffer != NULL);
  g_return_if_fail(library != NULL);
  g_return_if_fail(GAEL_IS_LIBRARY(library));

  ctx.library = library;
  xmlSAXUserParseMemory (GaelXmlLibraryParserHandler,
			 &ctx,
			 buffer, strlen(buffer));
  if( ctx.state == GXL_ERROR ) {
    g_message("Error in reading xml for library: ");
  }
}

/*
 * private functions.
 */

static void
gael_xml_library_start_document (GaelXmlLibraryParserData *ctx)
{
  ctx->state      = GXL_START;
  ctx->state_list = NULL;
}

static void
gael_xml_library_end_document (GaelXmlLibraryParserData *ctx)
{
  if( ctx->state_list != NULL ) {
    ctx->state = GXL_ERROR;
    while( ctx->state_list != NULL ) {
      ctx->state_list =
	g_list_delete_link(ctx->state_list,ctx->state_list);
    }
  }
}

static void
gael_xml_library_start_element (GaelXmlLibraryParserData *ctx,
			       const xmlChar *name,
			       const xmlChar **atts)
{
  gint i;
  GaelXmlLibraryState *oldstate;

  /*
   * Pushes the current state.
   */
  oldstate = g_new0(GaelXmlLibraryState,1);
  *oldstate = ctx->state;
  ctx->state_list = g_list_append(ctx->state_list,oldstate);

  switch( ctx->state ) {

  case GXL_START:
    /*
     * On start, check we have a library tag,
     * else this is an error.
     */
    if( !strcmp(name,"gael:library") ) {
      ctx->state = GXL_LIBRARY;
    } else {
      ctx->state = GXL_ERROR;
    }
    break;

  case GXL_LIBRARY:
    /*
     * Inside a library, look for the summary tag.
     */
    if( !strcmp(name,"gael:summary") ) {
      ctx->state = GXL_SUMMARY;
    } else {
      ctx->state = GXL_ERROR;
    }
    break;

  case GXL_SUMMARY:
    /*
     * Checks the possible tags within a summary
     * (symbol, model, package and link) else
     * sets an error
     */

    if( !strcmp(name,"gael:link") ) {
      /*
       * The tag is a link, checks name.
       */
      ctx->state = GXL_LINK;
      ctx->type  = strdup("link");
      if( atts != NULL ) {
	for( i=0; atts[2*i] != NULL; i++ ) {
	  if( !strcmp(atts[2*i],"name") ) {
	    ctx->name = strdup(atts[2*i+1]);
	  } else {
	    ctx->state = GXL_ERROR;
	  }
	}
      }
      break;
    }

    if( !strncmp(name,"gael:",5) ) {
      /*
       * The tag is a symbol, checks name.
       */
      ctx->state = GXL_ELEM;
      ctx->type  = strdup(name+5);
      if( atts != NULL ) {
	for( i=0; atts[2*i] != NULL; i++ ) {
	  if( !strcmp(atts[2*i],"name") ) {
	    ctx->name = strdup(atts[2*i+1]);
	  } else {
	    ctx->state = GXL_ERROR;
	  }
	}
      }
      break;
    }

    ctx->state = GXL_ERROR;
    break;

  case GXL_ERROR:
    break;

  default:
    ctx->state = GXL_ERROR;
    break;
  }
}

static void
gael_xml_library_end_element (GaelXmlLibraryParserData *ctx,
			      const xmlChar *name)
{
  if( ctx->state == GXL_ERROR )
    return;

  switch( ctx->state ) {

  case GXL_ELEM:
  case GXL_LINK:
    /*
     * element completed, insert it in the lib summary.
     */
    if( (ctx->name != NULL) &&
	(ctx->type != NULL) ) {
      g_hash_table_insert(ctx->library->type, ctx->name, ctx->type);
    } else {
      if( ctx->name != NULL )
	g_free(ctx->name);
      if( ctx->type != NULL )
	g_free(ctx->type);
    }
    ctx->name = NULL;
    ctx->type = NULL;

  default:
    break;
  }

  /*
   * Pops the state.
   */
  ctx->state = *((GaelXmlLibraryState*)g_list_last(ctx->state_list)->data);
  ctx->state_list = g_list_delete_link(ctx->state_list,g_list_last(ctx->state_list));
}

static void
gael_xml_library_characters (GaelXmlLibraryParserData *ctx,
			     const xmlChar *ch,
			     int len)
{
}
