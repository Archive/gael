/* gael-librarian-chooser.h
 * Copyright (C) 2001, 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GAEL_LIBRARIAN_CHOOSER_H__
#define __GAEL_LIBRARIAN_CHOOSER_H__

#include <gtk/gtk.h>
#include <glade/glade.h>

#define GAEL_TYPE_LIBRARIAN_CHOOSER            (gael_librarian_chooser_get_type ())
#define GAEL_LIBRARIAN_CHOOSER(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GAEL_TYPE_LIBRARIAN_CHOOSER, GaelLibrarianChooser))
#define GAEL_LIBRARIAN_CHOOSER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GAEL_TYPE_LIBRARIAN_CHOOSER, GaelLibrarianChooserClass))
#define GAEL_IS_LIBRARIAN_CHOOSER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GAEL_TYPE_LIBRARIAN_CHOOSER))
#define GAEL_IS_LIBRARIAN_CHOOSER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GAEL_TYPE_LIBRARIAN_CHOOSER))
#define GAEL_LIBRARIAN_CHOOSER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),   GAEL_TYPE_LIBRARIAN_CHOOSER, GaelLibrarianChooserClass))

typedef struct _GaelLibrarianChooser      GaelLibrarianChooser;
typedef struct _GaelLibrarianChooserClass GaelLibrarianChooserClass;

struct _GaelLibrarianChooser
{
  GObject object;

  GaelLibrarian *librarian;
  GladeXML      *xml;
  GtkWidget     *window;
  GtkTreeStore  *model;

  gpointer       symbol;
};

struct _GaelLibrarianChooserClass
{
  GObjectClass parent_class;
};

GType gael_librarian_chooser_get_type (void);

void  gael_librarian_chooser_add_element    (GaelLibrarianChooser *chooser,
					     guchar *library,
					     guchar *fullname,
					     guchar *type);
void  gael_librarian_chooser_remove_element (GaelLibrarianChooser *chooser,
					     guchar *fullname);

#endif
