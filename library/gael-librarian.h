/* gael-librarian.h
 * Copyright (C) 2001, 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GAEL_LIBRARIAN_H__
#define __GAEL_LIBRARIAN_H__

#include <gtk/gtk.h>

#define GAEL_TYPE_LIBRARIAN            (gael_librarian_get_type ())
#define GAEL_LIBRARIAN(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GAEL_TYPE_LIBRARIAN, GaelLibrarian))
#define GAEL_LIBRARIAN_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GAEL_TYPE_LIBRARIAN, GaelLibrarianClass))
#define GAEL_IS_LIBRARIAN(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GAEL_TYPE_LIBRARIAN))
#define GAEL_IS_LIBRARIAN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GAEL_TYPE_LIBRARIAN))
#define GAEL_LIBRARIAN_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),   GAEL_TYPE_LIBRARIAN, GaelLibrarianClass))

typedef struct _GaelLibrarian      GaelLibrarian;
typedef struct _GaelLibrarianClass GaelLibrarianClass;

#include "gael-library.h"
#include "gael-librarian-chooser.h"
#include "gael-library-element.h"

struct _GaelLibrarian
{
  GObject object;

  GHashTable           *libraries;
  GaelLibrarianChooser *chooser;
};

struct _GaelLibrarianClass
{
  GObjectClass parent_class;

  void (*comp_selected)   (GaelLibrarian *librarian, guchar *lib, guchar *name);
  void (*chooser_visible) (GaelLibrarian *librarian, gboolean state);
};

GType gael_librarian_get_type (void);

gpointer gael_librarian_get_elem    (GaelLibrarian *librarian,
				     guchar        *library_name,
				     guchar        *element_name);

void gael_librarian_show_chooser    (GaelLibrarian *librarian,
				     gboolean       show);

void gael_librarian_add_element     (GaelLibrarian *librarian,
				     guchar        *library,
				     guchar        *fullname,
				     guchar        *type);
void gael_librarian_remove_element  (GaelLibrarian *librarian,
				     guchar        *library,
				     guchar        *fullname);
gchar *gael_librarian_get_elem_type (GaelLibrarian *librarian,
				     guchar        *library,
				     guchar        *fullname);

#endif
