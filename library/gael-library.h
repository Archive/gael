/* gael-library.h
 * Copyright (C) 2001, 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GAEL_LIBRARY_H__
#define __GAEL_LIBRARY_H__

#include <gtk/gtk.h>

#define GAEL_TYPE_LIBRARY            (gael_library_get_type ())
#define GAEL_LIBRARY(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GAEL_TYPE_LIBRARY, GaelLibrary))
#define GAEL_LIBRARY_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GAEL_TYPE_LIBRARY, GaelLibraryClass))
#define GAEL_IS_LIBRARY(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GAEL_TYPE_LIBRARY))
#define GAEL_IS_LIBRARY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GAEL_TYPE_LIBRARY))
#define GAEL_LIBRARY_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),   GAEL_TYPE_LIBRARY, GaelLibraryClass))

typedef struct _GaelLibrary        GaelLibrary;
typedef struct _GaelLibraryClass   GaelLibraryClass;

struct _GaelLibrary
{
  GObject object;

  guchar  *name;
  guchar  *path;

  GaelLibrarian *librarian;

  GHashTable *type;
  GHashTable *elements;
};

struct _GaelLibraryClass
{
  GObjectClass parent_class;
};

GType gael_library_get_type (void);

gboolean gael_library_load (GaelLibrary *library);

gpointer gael_library_get_elem      (GaelLibrary *lib, guchar *name);
gchar   *gael_library_get_elem_type (GaelLibrary *lib, guchar *name);

#endif
