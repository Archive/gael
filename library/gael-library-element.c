/* gael-library-element.c
 * Copyright (C) 2001, 2002 Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "gael-librarian.h"
#include <util/gael-marshal.h>

#include <general/gael-general.h>
#include <loaders/gael-loader.h>
#include "gael-xml-library.h"
#include <util/gael-intl.h>
#include <string.h>


/* signals */
enum {
  LOAD,
  DISPLAY,
  LAST_SIGNAL
};

/* properties */
enum {
  PROP_NONE,
  PROP_NAME,
  PROP_PATH,
  PROP_LIBNAME,
  PROP_LIBPATH,
  PROP_LAST
};

static void gael_library_element_init         (GaelLibraryElement *element);
static void gael_library_element_class_init   (GaelLibraryElementClass *klass);
static void gael_library_element_finalize     (GObject *object);

static void gael_library_element_set_property (GObject *object,
					      guint property_id,
					      const GValue *value,
					      GParamSpec *pspec);
static void gael_library_element_get_property (GObject *object,
					      guint property_id,
					      GValue *value,
					      GParamSpec *pspec);

static GObjectClass *parent_class = NULL;
static guint element_signals[LAST_SIGNAL] = { 0 };

GType
gael_library_element_get_type (void)
{
  static GtkType gael_library_element_type = 0;

  if (!gael_library_element_type)
    {
      static const GTypeInfo gael_library_element_info =
      {
        sizeof (GaelLibraryElementClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) gael_library_element_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,
        
        sizeof (GaelLibraryElement),
        0, /* n_preallocs */
        (GInstanceInitFunc) gael_library_element_init,
      };

      gael_library_element_type = g_type_register_static (G_TYPE_OBJECT,
							 "GaelLibraryElement",
							 &gael_library_element_info,
							 0);
    }
  return gael_library_element_type;
}

static void
gael_library_element_class_init (GaelLibraryElementClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS(klass);
  parent_class = g_type_class_ref(G_TYPE_OBJECT);

  object_class->finalize = gael_library_element_finalize;

  object_class->set_property = gael_library_element_set_property;
  object_class->get_property = gael_library_element_get_property;

  element_signals[LOAD] = 
    g_signal_new ("load",
		  G_OBJECT_CLASS_TYPE(klass),
		  G_SIGNAL_RUN_LAST,
		  G_STRUCT_OFFSET (GaelLibraryElementClass, load), NULL, NULL,
		  gael_marshal_BOOLEAN__VOID,
		  G_TYPE_BOOLEAN, 0);

  element_signals[DISPLAY] = 
    g_signal_new ("display",
		  G_OBJECT_CLASS_TYPE(klass),
		  G_SIGNAL_RUN_LAST,
		  G_STRUCT_OFFSET (GaelLibraryElementClass, display), NULL, NULL,
		  gael_marshal_BOOLEAN__OBJECT,
		  G_TYPE_BOOLEAN, 1, G_TYPE_OBJECT);

  g_object_class_install_property (object_class,
				   PROP_NAME,
				   g_param_spec_string ("name", _("Element name"),
							_("The library element name"),
							"",
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_PATH,
				   g_param_spec_string ("path", _("Element path"),
							_("The library element path"),
							"",
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_LIBPATH,
				   g_param_spec_string ("libpath", _("Library path"),
							_("The library path"),
							"",
							G_PARAM_READWRITE));
}

static void
gael_library_element_init (GaelLibraryElement *element)
{
}

static void
gael_library_element_finalize (GObject *object)
{
  GaelLibraryElement *element = GAEL_LIBRARY_ELEMENT(object);

  g_free(element->name);
  g_free(element->path);
  g_free(element->libname);
  g_free(element->libpath);
}

static void
gael_library_element_set_property (GObject *object,
				  guint property_id,
				  const GValue *value,
				  GParamSpec *pspec)
{
  GaelLibraryElement *element = GAEL_LIBRARY_ELEMENT(object);

  switch (property_id)
    {

    case PROP_NAME:
      if( element->name != NULL ) {
	g_free(element->name);
      }
      element->name = strdup(g_value_get_string(value));
      break;

    case PROP_PATH:
      if( element->path != NULL ) {
	g_free(element->path);
      }
      element->path = strdup(g_value_get_string(value));
      break;

    case PROP_LIBPATH:
      if( element->libpath == NULL ) {
	g_free(element->libpath);
      }
      element->libpath = strdup(g_value_get_string(value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
gael_library_element_get_property (GObject *object,
				  guint property_id,
				  GValue *value,
				  GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

gboolean
gael_library_element_load (GaelLibraryElement *element)
{
  gboolean result;
  static guint load_signal_id = 0;

  if (load_signal_id == 0)
    load_signal_id = gtk_signal_lookup("load", GAEL_TYPE_LIBRARY_ELEMENT);

  g_signal_emit(element, load_signal_id, 0, &result);

  return result;
}

gboolean
gael_library_element_display (GaelLibraryElement *element,
			      GtkWidget          *widget)
{
  gboolean result;
  static guint load_signal_id = 0;

  if (load_signal_id == 0)
    load_signal_id = gtk_signal_lookup("display", GAEL_TYPE_LIBRARY_ELEMENT);

  g_signal_emit(element, load_signal_id, 0, widget, &result);

  return result;
}
