/* gael-library-element.h
 * Copyright (C) 2001, 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GAEL_LIBRARY_ELEMENT_H__
#define __GAEL_LIBRARY_ELEMENT_H__

#include <gtk/gtk.h>
#include <dia-newcanvas/dia-newcanvas.h>

#define GAEL_TYPE_LIBRARY_ELEMENT            (gael_library_element_get_type ())
#define GAEL_LIBRARY_ELEMENT(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GAEL_TYPE_LIBRARY_ELEMENT, GaelLibraryElement))
#define GAEL_LIBRARY_ELEMENT_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GAEL_TYPE_LIBRARY_ELEMENT, GaelLibraryElementClass))
#define GAEL_IS_LIBRARY_ELEMENT(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GAEL_TYPE_LIBRARY_ELEMENT))
#define GAEL_IS_LIBRARY_ELEMENT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GAEL_TYPE_LIBRARY_ELEMENT))
#define GAEL_LIBRARY_ELEMENT_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),   GAEL_TYPE_LIBRARY_ELEMENT, GaelLibraryElementClass))

typedef struct _GaelLibraryElement      GaelLibraryElement;
typedef struct _GaelLibraryElementClass GaelLibraryElementClass;

struct _GaelLibraryElement
{
  GObject  object;

  guchar  *name;
  guchar  *path;
  guchar  *libname;
  guchar  *libpath;
};

struct _GaelLibraryElementClass
{
  GObjectClass parent_class;

  gboolean (*load)    (GaelLibraryElement *element);
  gboolean (*display) (GaelLibraryElement *element,
		       GtkWidget *widget);
};

GType gael_library_element_get_type (void);

gboolean gael_library_element_load    (GaelLibraryElement *element);
gboolean gael_library_element_display (GaelLibraryElement *element,
				       GtkWidget          *widget);

#endif
