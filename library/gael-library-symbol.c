/* gael-library-symbol.c
 * Copyright (C) 2001, 2002 Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "gael-librarian.h"

#include "general/gael-general.h"
#include "loaders/gael-loader.h"
#include "xml/gael-xml.h"
#include "util/gael-intl.h"
#include <string.h>

/* properties */
enum {
  PROP_NONE,
  PROP_LAST
};

static void gael_library_symbol_init         (GaelLibrarySymbol *symbol);
static void gael_library_symbol_class_init   (GaelLibrarySymbolClass *klass);
static void gael_library_symbol_finalize     (GObject *object);

static void gael_library_symbol_set_property (GObject *object,
					      guint property_id,
					      const GValue *value,
					      GParamSpec *pspec);
static void gael_library_symbol_get_property (GObject *object,
					      guint property_id,
					      GValue *value,
					      GParamSpec *pspec);

static gboolean gael_library_symbol_load (GaelLibraryElement *element);

static GaelLibraryElementClass *parent_class = NULL;

GType
gael_library_symbol_get_type (void)
{
  static GtkType gael_library_symbol_type = 0;

  if (!gael_library_symbol_type)
    {
      static const GTypeInfo gael_library_symbol_info =
      {
        sizeof (GaelLibrarySymbolClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) gael_library_symbol_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,
        
        sizeof (GaelLibrarySymbol),
        0, /* n_preallocs */
        (GInstanceInitFunc) gael_library_symbol_init,
      };

      gael_library_symbol_type = g_type_register_static (GAEL_TYPE_LIBRARY_ELEMENT,
							 "GaelLibrarySymbol",
							 &gael_library_symbol_info,
							 0);
    }
  return gael_library_symbol_type;
}

static void
gael_library_symbol_class_init (GaelLibrarySymbolClass *klass)
{
  GObjectClass            *object_class;
  GaelLibraryElementClass *element_class;

  object_class  = G_OBJECT_CLASS(klass);
  element_class = GAEL_LIBRARY_ELEMENT_CLASS(klass);
  parent_class  = g_type_class_ref(GAEL_TYPE_LIBRARY_ELEMENT);

  object_class->finalize = gael_library_symbol_finalize;

  object_class->set_property = gael_library_symbol_set_property;
  object_class->get_property = gael_library_symbol_get_property;

  element_class->load = gael_library_symbol_load;
}

static void
gael_library_symbol_init (GaelLibrarySymbol *symbol)
{
}

static void
gael_library_symbol_finalize (GObject *object)
{
  GaelLibrarySymbol *symbol = GAEL_LIBRARY_SYMBOL(object);

  while( symbol->geometries != NULL ) {
    g_free(symbol->geometries->data);
    symbol->geometries = g_list_delete_link(symbol->geometries,symbol->geometries);
  }

  G_OBJECT_CLASS(parent_class)->finalize(object);
}

static void
gael_library_symbol_set_property (GObject *object,
				  guint property_id,
				  const GValue *value,
				  GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
gael_library_symbol_get_property (GObject *object,
				  guint property_id,
				  GValue *value,
				  GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static gboolean
gael_library_symbol_load (GaelLibraryElement *element)
{
  guchar *buffer;
  GaelLibrarySymbol *symbol = GAEL_LIBRARY_SYMBOL(element);

  /*
   * Do some check.
   */
  if( (element->name == NULL) ||
      (element->path == NULL) ||
      (element->libpath == NULL) ) {
    g_message("Missing a data to load a symbol");
    return FALSE;
  }

  /*
   * Load the XML.
   */
  buffer = gael_loader_load_element(element->libpath, element->path, element->name);
  if( buffer == NULL )
    return FALSE;

  /*
   * Parse the XML
   */
  gael_xml_symbol_parse(buffer,symbol);
  g_free(buffer);

  return TRUE;
}
