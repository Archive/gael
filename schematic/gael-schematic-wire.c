/* gael-schematic-wire.c
 * Copyright (C) 2002  Xavier Ordoquy
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtk.h>
#include "gael-schematic-wire.h"
#include "gael-schematic-conn.h"
#include "gael-schematic-utils.h"
#include "gael-schematic-view.h"
#include "gael-schematic-model.h"
#include "util/gael-intl.h"

/* properties */
enum {
  PROP_NONE,
  PROP_X1,
  PROP_Y1,
  PROP_X2,
  PROP_Y2,
  PROP_LINE_COLOR,
  PROP_LINE_WIDTH,
  PROP_LAST
};

static void gael_schematic_wire_init       (GaelSchematicWire      *wire);
static void gael_schematic_wire_class_init (GaelSchematicWireClass *klass);
static void gael_schematic_wire_finalize   (GObject                *object);
static void gael_schematic_wire_component_interface_class_init (GaelComponentInterfaceClass *vtable);

static void gael_schematic_wire_set_property (GObject *object,
					      guint property_id,
					      const GValue *value,
					      GParamSpec *pspec);
static void gael_schematic_wire_get_property (GObject *object,
					      guint property_id,
					      GValue *value,
					      GParamSpec *pspec);

static void    gael_schematic_wire_real_connection_one_moved (DiaCanvasItem *item,
							      DiaCanvasConnection *connection,
							      gdouble dx, gdouble dy);
static void    gael_schematic_wire_real_connection_two_moved (DiaCanvasItem *item,
							      DiaCanvasConnection *connection,
							      gdouble dx, gdouble dy);

static gint    gael_schematic_wire_real_connection_event     (DiaCanvasItem *item,
							      GdkEvent      *event,
							      DiaCanvasView *view,
							      gpointer       user_data);

static void      gael_schematic_wire_set_parent        ( GaelComponentInterface *icomp,
							 GaelComponent          *component );
static void      gael_schematic_wire_property_changed  ( GaelComponentInterface *icomp,
							 gchar                  *name );
static void      gael_schematic_wire_delete            ( GaelComponentInterface *icomp );
static void      gael_schematic_wire_place             ( GaelComponentInterface *icomp );
static gchar    *gael_schematic_wire_save              ( GaelComponentInterface *icomp,
							 gchar                  *prepend );
static gboolean  gael_schematic_wire_load              ( GaelComponentInterface *icomp,
							 xmlDocPtr               doc,
							 xmlNodePtr              cur );



static DiaCanvasLineClass *parent_class = NULL;


GtkType
gael_schematic_wire_get_type (void)
{
  static GtkType schematic_wire_type = 0;

  if (!schematic_wire_type)
    {
      static const GTypeInfo schematic_wire_info =
      {
	sizeof (GaelSchematicWireClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) gael_schematic_wire_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,
	
	sizeof (GaelSchematicWire),
	0, /* n_preallocs */
	(GInstanceInitFunc) gael_schematic_wire_init,
      };

      static const GInterfaceInfo schematic_wire_component_interface_info =
      {
	(GInterfaceInitFunc) gael_schematic_wire_component_interface_class_init,
	(GInterfaceFinalizeFunc) NULL,
	NULL,
      };

      schematic_wire_type = g_type_register_static (DIA_TYPE_CANVAS_GROUP,
						    "GaelSchematicWire",
						    &schematic_wire_info, 0);
      g_type_add_interface_static (schematic_wire_type,
				   GAEL_TYPE_COMPONENT_INTERFACE,
				   &schematic_wire_component_interface_info);
    }

  return schematic_wire_type;
}

static void
gael_schematic_wire_class_init (GaelSchematicWireClass *klass)
{
  GObjectClass *object_class;
  DiaCanvasItemClass *item_class;

  object_class = G_OBJECT_CLASS(klass);
  item_class   = DIA_CANVAS_ITEM_CLASS(klass);

  parent_class = g_type_class_ref (DIA_TYPE_CANVAS_GROUP);

  object_class->finalize = gael_schematic_wire_finalize;

  object_class->set_property = gael_schematic_wire_set_property;
  object_class->get_property = gael_schematic_wire_get_property;

  g_object_class_install_property (object_class,
				   PROP_X1,
				   g_param_spec_double ("x1", _("First edge"),
							_("The position of the first edge of the line"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0.0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_Y1,
				   g_param_spec_double ("y1", _("First edge"),
							_("The position of the first edge of the line"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_X2,
				   g_param_spec_double ("x2", _("Second edge"),
							_("The position of the second edge of the line"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_Y2,
				   g_param_spec_double ("y2", _("Second edge"),
							_("The position of the second edge of the line"),
							-G_MAXDOUBLE,
							G_MAXDOUBLE,
							0,
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_LINE_COLOR,
				   g_param_spec_boxed ("line_color", _("Line color"),
						       _("The line color of the line"),
						       GDK_TYPE_COLOR,
						       G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_LINE_WIDTH,
				   g_param_spec_double ("line_width", _("Line width"),
							_("The thickness of line"),
							0, G_MAXDOUBLE, 0,
							G_PARAM_READWRITE));
}

static void
gael_schematic_wire_component_interface_class_init (GaelComponentInterfaceClass *vtable)
{
  vtable->property_changed = gael_schematic_wire_property_changed;
  vtable->set_parent       = gael_schematic_wire_set_parent;
  vtable->delete           = gael_schematic_wire_delete;
  vtable->place            = gael_schematic_wire_place;
  vtable->save             = gael_schematic_wire_save;
  vtable->load             = gael_schematic_wire_load;
}

static void
gael_schematic_wire_init (GaelSchematicWire *wire)
{
}

GObject *
gael_schematic_wire_new (GaelComponent *component,
			 DiaCanvasItem *parent)
{
  GaelSchematicWire *wire;

  wire = g_object_new(GAEL_TYPE_SCHEMATIC_WIRE,
		      "parent", parent,
		      NULL);
  g_signal_connect( wire, "event", G_CALLBACK(gael_schematic_utils_event), NULL );
  gael_component_interface_set_parent(GAEL_COMPONENT_INTERFACE(wire),component);

  wire->line = g_object_new( DIA_TYPE_CANVAS_LINE,
			     "parent", DIA_CANVAS_GROUP(wire),
			     NULL );

  wire->con_one = g_object_new( GAEL_TYPE_SCHEMATIC_CONN,
				"parent", wire,
				"move_indep", TRUE,
				NULL );
  g_signal_connect(wire->con_one, "connection_moved",
		   G_CALLBACK(gael_schematic_wire_real_connection_one_moved), NULL);
  g_signal_connect(wire->con_one, "event",
		   G_CALLBACK(gael_schematic_wire_real_connection_event), NULL);

  wire->con_two = g_object_new( GAEL_TYPE_SCHEMATIC_CONN,
				"parent", wire,
				"move_indep", TRUE,
				NULL );
  g_signal_connect(wire->con_two, "connection_moved",
		   G_CALLBACK(gael_schematic_wire_real_connection_two_moved), NULL);
  g_signal_connect(wire->con_two, "event",
		   G_CALLBACK(gael_schematic_wire_real_connection_event), NULL);

  wire->atr_one = g_object_new( DIA_TYPE_CANVAS_ATTRACTED,
				"parent", wire,
				NULL );

  wire->atr_two = g_object_new( DIA_TYPE_CANVAS_ATTRACTED,
				"parent", wire,
				NULL );

  g_object_set(wire,
	       "x1", 0.0,
	       "y1", 0.0,
	       "x2", 0.0,
	       "y2", 0.0,
	       "line_color", GAEL_SCHEMATIC_MODEL(parent->canvas)->line_color,
	       "line_width", 2.0,
	       NULL);

  return G_OBJECT(wire);
}

static void
gael_schematic_wire_finalize(GObject *object)
{
  GaelSchematicWire *wire;

  g_return_if_fail(object != NULL);
  g_return_if_fail(GAEL_IS_SCHEMATIC_WIRE(object));

  wire = GAEL_SCHEMATIC_WIRE(object);

  if (G_OBJECT_CLASS(parent_class)->finalize)
    (* G_OBJECT_CLASS(parent_class)->finalize) (object);
}

static void
gael_schematic_wire_set_property (GObject *object,
				  guint property_id,
				  const GValue *value,
				  GParamSpec *pspec)
{
  GaelSchematicWire *wire = GAEL_SCHEMATIC_WIRE(object);
  GdkColor *color;
  gdouble val;

  switch (property_id) {
  case PROP_X1:
    val = g_value_get_double(value);
    g_object_set(G_OBJECT(wire->con_one), "x", val,NULL);
    g_object_set(G_OBJECT(wire->atr_one), "x", val,NULL);
    g_object_set(G_OBJECT(wire->line),"x1",val,NULL);
    break;
  case PROP_Y1:
    val = g_value_get_double(value);
    g_object_set(G_OBJECT(wire->con_one), "y", val,NULL);
    g_object_set(G_OBJECT(wire->atr_one), "y", val,NULL);
    g_object_set(G_OBJECT(wire->line),"y1",val,NULL);
    break;
  case PROP_X2:
    val = g_value_get_double(value);
    g_object_set(G_OBJECT(wire->con_two), "x", val,NULL);
    g_object_set(G_OBJECT(wire->atr_two), "x", val,NULL);
    g_object_set(G_OBJECT(wire->line),"x2",val,NULL);
    break;
  case PROP_Y2:
    val = g_value_get_double(value);
    g_object_set(G_OBJECT(wire->con_two), "y", val,NULL);
    g_object_set(G_OBJECT(wire->atr_two), "y", val,NULL);
    g_object_set(G_OBJECT(wire->line),"y2",val,NULL);
    break;
  case PROP_LINE_COLOR:
    color = g_value_get_boxed(value);
    g_object_set(G_OBJECT(wire->line),"line_color",color,NULL);
    break;
  case PROP_LINE_WIDTH:
    val = g_value_get_double(value);
    g_object_set(G_OBJECT(wire->line),"line_width",val,NULL);
    break;
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
gael_schematic_wire_get_property (GObject *object,
				  guint property_id,
				  GValue *value,
				  GParamSpec *pspec)
{
  G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
}

/*****************************************************************************
 ******************************* S I G N A L S *******************************
 *****************************************************************************/

static void
gael_schematic_wire_real_connection_one_moved (DiaCanvasItem *item,
					       DiaCanvasConnection *connection,
					       gdouble dx, gdouble dy)
{
  GaelSchematicWire *wire = GAEL_SCHEMATIC_WIRE(item->parent);

  if( DIA_CANVAS_ITEM(wire)->selected == FALSE ) {
    g_object_set(wire,"x1",connection->pos.x,NULL);
    g_object_set(wire,"y1",connection->pos.y,NULL);
  }
}

static void
gael_schematic_wire_real_connection_two_moved (DiaCanvasItem *item,
					       DiaCanvasConnection *connection,
					       gdouble dx, gdouble dy)
{
  GaelSchematicWire *wire = GAEL_SCHEMATIC_WIRE(item->parent);

  if( DIA_CANVAS_ITEM(wire)->selected == FALSE ) {
    g_object_set(wire,"x2",connection->pos.x,NULL);
    g_object_set(wire,"y2",connection->pos.y,NULL);
  }
}

static gint
gael_schematic_wire_real_connection_event ( DiaCanvasItem *item,
					    GdkEvent      *event,
					    DiaCanvasView *view,
					    gpointer       user_data )
{
  GaelSchematicWire *wire      = GAEL_SCHEMATIC_WIRE(item->parent);
  GaelSchematicView *gsview    = GAEL_SCHEMATIC_VIEW(view);
  DiaPoint           newPoint;

  switch (event->type) {

  case GDK_BUTTON_PRESS:
    gsview->point.x = event->button.x;
    gsview->point.y = event->button.y;
    if( event->button.button == 1 ) {
      if( gsview->state == GAEL_SCHEMATIC_NOTHING ) {
	gsview->state = GAEL_SCHEMATIC_MOVE;
	dia_canvas_view_grab(view,FALSE);
	gael_schematic_utils_selection_clear(view->canvas);
	gael_schematic_utils_selection_add(item);
	return TRUE;
      }
      if( gsview->state == GAEL_SCHEMATIC_PLACE_WIRE_2 ) {
	gsview->state = GAEL_SCHEMATIC_NOTHING;
	dia_canvas_view_ungrab(view);
	dia_canvas_item_make_connection(DIA_CANVAS_ITEM(wire));
	return TRUE;
      }
    }
    break;

  case GDK_BUTTON_RELEASE:
    if( event->button.button == 1 ) {
      if( gsview->state == GAEL_SCHEMATIC_MOVE ) {
	gsview->state = GAEL_SCHEMATIC_NOTHING;
	dia_canvas_view_ungrab(view);
	dia_canvas_item_make_connection(DIA_CANVAS_ITEM(wire));
	return TRUE;
      }
    }
    break;

  case GDK_MOTION_NOTIFY:
    if( (gsview->state == GAEL_SCHEMATIC_MOVE) ||
	(gsview->state == GAEL_SCHEMATIC_PLACE_WIRE_2) ) {
      if( DIA_CANVAS_CONNECTION(item) == wire->con_one ) {
	newPoint.x = event->button.x - gsview->point.x;
	newPoint.y = event->button.y - gsview->point.y;
	dia_canvas_item_snapmove (DIA_CANVAS_ITEM(wire->atr_one), &(newPoint.x), &(newPoint.y));
	dia_canvas_item_move(DIA_CANVAS_ITEM(wire->con_one), newPoint.x, newPoint.y);
	gsview->point.x += newPoint.x;
	gsview->point.y += newPoint.y;
	g_object_set(wire,
		     "x1", gsview->point.x,
		     "y1", gsview->point.y,
		     NULL);
      }
      if( DIA_CANVAS_CONNECTION(item) == wire->con_two ) {
	newPoint.x = event->button.x - gsview->point.x;
	newPoint.y = event->button.y - gsview->point.y;
	dia_canvas_item_snapmove (DIA_CANVAS_ITEM(wire->atr_two), &(newPoint.x), &(newPoint.y));
	dia_canvas_item_move(DIA_CANVAS_ITEM(wire->con_two), newPoint.x, newPoint.y);
	gsview->point.x += newPoint.x;
	gsview->point.y += newPoint.y;
	g_object_set(wire,
		     "x2", gsview->point.x,
		     "y2", gsview->point.y,
		     NULL);
      }
      return TRUE;
    }
    break;
  default:
    break;
  }
  return FALSE;
}

static void
gael_schematic_wire_set_parent ( GaelComponentInterface *icomp,
				 GaelComponent          *component )
{
  GAEL_SCHEMATIC_WIRE(icomp)->parent = component;
}

static void
gael_schematic_wire_property_changed ( GaelComponentInterface *icomp,
				       gchar                  *name )
{
  g_message("%s(%i) gael_schematic_wire_property_changed not implemented",__FILE__,__LINE__);
}

static void
gael_schematic_wire_place ( GaelComponentInterface *icomp )
{
  DiaCanvasItem       *item;
  DiaPoint             newPoint;
  gint                 x, y;

  item   = DIA_CANVAS_ITEM(icomp);

  gdk_window_get_pointer(gtk_widget_get_parent_window(GTK_WIDGET(item->canvas->focus_view)),&x,&y,0);
  newPoint.x = ((gdouble)x + item->canvas->extents.left) / item->canvas->focus_view->zoom_factor-1;
  newPoint.y = ((gdouble)y + item->canvas->extents.top)  / item->canvas->focus_view->zoom_factor-1;

  dia_canvas_item_snapmove(item, &(newPoint.x), &(newPoint.y));
  dia_canvas_item_move(item, newPoint.x, newPoint.y);
  GAEL_SCHEMATIC_VIEW(item->canvas->focus_view)->point.x = newPoint.x;
  GAEL_SCHEMATIC_VIEW(item->canvas->focus_view)->point.y = newPoint.y;

  dia_canvas_view_grab(item->canvas->focus_view,FALSE);
  gael_schematic_utils_selection_clear(item->canvas);
  gael_schematic_utils_selection_add(item);
  GAEL_SCHEMATIC_VIEW(item->canvas->focus_view)->state = GAEL_SCHEMATIC_PLACE_WIRE_1;
  item->canvas->focus_view->event_item = item;
  item->canvas->focus_view->point_item = item;
}

static void
gael_schematic_wire_delete ( GaelComponentInterface *icomp )
{
  DiaCanvasItem *item;

  item = DIA_CANVAS_ITEM(icomp);
  gael_schematic_utils_selection_rem(item);
  dia_canvas_group_remove_item(item->parent,item);
}

static gchar *
gael_schematic_wire_save ( GaelComponentInterface *icomp,
			   gchar                  *prepend )
{
  /*
  gpointer data[2];
  */
  GaelSchematicWire *wire = GAEL_SCHEMATIC_WIRE(icomp);
  GString *string = g_string_new("");

  g_string_append(string, prepend);
  g_string_append_printf(string, "<schematic x1=\"%f\" y1=\"%f\" x2=\"%f\" y2=\"%f\">\n",
			 wire->con_one->pos.x, wire->con_one->pos.y,
			 wire->con_two->pos.x, wire->con_two->pos.y);

  /*
  data[0] = string;
  data[1] = prepend;
  g_hash_table_foreach(wire->properties,
		       (GHFunc)gael_schematic_symbol_save_foreach,
		       data);
  */

  g_string_append(string, prepend);
  g_string_append(string, "</schematic>\n");

  return g_string_free(string,FALSE);
}

static gboolean
gael_schematic_wire_load ( GaelComponentInterface *icomp,
			   xmlDocPtr               doc,
			   xmlNodePtr              cur )
{
  char          *x, *y;

  /* Move to the right place the con_one */
  x = xmlGetProp(cur,"x1");
  y = xmlGetProp(cur,"y1");
  /* dia_canvas_item_move(DIA_CANVAS_ITEM(GAEL_SCHEMATIC_WIRE(icomp)->con_one),atof(x),atof(y)); */
  g_object_set(G_OBJECT(icomp),
	       "x1", atof(x),
	       "y1", atof(y),
	       NULL);
  free(x);
  free(y);

  /* Move to the right place the con_two */
  x = xmlGetProp(cur,"x2");
  y = xmlGetProp(cur,"y2");
  /* dia_canvas_item_move(DIA_CANVAS_ITEM(GAEL_SCHEMATIC_WIRE(icomp)->con_two),atof(x),atof(y)); */
  g_object_set(G_OBJECT(icomp),
	       "x2", atof(x),
	       "y2", atof(y),
	       NULL);
  free(x);
  free(y);

  return TRUE;
}
