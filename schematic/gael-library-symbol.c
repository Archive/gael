/* gael-library-symbol.c
 * Copyright (C) 2001, 2002 Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <library/gael-librarian.h>

#include <general/gael-general.h>
#include <loaders/gael-loader.h>
#include "gael-xml-symbol.h"
#include <util/gael-intl.h>
#include <string.h>
#include <gmodule.h>
#include <math.h>

/* properties */
enum {
  PROP_NONE,
  PROP_LAST
};

extern GdkColor line_color;
extern GdkColor fill_color;

static void gael_library_symbol_init         (GaelLibrarySymbol *symbol);
static void gael_library_symbol_class_init   (GaelLibrarySymbolClass *klass);
static void gael_library_symbol_finalize     (GObject *object);

static void gael_library_symbol_set_property (GObject *object,
					      guint property_id,
					      const GValue *value,
					      GParamSpec *pspec);
static void gael_library_symbol_get_property (GObject *object,
					      guint property_id,
					      GValue *value,
					      GParamSpec *pspec);

static gboolean gael_library_symbol_load    (GaelLibraryElement *element);
static gboolean gael_library_symbol_display (GaelLibraryElement *element,
					     GtkWidget          *widget);

GaelLibraryElement *get_new_elem (gchar *name,
				  gchar *path,
				  gchar *lib);

static GaelLibraryElementClass *parent_class = NULL;

GType
gael_library_symbol_get_type (void)
{
  static GtkType gael_library_symbol_type = 0;

  if (!gael_library_symbol_type)
    {
      static const GTypeInfo gael_library_symbol_info =
      {
        sizeof (GaelLibrarySymbolClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) gael_library_symbol_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,
        
        sizeof (GaelLibrarySymbol),
        0, /* n_preallocs */
        (GInstanceInitFunc) gael_library_symbol_init,
      };

      gael_library_symbol_type = g_type_register_static (GAEL_TYPE_LIBRARY_ELEMENT,
							 "GaelLibrarySymbol",
							 &gael_library_symbol_info,
							 0);
    }
  return gael_library_symbol_type;
}

static void
gael_library_symbol_class_init (GaelLibrarySymbolClass *klass)
{
  GObjectClass            *object_class;
  GaelLibraryElementClass *element_class;

  object_class  = G_OBJECT_CLASS(klass);
  element_class = GAEL_LIBRARY_ELEMENT_CLASS(klass);
  parent_class  = g_type_class_ref(GAEL_TYPE_LIBRARY_ELEMENT);

  object_class->finalize = gael_library_symbol_finalize;

  object_class->set_property = gael_library_symbol_set_property;
  object_class->get_property = gael_library_symbol_get_property;

  element_class->load    = gael_library_symbol_load;
  element_class->display = gael_library_symbol_display;
}

static void
gael_library_symbol_init (GaelLibrarySymbol *symbol)
{
}

static void
gael_library_symbol_finalize (GObject *object)
{
  GaelLibrarySymbol *symbol = GAEL_LIBRARY_SYMBOL(object);

  while( symbol->geometries != NULL ) {
    g_free(symbol->geometries->data);
    symbol->geometries = g_list_delete_link(symbol->geometries,symbol->geometries);
  }

  G_OBJECT_CLASS(parent_class)->finalize(object);
}

static void
gael_library_symbol_set_property (GObject *object,
				  guint property_id,
				  const GValue *value,
				  GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
gael_library_symbol_get_property (GObject *object,
				  guint property_id,
				  GValue *value,
				  GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static gboolean
gael_library_symbol_load (GaelLibraryElement *element)
{
  guchar *buffer;
  GaelLibrarySymbol *symbol = GAEL_LIBRARY_SYMBOL(element);

  /*
   * Do some check.
   */
  if( (element->name == NULL) ||
      (element->path == NULL) ||
      (element->libpath == NULL) ) {
    g_message("Missing a data to load a symbol");
    return FALSE;
  }

  /*
   * Load the XML.
   */
  buffer = gael_loader_load_element(element->libpath, element->path, element->name);
  if( buffer == NULL )
    return FALSE;

  /*
   * Parse the XML
   */
  gael_xml_symbol_parse(buffer,symbol);
  g_free(buffer);

  return TRUE;
}

#define RATIO(x) ceil((x)*ratio)

static gboolean
gael_library_symbol_display (GaelLibraryElement *element,
			     GtkWidget          *widget)
{
  GaelLibrarySymbol    *symbol;
  DiaRectangle          bounds;
  GaelGeom             *geom;
  GList                *list;
  gfloat                xratio, yratio, ratio;
  gdouble               dangle;

  symbol = GAEL_LIBRARY_SYMBOL(element);

  /*
   * Now get the element's bounds.
   */

  bounds.left   = 0;
  bounds.right  = 0;
  bounds.top    = 0;
  bounds.bottom = 0;

  if( symbol != NULL ) {

    list = symbol->geometries;
    while( list != NULL ) {
      geom = (GaelGeom*)list->data;
      switch( geom->type ) {

      case GAEL_POINT:
	g_message("%s(%i) GAEL_POINT todo",__FILE__,__LINE__);
	break;

      case GAEL_LINE:

	if( bounds.left > geom->line.x1 )
	  bounds.left = geom->line.x1;
	if( bounds.right < geom->line.x1 )
	  bounds.right = geom->line.x1;

	if( bounds.left > geom->line.x2 )
	  bounds.left = geom->line.x2;
	if( bounds.right < geom->line.x2 )
	  bounds.right = geom->line.x2;

	if( bounds.top > geom->line.y1 )
	  bounds.top = geom->line.y1;
	if( bounds.bottom < geom->line.y1 )
	  bounds.bottom = geom->line.y1;

	if( bounds.top > geom->line.y2 )
	  bounds.top = geom->line.y2;
	if( bounds.bottom < geom->line.y2 )
	  bounds.bottom = geom->line.y2;

	break;

      case GAEL_LINES:
	g_message("%s(%i) GAEL_LINES todo",__FILE__,__LINE__);
	break;

      case GAEL_RECTANGLE:

	if( bounds.left > geom->rectangle.left )
	  bounds.left = geom->rectangle.left;

	if( bounds.right < geom->rectangle.right )
	  bounds.right = geom->rectangle.right;

	if( bounds.top > geom->rectangle.top )
	  bounds.top = geom->rectangle.top;

	if( bounds.bottom < geom->rectangle.bottom )
	  bounds.bottom = geom->rectangle.bottom;

	break;

      case GAEL_ARC:

	/* TODO: exact computation (here only the full ellipse is considered) */

	if( bounds.left > geom->arc.x - geom->arc.width/2 )
	  bounds.left = geom->arc.x - geom->arc.width/2;

	if( bounds.right < geom->arc.x + geom->arc.width/2 )
	  bounds.right = geom->arc.x + geom->arc.width/2;

	if( bounds.top > geom->arc.y - geom->arc.height/2 )
	  bounds.top = geom->arc.y - geom->arc.height/2;

	if( bounds.bottom < geom->arc.y + geom->arc.height/2 )
	  bounds.bottom = geom->arc.y + geom->arc.height/2;

	break;

      case GAEL_POLYGON:
	g_message("%s(%i) GAEL_POLYGON todo",__FILE__,__LINE__);
	break;

      case GAEL_ATTRACT:
      case GAEL_ATTRACTED:
      case GAEL_CONNECTION:

	if( bounds.left > geom->attract.x )
	  bounds.left = geom->attract.x;

	if( bounds.right < geom->attract.x )
	  bounds.right = geom->attract.x;

	if( bounds.top > geom->attract.y )
	  bounds.top = geom->attract.y;

	if( bounds.bottom < geom->attract.y )
	  bounds.bottom = geom->attract.y;

	break;

      default:
	break;

      }
      list = list->next;
    }

    xratio = widget->allocation.width  / (bounds.right  - bounds.left ) * 0.9;
    yratio = widget->allocation.height / (bounds.bottom - bounds.top  ) * 0.9;

    ratio = xratio;
    if( yratio < xratio ) {
      ratio = yratio;
    }

    /*
    g_message("Bound box: %f %f %f %f",bounds.left,bounds.top,bounds.right,bounds.bottom);
    g_message("Ratio: %f %f %f",xratio,yratio,ratio);
    */

    list = symbol->geometries;
    while( list != NULL ) {
      geom = (GaelGeom*)list->data;
      switch( geom->type ) {

      case GAEL_POINT:
	g_message("%s(%i) GAEL_POINT todo",__FILE__,__LINE__);
	break;

      case GAEL_LINE:
	gdk_draw_line(widget->window,
		      widget->style->fg_gc[GTK_WIDGET_STATE (widget)],
		      widget->allocation.width/2  + RATIO(geom->line.x1),
		      widget->allocation.height/2 + RATIO(geom->line.y1),
		      widget->allocation.width/2  + RATIO(geom->line.x2),
		      widget->allocation.height/2 + RATIO(geom->line.y2));
	break;

      case GAEL_LINES:
	g_message("%s(%i) GAEL_LINES todo",__FILE__,__LINE__);
	break;

      case GAEL_RECTANGLE:
	if( geom->rectangle.filled ) {
	  gdk_colormap_alloc_color (gtk_widget_get_colormap(widget), &fill_color, FALSE, TRUE);
	  gdk_gc_set_foreground(widget->style->fg_gc[GTK_WIDGET_STATE (widget)], &fill_color);

 	  gdk_draw_rectangle(widget->window,
			     widget->style->fg_gc[GTK_WIDGET_STATE (widget)], TRUE,
			     widget->allocation.width/2  + RATIO(geom->rectangle.left),
			     widget->allocation.height/2 + RATIO(geom->rectangle.top),
			     RATIO(geom->rectangle.right  - geom->rectangle.left),
			     RATIO(geom->rectangle.bottom - geom->rectangle.top));	  
	}
	gdk_colormap_alloc_color (gtk_widget_get_colormap(widget), &line_color, FALSE, TRUE);
	gdk_gc_set_foreground(widget->style->fg_gc[GTK_WIDGET_STATE (widget)], &line_color);

	gdk_draw_rectangle(widget->window,
			   widget->style->fg_gc[GTK_WIDGET_STATE (widget)], FALSE,
			   widget->allocation.width/2  + RATIO(geom->rectangle.left),
			   widget->allocation.height/2 + RATIO(geom->rectangle.top),
			   RATIO(geom->rectangle.right  - geom->rectangle.left),
			   RATIO(geom->rectangle.bottom - geom->rectangle.top));
	break;

      case GAEL_ARC:
	dangle = geom->arc.angle2 - geom->arc.angle1;
	if (dangle < 0)
	  dangle += 360.0;

	if( geom->rectangle.filled ) {
	  gdk_colormap_alloc_color (gtk_widget_get_colormap(widget), &fill_color, FALSE, TRUE);
	  gdk_gc_set_foreground(widget->style->fg_gc[GTK_WIDGET_STATE (widget)], &fill_color);

	  gdk_draw_arc(widget->window,
		       widget->style->fg_gc[GTK_WIDGET_STATE (widget)], TRUE,
		       widget->allocation.width/2  + RATIO(geom->arc.x - geom->arc.width/2),
		       widget->allocation.height/2 + RATIO(geom->arc.y - geom->arc.height/2),
		       RATIO(geom->arc.width),
		       RATIO(geom->arc.height),
		       (int) (geom->arc.angle1 * 64), (int) (dangle * 64));
	}
	gdk_colormap_alloc_color (gtk_widget_get_colormap(widget), &line_color, FALSE, TRUE);
	gdk_gc_set_foreground(widget->style->fg_gc[GTK_WIDGET_STATE (widget)], &line_color);
	gdk_draw_arc(widget->window,
		     widget->style->fg_gc[GTK_WIDGET_STATE (widget)], FALSE,
		     widget->allocation.width/2  + RATIO(geom->arc.x - geom->arc.width/2),
		     widget->allocation.height/2 + RATIO(geom->arc.y - geom->arc.height/2),
		     RATIO(geom->arc.width),
		     RATIO(geom->arc.height),
		     (int) (geom->arc.angle1 * 64), (int) (dangle * 64));
	break;

      case GAEL_POLYGON:
	g_message("%s(%i) GAEL_POLYGON todo",__FILE__,__LINE__);
	break;

      case GAEL_ATTRACT:
      case GAEL_ATTRACTED:
      case GAEL_CONNECTION:

	if( bounds.left > geom->attract.x )
	  bounds.left = geom->attract.x;

	if( bounds.right < geom->attract.x )
	  bounds.right = geom->attract.x;

	if( bounds.top > geom->attract.y )
	  bounds.top = geom->attract.y;

	if( bounds.bottom < geom->attract.y )
	  bounds.bottom = geom->attract.y;

	break;

      default:
	break;

      }
      list = list->next;
    }
  }
  return FALSE;
}


/*
 * Module things.
 */

G_MODULE_EXPORT GaelLibraryElement *
get_new_elem (gchar *name,
	      gchar *path,
	      gchar *lib)
{
  return g_object_new(GAEL_TYPE_LIBRARY_SYMBOL,
		      "name",name,
		      "path",path,
		      "libpath",lib,
		      NULL);
}
