/* gael-xml-symbol.c
 * Copyright (C) 2001, 2002 Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <gtk/gtk.h>
#include <stdlib.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlmemory.h>
#include <string.h>

#include "gael-xml-symbol.h"
#include <general/gael-general.h>

typedef enum {
  GXS_START,
  GXS_SYMBOL,
  GXS_POINT,
  GXS_LINE,
  GXS_LINES,
  GXS_RECTANGLE,
  GXS_ARC,
  GXS_POLYGON,
  GXS_TEXT,
  GXS_ATTRACT,
  GXS_ATTRACTED,
  GXS_CONNECTION,
  GXS_PROPERTY,
  GXS_ERROR
} GaelXmlSymbolState;

typedef struct _GaelXmlSymbolParserData GaelXmlSymbolParserData;

struct _GaelXmlSymbolParserData {

  GaelXmlSymbolState state;
  GList *state_list;

  GaelLibrarySymbol *symbol;  

  GaelGeom *geom;
  GList    *geom_list;
};

static void gael_xml_symbol_start_document (GaelXmlSymbolParserData *ctx);
static void gael_xml_symbol_end_document   (GaelXmlSymbolParserData *ctx);
static void gael_xml_symbol_start_element  (GaelXmlSymbolParserData *ctx,
					    const xmlChar *name,
					    const xmlChar **atts);
static void gael_xml_symbol_end_element    (GaelXmlSymbolParserData *ctx,
					    const xmlChar *name);
static void gael_xml_symbol_characters     (GaelXmlSymbolParserData *ctx,
					    const xmlChar *ch,
					    int len);

xmlSAXHandler GaelXmlSymbolParserStruct = {
  NULL, //internalSubsetDebug,
  NULL, //isStandaloneDebug,
  NULL, //hasInternalSubsetDebug,
  NULL, //hasExternalSubsetDebug,
  NULL, //resolveEntityDebug,
  NULL, //getEntityDebug,
  NULL, //entityDeclDebug,
  NULL, //notationDeclDebug,
  NULL, //attributeDeclDebug,
  NULL, //elementDeclDebug,
  NULL, //unparsedEntityDeclDebug,
  NULL, //setDocumentLocatorDebug,
  (startDocumentSAXFunc)gael_xml_symbol_start_document,
  (endDocumentSAXFunc)gael_xml_symbol_end_document,
  (startElementSAXFunc)gael_xml_symbol_start_element,
  (endElementSAXFunc)gael_xml_symbol_end_element,
  NULL, //referenceDebug,
  (charactersSAXFunc)gael_xml_symbol_characters,
  NULL, //ignorableWhitespaceDebug,
  NULL, //processingInstructionDebug,
  NULL, //commentDebug,
  NULL, //warningDebug,
  NULL, //errorDebug,
  NULL, //fatalErrorDebug,
  NULL, //getParameterEntityDebug,
};

xmlSAXHandlerPtr GaelXmlSymbolParserHandler = &GaelXmlSymbolParserStruct;

void
gael_xml_symbol_parse(guchar *buffer,GaelLibrarySymbol* symbol)
{
  GaelXmlSymbolParserData ctx;

  ctx.symbol = symbol;
  xmlSAXUserParseMemory (GaelXmlSymbolParserHandler,
			 &ctx,
			 buffer, strlen(buffer));
}

/*
 * private functions.
 */

static void
gael_xml_symbol_start_document (GaelXmlSymbolParserData *ctx)
{
  ctx->state = GXS_START;
  ctx->state_list = NULL;
  ctx->geom       = NULL;
  ctx->geom_list  = NULL;
}

static void
gael_xml_symbol_end_document (GaelXmlSymbolParserData *ctx)
{
  if( ctx->state_list != NULL ) {
    ctx->state = GXS_ERROR;
    while( ctx->state_list != NULL ) {
      ctx->state_list =
	g_list_delete_link(ctx->state_list,ctx->state_list);
    }
  }

  while( ctx->geom_list != NULL ) {
    ctx->geom_list =
      g_list_delete_link(ctx->geom_list,ctx->geom_list);
  }
}

static void
gael_xml_symbol_start_element (GaelXmlSymbolParserData *ctx,
			       const xmlChar *name,
			       const xmlChar **atts)
{
  gint i;
  GaelXmlSymbolState *oldstate;

  /*
   * Pushes the current state.
   */
  oldstate = g_new0(GaelXmlSymbolState,1);
  *oldstate = ctx->state;
  ctx->state_list = g_list_append(ctx->state_list,oldstate);

  switch( ctx->state ) {

  case GXS_START:
    /*
     * On start, check we have a symbol tag,
     * and its name, else this is an error.
     */
    if( !strcmp(name,"gael:symbol") ) {
      ctx->state = GXS_SYMBOL;
      if( atts == NULL    ||
	  atts[0] == NULL ||
	  strcmp(atts[0],"name") ||
	  atts[1] == NULL ) {
	ctx->state = GXS_ERROR;
	g_message("%s(%i): Unknown XML tag %s (this makes me nervous, I'm giving up here) ",__FILE__,__LINE__,name);
	break;
      }
      GAEL_LIBRARY_ELEMENT(ctx->symbol)->name = strdup(atts[1]);
    } else {
      ctx->state = GXS_ERROR;
      g_message("%s(%i): Unknown XML tag %s (this makes me nervous, I'm giving up here) ",__FILE__,__LINE__,name);
    }
    break;

  case GXS_SYMBOL:
    /*
     * Point tag, get the x and y fields.
     */
    if( !strcmp(name,"gael:point") ) {
      ctx->state   = GXS_POINT;
      ctx->geom = g_new0(GaelGeom,1);
      ctx->geom->type = GAEL_POINT;
      if( atts != NULL ) {
	for( i=0; atts[2*i] != NULL; i++ ) {
	  if( !strcmp(atts[2*i],"x") ) {
	    ctx->geom->point.x = atof(atts[2*i+1]);
	  } else if( !strcmp(atts[2*i],"y") ) {
	    ctx->geom->point.y = atof(atts[2*i+1]);
	  } else {
	    ctx->state = GXS_ERROR;
	    g_message("%s(%i): Unknown XML tag %s (this makes me nervous, I'm giving up here) ",__FILE__,__LINE__,name);
	  }
	}
      }

    /*
     * Line tag, get x1, y1, x2, y2
     */
    } else if( !strcmp(name,"gael:line") ) {
      ctx->state   = GXS_LINE;
      ctx->geom = g_new0(GaelGeom,1);
      ctx->geom->type = GAEL_LINE;
      if( atts != NULL ) {
	for( i=0; atts[2*i] != NULL; i++ ) {
	  if( !strcmp(atts[2*i],"x1") ) {
	    ctx->geom->line.x1 = atof(atts[2*i+1]);
	  } else if( !strcmp(atts[2*i],"y1") ) {
	    ctx->geom->line.y1 = atof(atts[2*i+1]);
	  } else if( !strcmp(atts[2*i],"x2") ) {
	    ctx->geom->line.x2 = atof(atts[2*i+1]);
	  } else if( !strcmp(atts[2*i],"y2") ) {
	    ctx->geom->line.y2 = atof(atts[2*i+1]);
	  } else {
	    ctx->state = GXS_ERROR;
	    g_message("%s(%i): Unknown XML tag %s (this makes me nervous, I'm giving up here) ",__FILE__,__LINE__,name);
	  }
	}
      }
      
    /*
     * Lines tag, TODO
     */
    } else if( !strcmp(name,"gael:lines") ) {
      ctx->state = GXS_LINES;
      g_message("%s(%i): implement the lines tag",__FILE__,__LINE__);

    /*
     * Rectangles tag, get the filled, left, top, right and bottom
     * properties.
     */
    } else if( !strcmp(name,"gael:rectangle") ) {
      ctx->state   = GXS_RECTANGLE;
      ctx->geom = g_new0(GaelGeom,1);
      ctx->geom->type = GAEL_RECTANGLE;
      if( atts != NULL ) {
	for( i=0; atts[2*i] != NULL; i++ ) {
	  if( !strcmp(atts[2*i],"filled") ) {
	    if( !strcmp(atts[2*i+1],"TRUE") ) {
	      ctx->geom->rectangle.filled = TRUE;
	    } else if( !strcmp(atts[2*i+1],"FALSE") ) {
	      ctx->geom->rectangle.filled = TRUE;
	    } else {
	      ctx->state = GXS_ERROR;
	      g_message("%s(%i): Unknown XML tag %s (this makes me nervous, I'm giving up here) ",__FILE__,__LINE__,name);
	    }
	  } else if( !strcmp(atts[2*i],"left") ) {
	    ctx->geom->rectangle.left   = atof(atts[2*i+1]);
	  } else if( !strcmp(atts[2*i],"top") ) {
	    ctx->geom->rectangle.top    = atof(atts[2*i+1]);
	  } else if( !strcmp(atts[2*i],"right") ) {
	    ctx->geom->rectangle.right  = atof(atts[2*i+1]);
	  } else if( !strcmp(atts[2*i],"bottom") ) {
	    ctx->geom->rectangle.bottom = atof(atts[2*i+1]);
	  } else {
	    ctx->state = GXS_ERROR;
	    g_message("%s(%i): Unknown XML tag %s (this makes me nervous, I'm giving up here) ",__FILE__,__LINE__,name);
	  }
	}
      }

    /*
     * Arc tag, get filled, x, y, width, height,
     * angle1, angle2 properties.
     */
    } else if( !strcmp(name,"gael:arc") ) {
      ctx->state   = GXS_ARC;
      ctx->geom = g_new0(GaelGeom,1);
      ctx->geom->type = GAEL_ARC;
      if( atts != NULL ) {
	for( i=0; atts[2*i] != NULL; i++ ) {
	  if( !strcmp(atts[2*i],"filled") ) {
	    if( !strcmp(atts[2*i+1],"TRUE") ) {
	      ctx->geom->arc.filled = TRUE;
	    } else if( !strcmp(atts[2*i+1],"FALSE") ) {
	      ctx->geom->arc.filled = FALSE;
	    } else {
	      ctx->state = GXS_ERROR;
	      g_message("%s(%i): Unknown XML tag %s (this makes me nervous, I'm giving up here) ",__FILE__,__LINE__,name);
	    }
	  } else if( !strcmp(atts[2*i],"x") ) {
	    ctx->geom->arc.x      = atof(atts[2*i+1]);
	  } else if( !strcmp(atts[2*i],"y") ) {
	    ctx->geom->arc.y      = atof(atts[2*i+1]);
	  } else if( !strcmp(atts[2*i],"width") ) {
	    ctx->geom->arc.width  = atof(atts[2*i+1]);
	  } else if( !strcmp(atts[2*i],"height") ) {
	    ctx->geom->arc.height = atof(atts[2*i+1]);
	  } else if( !strcmp(atts[2*i],"angle1") ) {
	    ctx->geom->arc.angle1 = atof(atts[2*i+1]);
	  } else if( !strcmp(atts[2*i],"angle2") ) {
	    ctx->geom->arc.angle2 = atof(atts[2*i+1]);
	  } else {
	    ctx->state = GXS_ERROR;
	    g_message("%s(%i): Unknown XML tag %s (this makes me nervous, I'm giving up here) ",__FILE__,__LINE__,name);
	    g_message("couldn't understand attribute %s with value %s",atts[2*i],atts[2*i+1]);
	  }
	}
      }

    /*
     * Polygon tag, TODO
     */
    } else if( !strcmp(name,"gael:polygon") ) {
      ctx->state = GXS_POLYGON;
      g_message("%s(%i): implement the polygon tag",__FILE__,__LINE__);

    /*
     * 
     */
    } else if( !strcmp(name,"gael:text") ) {
      ctx->state = GXS_TEXT;
      ctx->geom = g_new0(GaelGeom,1);
      ctx->geom->type = GAEL_TEXT;
      if( atts != NULL ) {
	for( i=0; atts[2*i] != NULL; i++ ) {
	  if( !strcmp(atts[2*i],"x") ) {
	    ctx->geom->text.x = atof(atts[2*i+1]);
	  } else if( !strcmp(atts[2*i],"y") ) {
	    ctx->geom->text.y = atof(atts[2*i+1]);
	  } else if( !strcmp(atts[2*i],"text") ) {
	    ctx->geom->text.text = strdup(atts[2*i+1]);
	  } else {
	    ctx->state = GXS_ERROR;
	    g_message("%s(%i): Unknown XML tag %s (this makes me nervous, I'm giving up here) ",__FILE__,__LINE__,name);
	  }
	}
      }

    /*
     * attract tag, get (x,y)
     */
    } else if( !strcmp(name,"gael:attract") ) {
      ctx->state = GXS_ATTRACT;
      ctx->geom = g_new0(GaelGeom,1);
      ctx->geom->type = GAEL_ATTRACT;
      if( atts != NULL ) {
	for( i=0; atts[2*i] != NULL; i++ ) {
	  if( !strcmp(atts[2*i],"x") ) {
	    ctx->geom->attract.x = atof(atts[2*i+1]);
	  } else if( !strcmp(atts[2*i],"y") ) {
	    ctx->geom->attract.y = atof(atts[2*i+1]);
	  } else {
	    ctx->state = GXS_ERROR;
	    g_message("%s(%i): Unknown XML tag %s (this makes me nervous, I'm giving up here) ",__FILE__,__LINE__,name);
	  }
	}
      }

    /*
     * attracted tag, get (x,y)
     */
    } else if( !strcmp(name,"gael:attracted") ) {
      ctx->state = GXS_ATTRACTED;
      ctx->geom = g_new0(GaelGeom,1);
      ctx->geom->type = GAEL_ATTRACTED;
      if( atts != NULL ) {
	for( i=0; atts[2*i] != NULL; i++ ) {
	  if( !strcmp(atts[2*i],"x") ) {
	    ctx->geom->attracted.x = atof(atts[2*i+1]);
	  } else if( !strcmp(atts[2*i],"y") ) {
	    ctx->geom->attracted.y = atof(atts[2*i+1]);
	  } else {
	    ctx->state = GXS_ERROR;
	    g_message("%s(%i): Unknown XML tag %s (this makes me nervous, I'm giving up here) ",__FILE__,__LINE__,name);
	  }
	}
      }

    /*
     * Connection tag, get (x,y)
     */
    } else if( !strcmp(name,"gael:connection") ) {
      ctx->state = GXS_CONNECTION;
      ctx->geom = g_new0(GaelGeom,1);
      ctx->geom->type = GAEL_CONNECTION;
      if( atts != NULL ) {
	for( i=0; atts[2*i] != NULL; i++ ) {
	  if( !strcmp(atts[2*i],"x") ) {
	    ctx->geom->connection.x = atof(atts[2*i+1]);
	  } else if( !strcmp(atts[2*i],"y") ) {
	    ctx->geom->connection.y = atof(atts[2*i+1]);
	  } else if( !strcmp(atts[2*i],"id") ) {
	    ctx->geom->connection.id = atoi(atts[2*i+1]);
	  } else {
	    ctx->state = GXS_ERROR;
	    g_message("%s(%i): Unknown XML tag %s (this makes me nervous, I'm giving up here) ",__FILE__,__LINE__,name);
	  }
	}
      }

    /*
     * Property tag, get (name,value)
     */
    } else if( !strcmp(name,"gael:property") ) {
      ctx->state = GXS_PROPERTY;
      ctx->geom = g_new0(GaelGeom,1);
      ctx->geom->type = GAEL_PROPERTY;
      if( atts != NULL ) {
	for( i=0; atts[2*i] != NULL; i++ ) {
	  if( !strcmp(atts[2*i],"x") ) {
	    ctx->geom->property.x = atof(atts[2*i+1]);
	  } else if( !strcmp(atts[2*i],"y") ) {
	    ctx->geom->property.y = atof(atts[2*i+1]);
	  } else if( !strcmp(atts[2*i],"name") ) {
	    ctx->geom->property.name = strdup(atts[2*i+1]);
	  } else if( !strcmp(atts[2*i],"value") ) {
	    ctx->geom->property.value = strdup(atts[2*i+1]);
	  } else {
	    ctx->state = GXS_ERROR;
	    g_message("%s(%i): Unknown XML tag %s (this makes me nervous, I'm giving up here) ",__FILE__,__LINE__,name);
	  }
	}
      }

    } else {
      ctx->state = GXS_ERROR;
      g_message("%s(%i): Unknown XML tag %s (this makes me nervous, I'm giving up here) ",__FILE__,__LINE__,name);
    }
    break;

  case GXS_ERROR:
    break;

  default:
    ctx->state = GXS_ERROR;
    g_message("%s(%i): Unknown XML tag %s (this makes me nervous, I'm giving up here) ",__FILE__,__LINE__,name);
    break;
  }
}

static void
gael_xml_symbol_end_element (GaelXmlSymbolParserData *ctx,
			     const xmlChar *name)
{
  switch( ctx->state ) {

  case GXS_ERROR:
    return;

  case GXS_POINT:
  case GXS_LINE:
  case GXS_LINES:
  case GXS_RECTANGLE:
  case GXS_ARC:
  case GXS_POLYGON:
  case GXS_TEXT:
  case GXS_ATTRACT:
  case GXS_ATTRACTED:
  case GXS_CONNECTION:
  case GXS_PROPERTY:
    ctx->symbol->geometries = g_list_append(ctx->symbol->geometries,ctx->geom);
    ctx->geom = NULL;
    break;

  case GXS_SYMBOL:
    break;

  default:
    break;
  }

  /*
   * Pops the state.
   */
  ctx->state = *((GaelXmlSymbolState*)g_list_last(ctx->state_list)->data);
  ctx->state_list = g_list_delete_link(ctx->state_list,g_list_last(ctx->state_list));
}

static void
gael_xml_symbol_characters (GaelXmlSymbolParserData *ctx,
			    const xmlChar *ch,
			    int len)
{
}
