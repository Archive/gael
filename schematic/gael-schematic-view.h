/* gael-schematic-view.h
 * Copyright (C) 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GAEL_SCHEMATIC_VIEW_H__
#define __GAEL_SCHEMATIC_VIEW_H__

#include <gtk/gtk.h>
#include <glade/glade.h>
#include <dia-newcanvas/dia-newcanvas.h>

#define GAEL_TYPE_SCHEMATIC_VIEW            (gael_schematic_view_get_type ())
#define GAEL_SCHEMATIC_VIEW(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GAEL_TYPE_SCHEMATIC_VIEW, GaelSchematicView))
#define GAEL_SCHEMATIC_VIEW_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GAEL_TYPE_SCHEMATIC_VIEW, GaelSchematicViewClass))
#define GAEL_IS_SCHEMATIC_VIEW(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GAEL_TYPE_SCHEMATIC_VIEW))
#define GAEL_IS_SCHEMATIC_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GAEL_TYPE_SCHEMATIC_VIEW))
#define GAEL_SCHEMATIC_VIEW_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),   GAEL_TYPE_SCHEMATIC_VIEW, GaelSchematicViewClass))

typedef enum {
  GAEL_SCHEMATIC_NOTHING,
  GAEL_SCHEMATIC_MOVE,
  GAEL_SCHEMATIC_PLACE_COMPONENT,
  GAEL_SCHEMATIC_PLACE_WIRE_1,
  GAEL_SCHEMATIC_PLACE_WIRE_2
} GaelSchematicState;

typedef struct _GaelSchematicView      GaelSchematicView;
typedef struct _GaelSchematicViewClass GaelSchematicViewClass;

struct _GaelSchematicView
{
  DiaCanvasViewGdk object;

  GaelSchematicState state;
  DiaPoint point;

  BonoboUIComponent *component;

  gpointer *parent;
};

struct _GaelSchematicViewClass
{
  DiaCanvasViewGdkClass parent_class;
};

GType gael_schematic_view_get_type (void);

#endif
