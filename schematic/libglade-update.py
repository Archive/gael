#!/usr/bin/env python2.2

f = open('gael2.glade','r')

lines = f.readlines()

i = 0
for txt in lines:
    if txt == "<glade-interface>\n":
        lines.insert(i+1,'<requires lib="gladedianewcanvas2"/>\n')
    if txt == '                <widget class="GtkDrawingArea" id="DiaCanvasView">\n':
        lines[i] = '                <widget class="DiaCanvasViewGdk" id="DiaCanvasView">\n'
    if txt == '		<widget class="GtkDrawingArea" id="DiaCanvasView">\n':
        lines[i] = '                <widget class="DiaCanvasViewGdk" id="DiaCanvasView">\n'
    i = i+1

f.close()

f = open('gael2.glade','w')
f.writelines(lines)
f.close
