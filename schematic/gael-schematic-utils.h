/* gael-schematic-utils.h
 * Copyright (C) 2002 Xavier Ordoquy
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GAEL_SCHEMATIC_UTILS_H__
#define __GAEL_SCHEMATIC_UTILS_H__

#include <gtk/gtk.h>
#include <dia-newcanvas/dia-newcanvas.h>
#include <general/gael-view-interface.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void gael_schematic_utils_selection_add    ( DiaCanvasItem *item );
void gael_schematic_utils_selection_rem    ( DiaCanvasItem *item );
void gael_schematic_utils_selection_clear  ( DiaCanvas *canvas );
void gael_schematic_utils_selection_delete ( DiaCanvas *canvas );

gint gael_schematic_utils_grid_event       ( DiaCanvasItem *item,
					     GdkEvent      *event,
					     DiaCanvasView *view,
					     gpointer       user_data );
gint gael_schematic_utils_event            ( DiaCanvasItem *item,
					     GdkEvent      *event,
					     DiaCanvasView *view,
					     gpointer       user_data );


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
