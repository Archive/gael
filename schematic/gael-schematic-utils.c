/* gael-schematic-utils.c
 * Copyright (C) 2002 Xavier Ordoquy
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gael-schematic-utils.h"
#include "gael-schematic-view.h"
#include "gael-schematic-model.h"
#include "gael-schematic-symbol.h"
#include "gael-schematic-wire.h"
#include "../general/gael-design.h"
#include <gdk/gdkkeysyms.h>

static void gael_schematic_utils_change_line_color  (DiaCanvasItem *item, GdkColor *color);
static void gael_schematic_utils_selection_color    (DiaCanvasItem *item, GdkColor *color);
static void gael_schematic_utils_selection_move     (DiaCanvasItem *item, DiaPoint *DaPoint);
static void gael_schematic_utils_selection_snapmove (DiaCanvasItem *item, DiaPoint *DaPoint);

static void
gael_schematic_utils_change_line_color ( DiaCanvasItem *item,
					 GdkColor      *color )
{
  GObjectClass *klass = G_OBJECT_GET_CLASS(item);

  if( g_object_class_find_property(klass,"line_color") != NULL ) {
    g_object_set(item,"line_color", color,NULL);
  }
}

static void
gael_schematic_utils_selection_move ( DiaCanvasItem *item,
				      DiaPoint      *DaPoint )
{
  dia_canvas_item_move (item, DaPoint->x, DaPoint->y);
  if ( GAEL_IS_SCHEMATIC_SYMBOL(item) ) {
    GaelComponent *comp;

    comp = GAEL_COMPONENT(GAEL_SCHEMATIC_SYMBOL(item)->parent);
    gael_design_changed (GAEL_DESIGN(comp->design),TRUE);
  }
}

static void
gael_schematic_utils_selection_snapmove ( DiaCanvasItem *item,
					  DiaPoint      *DaPoint )
{
  DiaPoint thisPoint;

  thisPoint.x = DaPoint[0].x;
  thisPoint.y = DaPoint[0].y;
  dia_canvas_item_snapmove (item, &(thisPoint.x), &(thisPoint.y));
  if( (thisPoint.x*thisPoint.x+thisPoint.y*thisPoint.y) <
      (DaPoint[1].x*DaPoint[1].x+DaPoint[1].y*DaPoint[1].y) ) {
    DaPoint[1].x = thisPoint.x;
    DaPoint[1].y = thisPoint.y;
  }
}

static void
gael_schematic_utils_selection_color ( DiaCanvasItem *item,
				       GdkColor      *color )
{
  if( DIA_IS_CANVAS_GROUP(item) ) {
    dia_canvas_group_foreach( DIA_CANVAS_GROUP(item),
			      (GFunc)gael_schematic_utils_change_line_color,
			      color );
  } else {
    gael_schematic_utils_change_line_color( item, color );
  }
}

void
gael_schematic_utils_selection_add ( DiaCanvasItem *item )
{
  gael_schematic_utils_selection_color ( item, GAEL_SCHEMATIC_MODEL(item->canvas)->line_color_selected );
  dia_canvas_add_selection ( item->canvas, item );
}

void
gael_schematic_utils_selection_rem ( DiaCanvasItem *item )
{
  gael_schematic_utils_selection_color ( item, GAEL_SCHEMATIC_MODEL(item->canvas)->line_color );
  dia_canvas_remove_from_selection ( item->canvas, item );
}

void
gael_schematic_utils_selection_clear ( DiaCanvas *canvas )
{
  g_list_foreach ( canvas->selection,
		   (GFunc)gael_schematic_utils_selection_color,
		   GAEL_SCHEMATIC_MODEL(canvas)->line_color );
  dia_canvas_clear_selection(canvas);
}

static void
gael_schematic_utils_selection_foreach_delete_item ( DiaCanvasItem *item,
						     gpointer       user_data )
{
  g_object_ref(item);

  if ( GAEL_IS_SCHEMATIC_SYMBOL(item) ) {
    GaelSchematicSymbol *symbol;

    symbol = GAEL_SCHEMATIC_SYMBOL(item);
    gael_design_rem_component(GAEL_DESIGN(GAEL_COMPONENT(symbol->parent)->design),
			      GAEL_COMPONENT(symbol->parent));
  }
  if ( GAEL_IS_SCHEMATIC_WIRE(item) ) {
    GaelSchematicWire *wire;

    wire = GAEL_SCHEMATIC_WIRE(item);
    gael_design_rem_component(GAEL_DESIGN(GAEL_COMPONENT(wire->parent)->design),
			      GAEL_COMPONENT(wire->parent));
  }
  g_object_unref(item);
}

void
gael_schematic_utils_selection_delete ( DiaCanvas *canvas )
{
  GList *list;

  if( canvas->selection != NULL ) {
    list = g_list_copy(canvas->selection);
    dia_canvas_clear_selection(canvas);
    g_list_foreach ( list,
		     (GFunc)gael_schematic_utils_selection_foreach_delete_item,
		     NULL );
    g_list_free(list);
  }
}

gint
gael_schematic_utils_grid_event ( DiaCanvasItem *item,
				  GdkEvent      *event,
				  DiaCanvasView *view,
				  gpointer       user_data )
{
  GaelSchematicView *gsview = GAEL_SCHEMATIC_VIEW(view);

  switch (event->type) {

  case GDK_BUTTON_PRESS:
    if( event->button.button == 1 ) {
      if( gsview->state == GAEL_SCHEMATIC_NOTHING ) {
	gael_schematic_utils_selection_clear (view->canvas);
	return TRUE;
      }
    }
    break;

  case GDK_BUTTON_RELEASE:
    break;

  case GDK_KEY_PRESS:
    if( (event->key.keyval == GDK_Delete) &&
	(view->canvas->selection != NULL) ) {
      gael_schematic_utils_selection_delete(view->canvas); 
      return TRUE;
    }
    break;

  default:
    break;
  }
  return FALSE;
}

gint
gael_schematic_utils_event ( DiaCanvasItem *item,
			     GdkEvent      *event,
			     DiaCanvasView *view,
			     gpointer user_data )
{
  GaelSchematicView *gsview = GAEL_SCHEMATIC_VIEW(view);

  switch (event->type) {

  case GDK_BUTTON_PRESS:
    gsview->point.x = event->button.x;
    gsview->point.y = event->button.y;
    if( event->button.button == 1 ) {
      if( gsview->state == GAEL_SCHEMATIC_NOTHING ) {
	gsview->state = GAEL_SCHEMATIC_MOVE;
	dia_canvas_view_grab(view,FALSE);
	if( item->selected == FALSE ) {
	  if( !(event->button.state & GDK_CONTROL_MASK) ) {
	    gael_schematic_utils_selection_clear(view->canvas);
	  }
	  gael_schematic_utils_selection_add(item);
	}
	return TRUE;
      }
      if( gsview->state == GAEL_SCHEMATIC_PLACE_WIRE_1 ) {
	gael_schematic_utils_selection_clear(view->canvas);
	gael_schematic_utils_selection_add(DIA_CANVAS_ITEM(GAEL_SCHEMATIC_WIRE(item)->con_two));
	gsview->state = GAEL_SCHEMATIC_PLACE_WIRE_2;
	DIA_CANVAS_VIEW(gsview)->event_item = DIA_CANVAS_ITEM(GAEL_SCHEMATIC_WIRE(item)->con_two);
	DIA_CANVAS_VIEW(gsview)->point_item = DIA_CANVAS_ITEM(GAEL_SCHEMATIC_WIRE(item)->con_two);
      }
    }
    break;

  case GDK_BUTTON_RELEASE:
    if( event->button.button == 1 ) {
      if( gsview->state == GAEL_SCHEMATIC_MOVE ) {
	gsview->state = GAEL_SCHEMATIC_NOTHING;
	dia_canvas_view_ungrab(view);
	dia_canvas_item_make_connection(item);
	return TRUE;
      }
      if( gsview->state == GAEL_SCHEMATIC_PLACE_COMPONENT ) {
	gsview->state = GAEL_SCHEMATIC_NOTHING;
	dia_canvas_view_ungrab(view);
	dia_canvas_item_make_connection(item);
	return TRUE;
      }
    }
    break;

  case GDK_MOTION_NOTIFY:
    if( (gsview->state == GAEL_SCHEMATIC_MOVE) ||
	(gsview->state == GAEL_SCHEMATIC_PLACE_WIRE_1) ||
	(gsview->state == GAEL_SCHEMATIC_PLACE_COMPONENT) ) {
      DiaPoint points[2];

      points[0].x = event->button.x - gsview->point.x;
      points[0].y = event->button.y - gsview->point.y;
      points[1].x = G_MAXDOUBLE;
      points[1].y = G_MAXDOUBLE;
      g_list_foreach ( view->canvas->selection,
		       (GFunc)gael_schematic_utils_selection_snapmove,
		       &points );
      g_list_foreach ( view->canvas->selection,
		       (GFunc)gael_schematic_utils_selection_move,
		       &(points[1]) );
      gsview->point.x += points[1].x;
      gsview->point.y += points[1].y;
      return TRUE;
    }

  default:
    return FALSE;
  }
  return FALSE;
}
