/* gael-schematic-conn.c
 * Copyright (C) 2001, 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <gtk/gtk.h>
#include "gael-schematic-conn.h"
#include "util/gael-intl.h"

/* properties */
enum {
  PROP_NONE,
  PROP_LAST
};

static void gael_schematic_conn_init       (GaelSchematicConn      *conn);
static void gael_schematic_conn_class_init (GaelSchematicConnClass *klass);
static void gael_schematic_conn_finalize   (GObject                *object);

static void gael_schematic_conn_set_property (GObject *object,
					     guint property_id,
					     const GValue *value,
					     GParamSpec *pspec);
static void gael_schematic_conn_get_property (GObject *object,
					     guint property_id,
					     GValue *value,
					     GParamSpec *pspec);

static void gael_schematic_conn_real_render  (DiaCanvasItem *item,
					     DiaRenderer *renderer,
					     DiaRectangle *rectangle);

static DiaCanvasConnectionClass *parent_class = NULL;


GtkType
gael_schematic_conn_get_type (void)
{
  static GtkType schematic_conn_type = 0;

  if (!schematic_conn_type)
    {
      static const GTypeInfo schematic_conn_info =
      {
	sizeof (GaelSchematicConnClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) gael_schematic_conn_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,
	
	sizeof (GaelSchematicConn),
	0, /* n_preallocs */
	(GInstanceInitFunc) gael_schematic_conn_init,
      };

      schematic_conn_type = g_type_register_static (DIA_TYPE_CANVAS_CONNECTION,
						    "GaelSchematicConn",
						    &schematic_conn_info, 0);
    }

  return schematic_conn_type;
}

static void
gael_schematic_conn_class_init (GaelSchematicConnClass *klass)
{
  GObjectClass *object_class;
  DiaCanvasItemClass *item_class;

  object_class = G_OBJECT_CLASS(klass);
  item_class   = DIA_CANVAS_ITEM_CLASS(klass);

  parent_class = g_type_class_ref(DIA_TYPE_CANVAS_CONNECTION);

  object_class->finalize = gael_schematic_conn_finalize;

  object_class->set_property = gael_schematic_conn_set_property;
  object_class->get_property = gael_schematic_conn_get_property;

  item_class->render       = gael_schematic_conn_real_render;

}


static void
gael_schematic_conn_init (GaelSchematicConn *conn)
{
  conn->show = FALSE;
}

static void
gael_schematic_conn_finalize(GObject *object)
{
  GaelSchematicConn *conn;

  g_return_if_fail(object != NULL);
  g_return_if_fail(GAEL_IS_SCHEMATIC_CONN(object));

  conn = GAEL_SCHEMATIC_CONN(object);

  if (G_OBJECT_CLASS(parent_class)->finalize)
    (* G_OBJECT_CLASS(parent_class)->finalize) (object);
}

static void
gael_schematic_conn_set_property (GObject *object,
				  guint property_id,
				  const GValue *value,
				  GParamSpec *pspec)
{
  //GaelSchematicConn *conn = GAEL_SCHEMATIC_CONN(object);

  switch (property_id) {
  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
gael_schematic_conn_get_property (GObject *object,
				  guint property_id,
				  GValue *value,
				  GParamSpec *pspec)
{
  G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
}


static void
gael_schematic_conn_real_render (DiaCanvasItem *item,
				DiaRenderer *renderer,
				DiaRectangle *rectangle)
{
  DiaCanvasConnection *connection = DIA_CANVAS_CONNECTION(item);
  GaelSchematicConn    *conn        = GAEL_SCHEMATIC_CONN(item);

  if( conn->show == TRUE ) {
    dia_renderer_set_color(renderer, &connection->fill_color);
    dia_renderer_draw_ellipse(renderer, TRUE, &connection->pos, 6, 6);
  }
}
