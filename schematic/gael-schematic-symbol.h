/* gael-schematic-symbol.h
 * Copyright (C) 2001, 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GAEL_SCHEMATIC_SYMBOL_H__
#define __GAEL_SCHEMATIC_SYMBOL_H__

#include <gtk/gtk.h>
#include <dia-newcanvas/dia-newcanvas.h>
#include <general/gael-component.h>
#include <library/gael-librarian.h>

#define GAEL_TYPE_SCHEMATIC_SYMBOL            (gael_schematic_symbol_get_type ())
#define GAEL_SCHEMATIC_SYMBOL(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GAEL_TYPE_SCHEMATIC_SYMBOL, GaelSchematicSymbol))
#define GAEL_SCHEMATIC_SYMBOL_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GAEL_TYPE_SCHEMATIC_SYMBOL, GaelSchematicSymbolClass))
#define GAEL_IS_SCHEMATIC_SYMBOL(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GAEL_TYPE_SCHEMATIC_SYMBOL))
#define GAEL_IS_SCHEMATIC_SYMBOL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GAEL_TYPE_SCHEMATIC_SYMBOL))
#define GAEL_SCHEMATIC_SYMBOL_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),   GAEL_TYPE_SCHEMATIC_SYMBOL, GaelSchematicSymbolClass))

typedef struct _GaelSchematicSymbol      GaelSchematicSymbol;
typedef struct _GaelSchematicSymbolClass GaelSchematicSymbolClass;

struct _GaelSchematicSymbol
{
  //GObject object;
  DiaCanvasGroup object;

  gchar *library;
  gchar *name;

  gpointer parent;

  GHashTable *properties;

  gpointer schematic;

  gdouble x,y;
};

struct _GaelSchematicSymbolClass
{
  //GObjectClass parent_class;
  DiaCanvasGroupClass parent_class;
};

GType  gael_schematic_symbol_get_type    (void);

GObject *gael_schematic_symbol_new(GaelComponent *component,
				   GaelLibrarian *librarian,
				   gpointer       group,
				   gchar         *lib,
				   gchar         *name);

#endif
