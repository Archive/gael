/* gael-schematic-wire.h
 * Copyright (C) 2002 Xavier Ordoquy
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GAEL_SCHEMATIC_WIRE_H__
#define __GAEL_SCHEMATIC_WIRE_H__

#include <gtk/gtk.h>
#include <dia-newcanvas/dia-newcanvas.h>
#include <general/gael-view-interface.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define GAEL_TYPE_SCHEMATIC_WIRE            (gael_schematic_wire_get_type ())
#define GAEL_SCHEMATIC_WIRE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GAEL_TYPE_SCHEMATIC_WIRE, GaelSchematicWire))
#define GAEL_SCHEMATIC_WIRE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GAEL_TYPE_SCHEMATIC_WIRE, GaelSchematicWireClass))
#define GAEL_IS_SCHEMATIC_WIRE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GAEL_TYPE_SCHEMATIC_WIRE))
#define GAEL_IS_SCHEMATIC_WIRE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GAEL_TYPE_SCHEMATIC_WIRE))
#define GAEL_SCHEMATIC_WIRE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),   GAEL_TYPE_SCHEMATIC_WIRE, GaelSchematicWireClass))

typedef struct _GaelSchematicWire      GaelSchematicWire;
typedef struct _GaelSchematicWireClass GaelSchematicWireClass;

struct _GaelSchematicWire
{
  DiaCanvasGroup object;

  gpointer schematic;

  DiaCanvasLine       *line;

  DiaCanvasConnection *con_one;
  DiaCanvasConnection *con_two;

  DiaCanvasAttract *atr_one;
  DiaCanvasAttract *atr_two;

  gpointer parent;
};

struct _GaelSchematicWireClass
{
  DiaCanvasGroupClass parent_class;
};

GType  gael_schematic_wire_get_type    (void);

GObject *gael_schematic_wire_new (GaelComponent *component,
				  DiaCanvasItem *parent);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GAEL_SCHEMATIC_ITEM_H__ */
