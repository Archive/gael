/* gael-schematic-conn.h
 * Copyright (C) 2001, 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GAEL_SCHEMATIC_CONN_H__
#define __GAEL_SCHEMATIC_CONN_H__

#include <gtk/gtk.h>
#include <glade/glade.h>
#include <dia-newcanvas/dia-newcanvas.h>

#define GAEL_TYPE_SCHEMATIC_CONN            (gael_schematic_conn_get_type ())
#define GAEL_SCHEMATIC_CONN(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GAEL_TYPE_SCHEMATIC_CONN, GaelSchematicConn))
#define GAEL_SCHEMATIC_CONN_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GAEL_TYPE_SCHEMATIC_CONN, GaelSchematicConnClass))
#define GAEL_IS_SCHEMATIC_CONN(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GAEL_TYPE_SCHEMATIC_CONN))
#define GAEL_IS_SCHEMATIC_CONN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GAEL_TYPE_SCHEMATIC_CONN))
#define GAEL_SCHEMATIC_CONN_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),   GAEL_TYPE_SCHEMATIC_CONN, GaelSchematicConnClass))

typedef struct _GaelSchematicConn GaelSchematicConn;
typedef struct _GaelSchematicConnClass GaelSchematicConnClass;

struct _GaelSchematicConn
{
  DiaCanvasConnection item;

  gboolean show;
};

struct _GaelSchematicConnClass
{
  DiaCanvasConnectionClass parent_class;
};

GType  gael_schematic_conn_get_type    (void);

#endif
