/* gael-schematic-model.c
 * Copyright (C) 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <gmodule.h>
#include <general/gael-model-interface.h>
#include "gael-schematic-model.h"
#include "gael-schematic-symbol.h"
#include <util/gael-intl.h>
#include <library/gael-librarian.h>
#include <general/gael-main-window.h>
#include "gael-schematic-view.h"
#include "gael-schematic-wire.h"
#include "gael-schematic-utils.h"

/* properties */
enum {
  PROP_NONE,
  PROP_LAST
};

static void gael_schematic_model_init           (GaelSchematicModel *schem);
static void gael_schematic_model_class_init     (GaelSchematicModelClass *klass);
static void gael_schematic_model_interface_init (GaelModelInterfaceClass *imodel);

static void gael_schematic_model_set_property   (GObject *object,
						guint property_id,
						const GValue *value,
						GParamSpec *pspec);
static void gael_schematic_model_get_property   (GObject *object,
						guint property_id,
						GValue *value,
						GParamSpec *pspec);
static GObject * gael_schematic_model_get_component (GaelModelInterface *imodel,
						     GaelComponent      *component,
						     GaelComponentType   type,
						     gchar              *lib,
						     gchar              *name);

GaelModelInterface *get_new_model (void);

GType
gael_schematic_model_get_type (void)
{
  static GtkType gael_schematic_model_type = 0;

  if (!gael_schematic_model_type)
    {
      static const GTypeInfo gael_schematic_model_info =
      {
        sizeof (GaelSchematicModelClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) gael_schematic_model_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,

        sizeof (GaelSchematicModel),
        0, /* n_preallocs */
        (GInstanceInitFunc) gael_schematic_model_init,
      };

      static const GInterfaceInfo model_interface_info =
      {
	(GInterfaceInitFunc) gael_schematic_model_interface_init,
	(GInterfaceFinalizeFunc) NULL,
	NULL,
      };

      gael_schematic_model_type = g_type_register_static (DIA_TYPE_CANVAS,
							  "GaelSchematicModel",
							  &gael_schematic_model_info,
							  0);
      g_type_add_interface_static (GAEL_TYPE_SCHEMATIC_MODEL,
				   GAEL_TYPE_MODEL_INTERFACE,
				   &model_interface_info);
    }
  return gael_schematic_model_type;
}

static void
gael_schematic_model_interface_init (GaelModelInterfaceClass *imodel)
{
  imodel->get_component = gael_schematic_model_get_component;
}

static void
gael_schematic_model_class_init (GaelSchematicModelClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS(klass);

  object_class->set_property = gael_schematic_model_set_property;
  object_class->get_property = gael_schematic_model_get_property;
}

static void
gael_schematic_model_init (GaelSchematicModel *schematic)
{
  DiaRectangle extents = { 0, 0, 2048, 2048 };
  GdkColor line_color          = { 0, 0x0000, 0x0000, 0x0000 };
  GdkColor line_color_selected = { 0, 0xffff, 0x0000, 0x0000 };
  GdkColor fill_color          = { 0, 0xffff, 0xffff, 0x99ff };
  GdkColor fill_color_selected = { 0, 0xffff, 0xcccc, 0x0000 };

  dia_canvas_set_extents(DIA_CANVAS(schematic), &extents);

  schematic->line_color          = gdk_color_copy(&line_color);
  schematic->line_color_selected = gdk_color_copy(&line_color_selected);
  schematic->fill_color          = gdk_color_copy(&fill_color);
  schematic->fill_color_selected = gdk_color_copy(&fill_color_selected);

  /*
  g_object_new(DIA_TYPE_CANVAS_RECTANGLE,
	       "parent", DIA_CANVAS(schematic)->root,
	       "x1", 10.0, "y1", 10.0,
	       "x2", 130.0, "y2", 130.0,
	       "fill_color", &cyan,
	       "line_color", &green,
	       "line_width", 8.0,
	       NULL);
  */
}

static void
gael_schematic_model_set_property (GObject *object,
				   guint property_id,
				   const GValue *value,
				   GParamSpec *pspec)
{
  G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
}

static void
gael_schematic_model_get_property (GObject *object,
				   guint property_id,
				   GValue *value,
				   GParamSpec *pspec)
{
  G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
}

static GObject *
gael_schematic_model_get_component (GaelModelInterface *imodel,
				    GaelComponent      *component,
				    GaelComponentType   type,
				    gchar              *lib,
				    gchar              *name)
{
  switch (type) {

  case gct_component:
    {
      GaelSchematicSymbol *symbol;
      GaelLibrarian       *librarian;

      librarian = gael_main_window_get_librarian(GAEL_MAIN_WINDOW(GAEL_SCHEMATIC_VIEW(DIA_CANVAS(imodel)->focus_view)->parent));

      symbol = GAEL_SCHEMATIC_SYMBOL(gael_schematic_symbol_new(component,
							       librarian,
							       DIA_CANVAS(imodel)->root,
							       lib,
							       name));

      return G_OBJECT(symbol);
    }
    break;

  case gct_wire:
    {
      GaelSchematicWire *wire;

      wire = GAEL_SCHEMATIC_WIRE(gael_schematic_wire_new(component, DIA_CANVAS(imodel)->root));

      return G_OBJECT(wire);
    }

  default:
    return NULL;
  }
  return NULL;
}

/*
 * Module things.
 */

G_MODULE_EXPORT GaelModelInterface *
get_new_model (void)
{
  return g_object_new(GAEL_TYPE_SCHEMATIC_MODEL, NULL);
}
