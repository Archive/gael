/* gael-schematic-symbol.c
 * Copyright (C) 2001, 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string.h>
#include <gtk/gtk.h>
#include <library/gael-librarian.h>
#include <library/gael-librarian-chooser.h>
#include <general/gael-general.h>
#include "gael-schematic-symbol.h"
#include "gael-schematic-pin.h"
#include "gael-schematic-utils.h"
#include "gael-schematic-view.h"
#include "gael-library-symbol.h"
#include "gael-schematic-wire.h"
#include "util/gael-intl.h"
#include <general/gael-main-window.h>
#include <gmodule.h>

GdkColor line_color          = { 0, 0x0000, 0x0000, 0x0000 };
GdkColor line_color_selected = { 0, 0xffff, 0x0000, 0x0000 };
GdkColor fill_color          = { 0, 0xffff, 0xffff, 0x99ff };
GdkColor fill_color_selected = { 0, 0xffff, 0xcccc, 0x0000 };


/* properties */
enum {
  PROP_NONE,
  PROP_LIBRARY,
  PROP_NAME,
  PROP_LAST
};

static void gael_schematic_symbol_component_interface_class_init (GaelComponentInterfaceClass *vtable);

static void gael_schematic_symbol_init       ( GaelSchematicSymbol      *symbol );
static void gael_schematic_symbol_class_init ( GaelSchematicSymbolClass *klass );
static void gael_schematic_symbol_finalize   ( GObject                  *object );

static void gael_schematic_symbol_set_property ( GObject      *object,
						 guint         property_id,
						 const GValue *value,
						 GParamSpec   *pspec );
static void gael_schematic_symbol_get_property ( GObject      *object,
						 guint         property_id,
						 GValue       *value,
						 GParamSpec   *pspec );

static void      gael_schematic_symbol_set_parent        ( GaelComponentInterface *icomp,
							   GaelComponent          *component );
static void      gael_schematic_symbol_property_changed  ( GaelComponentInterface *icomp,
							   gchar                  *name );
static void      gael_schematic_symbol_delete            ( GaelComponentInterface *icomp );
static void      gael_schematic_symbol_place             ( GaelComponentInterface *icomp );
static gchar    *gael_schematic_symbol_save              ( GaelComponentInterface *icomp,
							   gchar                  *prepend );
static gboolean  gael_schematic_symbol_load              ( GaelComponentInterface *icomp,
							   xmlDocPtr               doc,
							   xmlNodePtr              cur );



static void gael_schematic_symbol_create_item       ( GaelLibrarySymbol   *libsymbol,
						      GaelSchematicSymbol *symbol );

static gint gael_schematic_symbol_event             ( DiaCanvasItem *item,
						      GdkEvent      *event,
						      DiaCanvasView *view );

static void gael_schematic_symbol_connection_moved_handler (DiaCanvasItem *item, DiaCanvasConnection *connection, gdouble dx, gdouble dy);

static void gael_schematic_symbol_move      ( DiaCanvasItem       *item,
					      gdouble              dx,
					      gdouble              dy);


static void gael_schematic_symbol_callback_list_properties (GtkMenuItem *item, gpointer data);
static void gael_schematic_symbol_list_properties_in_treeview (gpointer key,
							       gpointer value,
							       gpointer user_data);
static void gael_schematic_symbol_list_properties_new    (GtkButton *button, gpointer user_data);
static void gael_schematic_symbol_list_properties_del    (GtkButton *button, gpointer user_data);
static void gael_schematic_symbol_list_properties_Cancel (GtkButton *button, gpointer user_data);
static void gael_schematic_symbol_list_properties_OK     (GtkButton *button, gpointer user_data);
static void gael_schematic_symbol_list_properties_fixed_toggled (GtkCellRendererToggle *cell,
								 gchar                 *path_str,
								 gpointer               data);
static void gael_schematic_symbol_list_properties_cell_edited   (GtkCellRendererText *cell,
								 const gchar         *path_string,
								 const gchar         *new_text,
								 gpointer             data);
static void gael_schematic_symbol_save_foreach (gchar         *key,
						DiaCanvasText *value,
						gpointer       data[2]);
static void gael_schematic_symbol_connect (DiaCanvasItem *item,
					   DiaCanvasConnection *connection);


enum
{
  COLUMN_NAME,
  COLUMN_VALUE,
  COLUMN_SHOW,
  NUM_COLUMNS
};

//static GObjectClass *parent_class = NULL;
static DiaCanvasGroupClass *parent_class = NULL;

GtkType
gael_schematic_symbol_get_type (void)
{
  static GtkType schematic_symbol_type = 0;

  if (!schematic_symbol_type)
    {
      static const GTypeInfo schematic_symbol_info =
      {
	sizeof (GaelSchematicSymbolClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) gael_schematic_symbol_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,
	
	sizeof (GaelSchematicSymbol),
	0, /* n_preallocs */
	(GInstanceInitFunc) gael_schematic_symbol_init,
      };

      static const GInterfaceInfo schematic_symbol_component_interface_info =
      {
	(GInterfaceInitFunc) gael_schematic_symbol_component_interface_class_init,
	(GInterfaceFinalizeFunc) NULL,
	NULL,
      };

      schematic_symbol_type = g_type_register_static (DIA_TYPE_CANVAS_GROUP,
						      "GaelSchematicSymbol",
						      &schematic_symbol_info, 0);
      g_type_add_interface_static (schematic_symbol_type,
				   GAEL_TYPE_COMPONENT_INTERFACE,
				   &schematic_symbol_component_interface_info);
    }

  return schematic_symbol_type;
}

static void
gael_schematic_symbol_class_init (GaelSchematicSymbolClass *klass)
{
  GObjectClass       *object_class;
  DiaCanvasItemClass *item_class;

  object_class = G_OBJECT_CLASS(klass);
  item_class = DIA_CANVAS_ITEM_CLASS(klass);

  parent_class = g_type_class_ref(DIA_TYPE_CANVAS_GROUP);

  object_class->finalize = gael_schematic_symbol_finalize;

  object_class->set_property = gael_schematic_symbol_set_property;
  object_class->get_property = gael_schematic_symbol_get_property;

  item_class->move = gael_schematic_symbol_move;

  g_object_class_install_property (object_class,
				   PROP_LIBRARY,
				   g_param_spec_string ("library", _("Library name"),
							_("The library name"),
							"",
							G_PARAM_READWRITE));
  g_object_class_install_property (object_class,
				   PROP_NAME,
				   g_param_spec_string ("name", _("symbol name"),
							_("The symbol name"),
							"",
							G_PARAM_READWRITE));
}


static void
gael_schematic_symbol_init (GaelSchematicSymbol *symbol)
{
  symbol->properties = g_hash_table_new( (GHashFunc)g_str_hash,
					 (GEqualFunc)g_str_equal);
}

static void
gael_schematic_symbol_finalize(GObject *object)
{
  g_return_if_fail(object != NULL);
  g_return_if_fail(GAEL_IS_SCHEMATIC_SYMBOL(object));

  g_hash_table_destroy(GAEL_SCHEMATIC_SYMBOL(object)->properties);

  if (G_OBJECT_CLASS(parent_class)->finalize)
    (* G_OBJECT_CLASS(parent_class)->finalize) (object);
}

static void
gael_schematic_symbol_component_interface_class_init (GaelComponentInterfaceClass *vtable)
{
  vtable->property_changed = gael_schematic_symbol_property_changed;
  vtable->set_parent       = gael_schematic_symbol_set_parent;
  vtable->delete           = gael_schematic_symbol_delete;
  vtable->place            = gael_schematic_symbol_place;
  vtable->save             = gael_schematic_symbol_save;
  vtable->load             = gael_schematic_symbol_load;
}

static void
gael_schematic_symbol_set_property (GObject *object,
				    guint property_id,
				    const GValue *value,
				    GParamSpec *pspec)
{
  GaelSchematicSymbol *symbol = GAEL_SCHEMATIC_SYMBOL(object);

  switch (property_id) {

    case PROP_LIBRARY:
      if( symbol->library == NULL ) {
	symbol->library = strdup(g_value_get_string(value));
      }
      break;

    case PROP_NAME:
      if( symbol->name == NULL ) {
	symbol->name = strdup(g_value_get_string(value));
      }
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;

  }
}

static void
gael_schematic_symbol_get_property (GObject *object,
				    guint property_id,
				    GValue *value,
				    GParamSpec *pspec)
{
  G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
}

static void
gael_schematic_symbol_set_parent (GaelComponentInterface *icomp,
				  GaelComponent          *component)
{
  GAEL_SCHEMATIC_SYMBOL(icomp)->parent = component;
}

static void
gael_schematic_symbol_property_changed  (GaelComponentInterface *icomp,
					 gchar                  *name)
{
  DiaCanvasText       *text;
  GaelSchematicSymbol *symbol;
  gpointer             new_value;

  symbol = GAEL_SCHEMATIC_SYMBOL(icomp);

  new_value = g_hash_table_lookup(GAEL_COMPONENT(symbol->parent)->properties,name);

  if( (text = g_hash_table_lookup(symbol->properties,name)) != NULL ) {
    g_object_set(text, "text", new_value, NULL);
  }
}

void
gael_schematic_symbol_create_item (GaelLibrarySymbol *libsymbol, GaelSchematicSymbol *symbol)
{
  DiaCanvasItem *item;
  GList *tmp;
  GaelGeom *geom;
  DiaCanvasGroup* group = DIA_CANVAS_GROUP(symbol);

  gdouble  line_width = 2.0;

  g_return_if_fail( GAEL_IS_SCHEMATIC_SYMBOL(group) );

  tmp = libsymbol->geometries;
  while( tmp != NULL ) {
    geom = (GaelGeom*)tmp->data;
    switch( geom->type ) {

    case GAEL_POINT:
      g_message("%s(%i) GAEL_POINT todo",__FILE__,__LINE__);
      break;

    case GAEL_LINE:
      item = g_object_new(DIA_TYPE_CANVAS_LINE,
			  "parent", group,
			  "x1", geom->line.x1,
			  "y1", geom->line.y1,
			  "x2", geom->line.x2,
			  "y2", geom->line.y2,
			  "line_color", &line_color,
			  "line_width", line_width,
			  "move_indep", FALSE,
			  NULL);
      break;

    case GAEL_LINES:
      g_message("%s(%i) GAEL_LINES todo",__FILE__,__LINE__);
      break;

    case GAEL_RECTANGLE:
      item = g_object_new(DIA_TYPE_CANVAS_RECTANGLE,
			  "parent", group,
			  "x1", geom->rectangle.left,
			  "y1", geom->rectangle.top,
			  "x2", geom->rectangle.right,
			  "y2", geom->rectangle.bottom,
			  "line_color", &line_color,
			  "line_width", line_width,
			  "move_indep", FALSE,
			  NULL);
      if( geom->rectangle.filled ) {
	g_object_set(item,"fill_color", &fill_color,NULL);
      }
      break;

    case GAEL_ARC:
      item = g_object_new(DIA_TYPE_CANVAS_ARC,
			  "parent", group,
			  "x",      geom->arc.x,
			  "y",      geom->arc.y,
			  "width",  geom->arc.width,
			  "height", geom->arc.height,
			  "angle1", geom->arc.angle1,
			  "angle2", geom->arc.angle2,
			  "line_color", &line_color,
			  "line_width", line_width,
			  "move_indep", FALSE,
			  NULL);
      if( geom->rectangle.filled ) {
	g_object_set(item,"fill_color", &fill_color,NULL);
      }
      break;

    case GAEL_POLYGON:
      g_message("%s(%i) GAEL_POLYGON todo",__FILE__,__LINE__);
      break;

    case GAEL_TEXT:
      item = g_object_new(DIA_TYPE_CANVAS_TEXT,
			  "parent", group,
			  "x", geom->text.x,
			  "y", geom->text.y,
			  "move_indep", FALSE,
			  "text",geom->text.text,
			  NULL);
      break;

    case GAEL_ATTRACT:
      item = g_object_new(DIA_TYPE_CANVAS_ATTRACT,
			  "parent", group,
			  "x", geom->attract.x,
			  "y", geom->attract.y,
			  "move_indep", FALSE,
			  NULL);
      break;

    case GAEL_ATTRACTED:
      item = g_object_new(DIA_TYPE_CANVAS_ATTRACTED,
			  "parent", group,
			  "x", geom->attract.x,
			  "y", geom->attract.y,
			  "move_indep", FALSE,
			  NULL);
      break;

    case GAEL_CONNECTION:
      item = g_object_new(GAEL_TYPE_SCHEMATIC_PIN,
			  "parent", group,
			  "x", geom->connection.x,
			  "y", geom->connection.y,
			  "id", geom->connection.id,
			  "move_indep", FALSE,
			  NULL);
      g_signal_connect(item,"connection_moved",G_CALLBACK(gael_schematic_symbol_connection_moved_handler), NULL);
      g_signal_connect(item, "connect", G_CALLBACK(gael_schematic_symbol_connect), NULL);
      item = g_object_new(DIA_TYPE_CANVAS_ATTRACT,
			  "parent", group,
			  "x", geom->connection.x,
			  "y", geom->connection.y,
			  "move_indep", FALSE,
			  NULL);
      item = g_object_new(DIA_TYPE_CANVAS_ATTRACTED,
			  "parent", group,
			  "x", geom->connection.x,
			  "y", geom->connection.y,
			  "move_indep", FALSE,
			  NULL);
      break;

    case GAEL_PROPERTY:
      item = g_object_new(DIA_TYPE_CANVAS_TEXT,
			  "parent", group,
			  "x", geom->property.x,
			  "y", geom->property.y,
			  "move_indep", TRUE,
			  "text", geom->property.value,
			  NULL);
      g_signal_connect(item,"event",G_CALLBACK(gael_schematic_utils_event), symbol->schematic);
      g_hash_table_insert(symbol->properties,strdup(geom->property.name),item);

      gael_component_set_property(GAEL_COMPONENT(symbol->parent), geom->property.name, geom->property.value);

      dia_canvas_item_show(DIA_CANVAS_ITEM(item));
      break;

    default:
      break;

    }
    tmp = tmp->next;
  }
}

static void
gael_schematic_symbol_connection_moved_handler (DiaCanvasItem *item,
						DiaCanvasConnection *connection,
						gdouble dx, gdouble dy)
{
  DiaCanvasConnection *dest = DIA_CANVAS_CONNECTION(item);

  dest->connections = g_list_remove(dest->connections,connection);
  connection->connections = g_list_remove(connection->connections,dest);
  //g_object_unref(dest);
  //g_object_unref(connection);
}

static gint
gael_schematic_symbol_event(DiaCanvasItem *item, GdkEvent *event, DiaCanvasView *view)
{
  switch (event->type) {

  case GDK_BUTTON_PRESS:
    if( event->button.button == 3 ) {
      GtkWidget *menu;
      GtkWidget *menu_items;

      menu = gtk_menu_new ();

      menu_items = gtk_menu_item_new_with_label ("List Properties");
      gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_items);
      g_signal_connect (G_OBJECT (menu_items), "activate",
                        G_CALLBACK (gael_schematic_symbol_callback_list_properties), 
			item );
      gtk_widget_show (menu_items);

      gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL, NULL,
		      event->button.button, event->button.time);
    }
    return TRUE;

  default:
    return FALSE;
  }
  return FALSE;
}

void
gael_schematic_symbol_callback_list_properties (GtkMenuItem *item,
						gpointer user_data)
{
  GladeXML          *xml;
  GtkWidget         *widget;
  GtkListStore      *store;
  //GtkTreeIter        iter;
  GtkCellRenderer   *renderer;
  GtkTreeViewColumn *column;
  gpointer          *data;

  GaelSchematicSymbol *symbol = GAEL_SCHEMATIC_SYMBOL(user_data);

  xml = glade_xml_new(GAEL_DATADIR "/gael/glade/gael2.glade", "PropertyListDialog", NULL);

  widget = glade_xml_get_widget(xml,"PropertyListDialogList");

  store = gtk_list_store_new (NUM_COLUMNS, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_BOOLEAN);
  gtk_tree_view_set_model(GTK_TREE_VIEW(widget),GTK_TREE_MODEL(store));

  renderer = gtk_cell_renderer_text_new ();
  g_signal_connect  (G_OBJECT (renderer), "edited", G_CALLBACK (gael_schematic_symbol_list_properties_cell_edited), store);
  g_object_set_data (G_OBJECT (renderer), "column", (gint *)COLUMN_NAME);
  g_object_set(renderer,"editable", TRUE, NULL);
  column = gtk_tree_view_column_new_with_attributes ("Name", renderer, "text", COLUMN_NAME,  NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW(widget), column);

  renderer = gtk_cell_renderer_text_new ();
  g_signal_connect  (G_OBJECT (renderer), "edited", G_CALLBACK (gael_schematic_symbol_list_properties_cell_edited), store);
  g_object_set_data (G_OBJECT (renderer), "column", (gint *)COLUMN_VALUE);
  g_object_set(renderer,"editable", TRUE, NULL);
  column = gtk_tree_view_column_new_with_attributes ("Value", renderer, "text", COLUMN_VALUE, NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW(widget), column);

  renderer = gtk_cell_renderer_toggle_new ();
  g_signal_connect (G_OBJECT (renderer), "toggled", G_CALLBACK (gael_schematic_symbol_list_properties_fixed_toggled), store);
  column = gtk_tree_view_column_new_with_attributes ("Show", renderer, "active", COLUMN_SHOW, NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW(widget), column);

  data = g_new0(gpointer,2);
  data[0] = store;
  data[1] = symbol;
  g_hash_table_foreach(GAEL_COMPONENT(symbol->parent)->properties, gael_schematic_symbol_list_properties_in_treeview, data);
  g_free(data);

  widget = glade_xml_get_widget(xml,"PropertyListDialogNew");
  g_signal_connect(widget, "clicked", G_CALLBACK(gael_schematic_symbol_list_properties_new), store);

  widget = glade_xml_get_widget(xml,"PropertyListDialogDelete");
  g_signal_connect(widget, "clicked", G_CALLBACK(gael_schematic_symbol_list_properties_del), glade_xml_get_widget(xml,"PropertyListDialogList"));

  data = g_new0(gpointer,2);

  data[0] = xml;
  data[1] = symbol;

  widget = glade_xml_get_widget(xml,"PropertyListDialogCancel");
  g_signal_connect(widget, "clicked", G_CALLBACK(gael_schematic_symbol_list_properties_Cancel), data);

  widget = glade_xml_get_widget(xml,"PropertyListDialogOK");
  g_signal_connect(widget, "clicked", G_CALLBACK(gael_schematic_symbol_list_properties_OK), data);
}

static void
gael_schematic_symbol_list_properties_in_treeview (gpointer key,
						   gpointer value,
						   gpointer user_data)
{
  GtkTreeIter          iter;
  GtkListStore        *store;
  GaelSchematicSymbol *symbol;
  gboolean             show;
  DiaCanvasItem       *item;

  store  = GTK_LIST_STORE(((gpointer*)user_data)[0]);
  symbol = GAEL_SCHEMATIC_SYMBOL(((gpointer*)user_data)[1]);

  show = FALSE;
  if( ((item = g_hash_table_lookup(symbol->properties,key)) != NULL) &&
      (DIA_CANVAS_ITEM(item)->visible == 1) ) {
    show = TRUE;
  }

  gtk_list_store_append (store, &iter);
  gtk_list_store_set (store, &iter,
		      COLUMN_NAME,  (guchar*)key,
		      COLUMN_VALUE, (guchar*)value,
		      COLUMN_SHOW,  show,
		      -1);
}

static void
gael_schematic_symbol_list_properties_new (GtkButton *button,
					   gpointer user_data)
{
  GtkTreeIter   iter;
  GtkListStore *store = GTK_LIST_STORE(user_data);

  gtk_list_store_append (store, &iter);
  gtk_list_store_set (store, &iter,
		      COLUMN_NAME,  "name",
		      COLUMN_VALUE, "value",
		      COLUMN_SHOW,  FALSE,
		      -1);
}

static void
gael_schematic_symbol_list_properties_del (GtkButton *button,
					   gpointer user_data)
{
  GtkTreeIter iter;
  GtkTreeView *treeview = (GtkTreeView *)user_data;
  GtkTreeModel *model = gtk_tree_view_get_model (treeview);
  GtkTreeSelection *selection = gtk_tree_view_get_selection (treeview);

  if (gtk_tree_selection_get_selected (selection, NULL, &iter)) {
    gint i;
    GtkTreePath *path;

    path = gtk_tree_model_get_path (model, &iter);
    i = gtk_tree_path_get_indices (path)[0];
    gtk_list_store_remove (GTK_LIST_STORE (model), &iter);

    gtk_tree_path_free (path);
  }
}

static void
gael_schematic_symbol_list_properties_Cancel (GtkButton *button,
					      gpointer user_data)
{
  gpointer  *data = user_data;

  gtk_widget_destroy(glade_xml_get_widget((GladeXML*)data[0],"PropertyListDialog"));
  g_free(data);
}

static void
gael_schematic_symbol_list_properties_OK (GtkButton *button,
					  gpointer user_data)
{
  GladeXML            *xml;
  GaelSchematicSymbol *symbol;
  GtkTreeIter          iter;
  GtkTreeModel        *model;
  guchar              *name,*value;
  gboolean             display;
  GtkWidget           *widget;


  xml = ((gpointer*)user_data)[0];
  symbol = GAEL_SCHEMATIC_SYMBOL(((gpointer*)user_data)[1]);

  model = gtk_tree_view_get_model(GTK_TREE_VIEW(glade_xml_get_widget(xml,"PropertyListDialogList")));
  
  if( gtk_tree_model_get_iter_first(model,&iter) == TRUE ) {

    gtk_tree_model_get (model, &iter, COLUMN_NAME, &name, COLUMN_VALUE, &value, COLUMN_SHOW, &display, -1);

    if( display == TRUE ) {
      
      widget = g_hash_table_lookup(symbol->properties,name);

      if( widget == NULL ) {
	widget = g_object_new(DIA_TYPE_CANVAS_TEXT,
			      "parent", DIA_CANVAS_GROUP(symbol),
			      "x", (DIA_CANVAS_ITEM(symbol)->bounds.right + DIA_CANVAS_ITEM(symbol)->bounds.left)/2.0,
			      "y", DIA_CANVAS_ITEM(symbol)->bounds.bottom + 10.0,
			      "move_indep", TRUE,
			      "text", value,
			      NULL);
	g_signal_connect(widget,"event",G_CALLBACK(gael_schematic_utils_event), symbol->schematic);
	g_hash_table_insert(symbol->properties,strdup(name),widget);
      }

      dia_canvas_item_show(DIA_CANVAS_ITEM(widget));

    } else {
      widget = g_hash_table_lookup(symbol->properties,name);
      if( widget != NULL ) {
	dia_canvas_item_hide(DIA_CANVAS_ITEM(widget));
      }
    }

    gael_component_set_property(GAEL_COMPONENT(symbol->parent),name,value);

    while( gtk_tree_model_iter_next(model,&iter) == TRUE ) {
      gtk_tree_model_get (model, &iter, COLUMN_NAME, &name, COLUMN_VALUE, &value, COLUMN_SHOW, &display, -1);

      if( display == TRUE ) {

	widget = g_hash_table_lookup(symbol->properties,name);

	if( widget == NULL ) {
	  widget = g_object_new(DIA_TYPE_CANVAS_TEXT,
				"parent", DIA_CANVAS_GROUP(symbol),
				"x", (DIA_CANVAS_ITEM(symbol)->bounds.right + DIA_CANVAS_ITEM(symbol)->bounds.left)/2.0,
				"y", DIA_CANVAS_ITEM(symbol)->bounds.bottom + 10.0,
				"move_indep", TRUE,
				"text", value,
				NULL);
	  g_signal_connect(widget,"event",G_CALLBACK(gael_schematic_utils_event), symbol->schematic);
	  g_hash_table_insert(symbol->properties,strdup(name),widget);
	}

	dia_canvas_item_show(DIA_CANVAS_ITEM(widget));

      } else {
	widget = g_hash_table_lookup(symbol->properties,name);
	if( widget != NULL ) {
	  dia_canvas_item_hide(DIA_CANVAS_ITEM(widget));
	}
      }

      gael_component_set_property(GAEL_COMPONENT(symbol->parent),name,value);
    }
  }

  gtk_widget_destroy(glade_xml_get_widget(xml,"PropertyListDialog"));
  g_free(user_data);
}

static void
gael_schematic_symbol_list_properties_fixed_toggled (GtkCellRendererToggle *cell,
						     gchar                 *path_str,
						     gpointer               data)
{
  GtkTreeModel *model = (GtkTreeModel *)data;
  GtkTreeIter  iter;
  GtkTreePath *path = gtk_tree_path_new_from_string (path_str);
  gboolean fixed;

  gtk_tree_model_get_iter (model, &iter, path);
  gtk_tree_model_get (model, &iter, COLUMN_SHOW, &fixed, -1);
  fixed ^= 1;
  gtk_list_store_set (GTK_LIST_STORE (model), &iter, COLUMN_SHOW, fixed, -1);
  gtk_tree_path_free (path);
}

static void
gael_schematic_symbol_list_properties_cell_edited (GtkCellRendererText *cell,
						   const gchar         *path_string,
						   const gchar         *new_text,
						   gpointer             data)
{
  GtkTreeModel *model = (GtkTreeModel *)data;
  GtkTreePath  *path = gtk_tree_path_new_from_string (path_string);
  GtkTreeIter   iter;
  gint         *column;

  column = g_object_get_data (G_OBJECT (cell), "column");

  gtk_tree_model_get_iter (model, &iter, path);
  gtk_list_store_set (GTK_LIST_STORE (model), &iter, column, new_text, -1);
  gtk_tree_path_free (path);
}


GObject *
gael_schematic_symbol_new(GaelComponent *component,
			  GaelLibrarian *librarian,
			  gpointer       group,
			  gchar         *lib,
			  gchar         *name)
{
  GaelSchematicSymbol *symbol;

  symbol = g_object_new(GAEL_TYPE_SCHEMATIC_SYMBOL,
		       "parent", group,
			NULL );

  gael_component_interface_set_parent(GAEL_COMPONENT_INTERFACE(symbol),component);
  g_object_set(symbol, "library", lib, "name", name, NULL);

  gael_schematic_symbol_create_item(gael_librarian_get_elem(librarian,
							    symbol->library,
							    symbol->name),
				    symbol);

  g_signal_connect(symbol,"event",G_CALLBACK(gael_schematic_symbol_event), DIA_CANVAS_VIEW(DIA_CANVAS_ITEM(group)->canvas->focus_view));
  g_signal_connect(symbol,"event",G_CALLBACK(gael_schematic_utils_event),  DIA_CANVAS_VIEW(DIA_CANVAS_ITEM(group)->canvas->focus_view));

  return G_OBJECT(symbol);
}

static void
gael_schematic_symbol_delete ( GaelComponentInterface *icomp )
{
  DiaCanvasItem *item;

  item = DIA_CANVAS_ITEM(icomp);
  gael_schematic_utils_selection_rem(item);
  dia_canvas_group_remove_item(item->parent,item);
}

static void
gael_schematic_symbol_place ( GaelComponentInterface *icomp )
{
  DiaCanvasItem       *item;
  DiaPoint             newPoint;
  gint                 x, y;

  item   = DIA_CANVAS_ITEM(icomp);

  gdk_window_get_pointer(gtk_widget_get_parent_window(GTK_WIDGET(item->canvas->focus_view)),&x,&y,0);
  newPoint.x = ((gdouble)x + item->canvas->extents.left) / item->canvas->focus_view->zoom_factor-1;
  newPoint.y = ((gdouble)y + item->canvas->extents.top)  / item->canvas->focus_view->zoom_factor-1;

  dia_canvas_item_snapmove(item, &(newPoint.x), &(newPoint.y));
  dia_canvas_item_move(item, newPoint.x, newPoint.y);
  GAEL_SCHEMATIC_VIEW(item->canvas->focus_view)->point.x = newPoint.x;
  GAEL_SCHEMATIC_VIEW(item->canvas->focus_view)->point.y = newPoint.y;

  dia_canvas_view_grab(item->canvas->focus_view,FALSE);
  gael_schematic_utils_selection_clear(item->canvas);
  gael_schematic_utils_selection_add(item);
  GAEL_SCHEMATIC_VIEW(item->canvas->focus_view)->state = GAEL_SCHEMATIC_MOVE;
  item->canvas->focus_view->event_item = item;
  item->canvas->focus_view->point_item = item;
}

static void
gael_schematic_symbol_save_foreach (gchar         *key,
				    DiaCanvasText *value,
				    gpointer       data[2])
{
  char    *shown;
  GString *string  = data[0];
  gchar   *prepend = data[1];

  if( DIA_CANVAS_ITEM(value)->visible ) {
    shown = "TRUE";
  } else {
    shown = "FALSE";
  }
  g_string_append(string, prepend);
  g_string_append_printf(string, "\t<property name=\"%s\" shown=\"%s\" x=\"%f\" y=\"%f\"/>\n",
			 key, shown, value->point.x, value->point.y);
}

static gchar *
gael_schematic_symbol_save (GaelComponentInterface *icomp,
			    gchar *prepend)
{
  gpointer data[2];
  GaelSchematicSymbol *symbol = GAEL_SCHEMATIC_SYMBOL(icomp);
  GString *string = g_string_new("");

  g_string_append(string, prepend);
  g_string_append(string, "<schematic name=\"");
  if( symbol->name != NULL ) {
    g_string_append(string, symbol->name);
  }
  g_string_append(string, "\" lib=\"");
  if( symbol->library != NULL ) {
    g_string_append(string, symbol->library);
  }

  g_string_append_printf(string, "\" x=\"%f\" y=\"%f\" >\n",symbol->x,symbol->y);

  data[0] = string;
  data[1] = prepend;
  g_hash_table_foreach(symbol->properties,
		       (GHFunc)gael_schematic_symbol_save_foreach,
		       data);

  g_string_append(string, prepend);
  g_string_append(string, "</schematic>\n");

  return g_string_free(string,FALSE);
}

static gboolean
gael_schematic_symbol_load (GaelComponentInterface *icomp,
			    xmlDocPtr doc,
			    xmlNodePtr cur)
{
  char          *x, *y;
  char          *name, *library;
  DiaCanvasText *text;

  /* Set the name and library */
  name    = xmlGetProp(cur,"name");
  library = xmlGetProp(cur,"library");
  g_object_set(G_OBJECT(icomp),
	       "name",    name,
	       "library", library,
	       NULL);
  free(name);
  free(library);

  /* Move to the right place the symbol */
  x = xmlGetProp(cur,"x");
  y = xmlGetProp(cur,"y");
  dia_canvas_item_move(DIA_CANVAS_ITEM(icomp),atof(x),atof(y));
  free(x);
  free(y);

  /* Place the properties to the right location */
  cur = cur->xmlChildrenNode;
  while (cur != NULL) {
    if ( !strcmp(cur->name,"text") ) {
      cur = cur->next;
      continue;
    }
    if ( !strcmp(cur->name,"property") ) {
      char *temp;

      name = xmlGetProp(cur,"name");
      text = g_hash_table_lookup(GAEL_SCHEMATIC_SYMBOL(icomp)->properties,name);
      if ( text != NULL ) {

	temp = xmlGetProp(cur,"shown");
	x = xmlGetProp(cur,"x");
	y = xmlGetProp(cur,"y");

	/* If the property isn't visible, destroy it */
	if ( strcasecmp(temp,"TRUE") ) {
	  g_message("%s(%i): TODO: destroy this property as it's not shown",__FILE__,__LINE__);
	}
	dia_canvas_item_move(DIA_CANVAS_ITEM(text),
			     atof(x) - DIA_CANVAS_TEXT(text)->point.x,
			     atof(y) - DIA_CANVAS_TEXT(text)->point.y);

	free(temp);
	free(x);
	free(y);
      } else {
	g_message("%s(%i): TODO: create the property as it's been added manually",__FILE__,__LINE__);
      }
      free(name);

    } else {
      g_message("Unknown tag %s in Schematic symbols",cur->name);
    }
    cur = cur->next;
  }
  return TRUE;
}

static void
gael_schematic_symbol_move ( DiaCanvasItem *item,
			     gdouble        dx,
			     gdouble        dy )
{
  GaelSchematicSymbol *symbol;

  g_return_if_fail(GAEL_IS_SCHEMATIC_SYMBOL(item));
  symbol = GAEL_SCHEMATIC_SYMBOL(item);

  symbol->x += dx;
  symbol->y += dy;
  DIA_CANVAS_ITEM_CLASS(parent_class)->move(item,dx,dy);
}

static void
gael_schematic_symbol_connect (DiaCanvasItem *item,
			       DiaCanvasConnection *connection)
{
  DiaCanvasConnection *this;

  g_return_if_fail( item != NULL );
  g_return_if_fail( connection != NULL );

  if( item == DIA_CANVAS_ITEM(connection) )
    return;

  this = DIA_CANVAS_CONNECTION(item);

  if( (this->pos.x == connection->pos.x) &&
      (this->pos.y == connection->pos.y) ) {
    gint number;
    GList *tmp;

    /*
     * Check that the connection doesn't already exists.
     */
    tmp = this->connections;
    while( tmp != NULL ) {
      if( tmp->data == connection )
	return;
      tmp = tmp->next;
    }

    number = GAEL_SCHEMATIC_PIN(this)->id;
    gael_component_connect(GAEL_COMPONENT(GAEL_SCHEMATIC_SYMBOL(item->parent)->parent),
			   number,
			   GAEL_COMPONENT(GAEL_SCHEMATIC_WIRE(DIA_CANVAS_ITEM(connection)->parent)->parent));
  }
}
