/* gael-schematic-pin.h
 * Copyright (C) 2001, 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GAEL_SCHEMATIC_PIN_H__
#define __GAEL_SCHEMATIC_PIN_H__

#include <gtk/gtk.h>
#include <glade/glade.h>
#include <dia-newcanvas/dia-newcanvas.h>

#define GAEL_TYPE_SCHEMATIC_PIN            (gael_schematic_pin_get_type ())
#define GAEL_SCHEMATIC_PIN(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GAEL_TYPE_SCHEMATIC_PIN, GaelSchematicPin))
#define GAEL_SCHEMATIC_PIN_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GAEL_TYPE_SCHEMATIC_PIN, GaelSchematicPinClass))
#define GAEL_IS_SCHEMATIC_PIN(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GAEL_TYPE_SCHEMATIC_PIN))
#define GAEL_IS_SCHEMATIC_PIN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GAEL_TYPE_SCHEMATIC_PIN))
#define GAEL_SCHEMATIC_PIN_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),   GAEL_TYPE_SCHEMATIC_PIN, GaelSchematicPinClass))

typedef struct _GaelSchematicPin GaelSchematicPin;
typedef struct _GaelSchematicPinClass GaelSchematicPinClass;

struct _GaelSchematicPin
{
  DiaCanvasConnection item;

  gint id;

  gboolean fill;
};

struct _GaelSchematicPinClass
{
  DiaCanvasConnectionClass parent_class;
};

GType  gael_schematic_pin_get_type    (void);

#endif
