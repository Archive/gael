/* gael-schematic-model.h
 * Copyright (C) 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GAEL_SCHEMATIC_MODEL_H__
#define __GAEL_SCHEMATIC_MODEL_H__

#include <gtk/gtk.h>
#include <dia-newcanvas/dia-newcanvas.h>

#define GAEL_TYPE_SCHEMATIC_MODEL            (gael_schematic_model_get_type ())
#define GAEL_SCHEMATIC_MODEL(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GAEL_TYPE_SCHEMATIC_MODEL, GaelSchematicModel))
#define GAEL_SCHEMATIC_MODEL_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GAEL_TYPE_SCHEMATIC_MODEL, GaelSchematicModelClass))
#define GAEL_IS_SCHEMATIC_MODEL(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GAEL_TYPE_SCHEMATIC_MODEL))
#define GAEL_IS_SCHEMATIC_MODEL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GAEL_TYPE_SCHEMATIC_MODEL))
#define GAEL_SCHEMATIC_MODEL_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),   GAEL_TYPE_SCHEMATIC_MODEL, GaelSchematicModelClass))

typedef struct _GaelSchematicModel      GaelSchematicModel;
typedef struct _GaelSchematicModelClass GaelSchematicModelClass;

struct _GaelSchematicModel
{
  DiaCanvas object;

  GdkColor *line_color;
  GdkColor *line_color_selected;
  GdkColor *fill_color;
  GdkColor *fill_color_selected;
};

struct _GaelSchematicModelClass
{
  DiaCanvasClass parent_class;
};

GType gael_schematic_model_get_type (void);

#endif
