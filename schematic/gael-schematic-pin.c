/* gael-schematic-pin.c
 * Copyright (C) 2001, 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <gtk/gtk.h>
#include "gael-schematic-pin.h"
#include "util/gael-intl.h"

/* properties */
enum {
  PROP_NONE,
  PROP_ID,
  PROP_LAST
};

static void gael_schematic_pin_init       (GaelSchematicPin      *pin);
static void gael_schematic_pin_class_init (GaelSchematicPinClass *klass);
static void gael_schematic_pin_finalize   (GObject                *object);

static void gael_schematic_pin_set_property (GObject *object,
					     guint property_id,
					     const GValue *value,
					     GParamSpec *pspec);
static void gael_schematic_pin_get_property (GObject *object,
					     guint property_id,
					     GValue *value,
					     GParamSpec *pspec);

static void gael_schematic_pin_real_render  (DiaCanvasItem *item,
					     DiaRenderer *renderer,
					     DiaRectangle *rectangle);


static DiaCanvasConnectionClass *parent_class = NULL;


GtkType
gael_schematic_pin_get_type (void)
{
  static GtkType schematic_pin_type = 0;

  if (!schematic_pin_type)
    {
      static const GTypeInfo schematic_pin_info =
      {
	sizeof (GaelSchematicPinClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) gael_schematic_pin_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,
	
	sizeof (GaelSchematicPin),
	0, /* n_preallocs */
	(GInstanceInitFunc) gael_schematic_pin_init,
      };

      schematic_pin_type = g_type_register_static (DIA_TYPE_CANVAS_CONNECTION,
						    "GaelSchematicPin",
						    &schematic_pin_info, 0);
    }

  return schematic_pin_type;
}

static void
gael_schematic_pin_class_init (GaelSchematicPinClass *klass)
{
  GObjectClass *object_class;
  DiaCanvasItemClass *item_class;

  object_class = G_OBJECT_CLASS(klass);
  item_class   = DIA_CANVAS_ITEM_CLASS(klass);

  parent_class = g_type_class_ref(DIA_TYPE_CANVAS_CONNECTION);

  object_class->finalize = gael_schematic_pin_finalize;

  object_class->set_property = gael_schematic_pin_set_property;
  object_class->get_property = gael_schematic_pin_get_property;

  item_class->render       = gael_schematic_pin_real_render;

  g_object_class_install_property (object_class,
				   PROP_ID,
				   g_param_spec_int ("id", _("Pin identity"),
						     _("The pin number"),
						     -G_MAXINT,
						     G_MAXINT,
						     0,
						     G_PARAM_READWRITE));
}


static void
gael_schematic_pin_init (GaelSchematicPin *pin)
{
  pin->fill = TRUE;
}

static void
gael_schematic_pin_finalize(GObject *object)
{
  GaelSchematicPin *pin;

  g_return_if_fail(object != NULL);
  g_return_if_fail(GAEL_IS_SCHEMATIC_PIN(object));

  pin = GAEL_SCHEMATIC_PIN(object);

  if (G_OBJECT_CLASS(parent_class)->finalize)
    (* G_OBJECT_CLASS(parent_class)->finalize) (object);
}

static void
gael_schematic_pin_set_property (GObject *object,
				  guint property_id,
				  const GValue *value,
				  GParamSpec *pspec)
{
  GaelSchematicPin *pin = GAEL_SCHEMATIC_PIN(object);

  switch (property_id) {
  case PROP_ID:
    pin->id = g_value_get_int(value);
    break;

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
gael_schematic_pin_get_property (GObject *object,
				  guint property_id,
				  GValue *value,
				  GParamSpec *pspec)
{
  GaelSchematicPin *pin = GAEL_SCHEMATIC_PIN(object);

  switch (property_id) {
  case PROP_ID:
    g_value_set_int(value,pin->id);
    break;

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}


static void
gael_schematic_pin_real_render (DiaCanvasItem *item,
				DiaRenderer *renderer,
				DiaRectangle *rectangle)
{
  DiaCanvasConnection *connection = DIA_CANVAS_CONNECTION(item);
  GaelSchematicPin    *pin        = GAEL_SCHEMATIC_PIN(item);

  if( item->visible == TRUE ) {
    dia_renderer_set_color(renderer, &connection->fill_color);
    dia_renderer_draw_ellipse(renderer, pin->fill, &connection->pos, 6, 6);
  }
}
