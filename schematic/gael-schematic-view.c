/* gael-schematic-view.c
 * Copyright (C) 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>
#include <gnome.h>

#include <math.h>
#include <gmodule.h>
#include <general/gael-view-interface.h>
#include <general/gael-model-interface.h>
#include <general/gael-main-window.h>
#include <general/gael-component.h>
#include <general/gael-design.h>
#include <library/gael-librarian.h>
#include "gael-schematic-view.h"
#include "gael-schematic-symbol.h"
#include "gael-schematic-utils.h"
#include "util/gael-intl.h"

/* properties */
enum {
  PROP_NONE,
  PROP_LAST
};

static void gael_schematic_view_init           (GaelSchematicView *schem);
static void gael_schematic_view_class_init     (GaelSchematicViewClass *klass);
static void gael_schematic_view_interface_init (GaelViewInterfaceClass *iview);

static void gael_schematic_view_set_property   (GObject *object,
						guint property_id,
						const GValue *value,
						GParamSpec *pspec);
static void gael_schematic_view_get_property   (GObject *object,
						guint property_id,
						GValue *value,
						GParamSpec *pspec);


static GtkWidget   *gael_schematic_iview_get_widget  (GaelViewInterface  *iview);
static void         gael_schematic_iview_init        (GaelViewInterface  *iview);
static void         gael_schematic_iview_activate    (GaelViewInterface  *iview,
						      BonoboUIContainer  *container);
static void         gael_schematic_iview_deactivate  (GaelViewInterface  *iview);
static void         gael_schematic_iview_set_model   (GaelViewInterface  *iview,
						      GaelModelInterface *imodel);
static void         gael_schematic_iview_set_parent  (GaelViewInterface  *iview,
						      gpointer           *parent);


static void         gael_schematic_view_zoom_to_fit_cb (BonoboUIComponent *component, 
							gpointer           data, 
							const char        *cname);
static void         gael_schematic_view_zoom_in_cb     (BonoboUIComponent *component, 
							gpointer           data, 
							const char        *cname);
static void         gael_schematic_view_zoom_out_cb    (BonoboUIComponent *component, 
							gpointer           data, 
							const char        *cname);
static void         gael_schematic_view_zoom_100_cb    (BonoboUIComponent *component, 
							gpointer           data, 
							const char        *cname);
static void         gael_schematic_view_component_cb   (BonoboUIComponent *component, 
							gpointer           data, 
							const char        *cname);
static void         gael_schematic_view_wire_cb        (BonoboUIComponent *component, 
							gpointer           data, 
							const char        *cname);

static void         gael_schematic_view_chooser_visible_cb (GObject *object,
							    gboolean state);

static void         gael_schematic_view_component_selected_cb (GObject *object,
							       guchar  *lib,
							       guchar  *name);

GaelViewInterface *get_new_view (void);
const gchar *get_icon (void);
const gchar *get_name (void);
const gchar *get_name_i18n (void);

static BonoboUIVerb verbs[] = {
  BONOBO_UI_VERB ("ZoomToFit", gael_schematic_view_zoom_to_fit_cb),
  BONOBO_UI_VERB ("ZoomIn",    gael_schematic_view_zoom_in_cb),
  BONOBO_UI_VERB ("ZoomOut",   gael_schematic_view_zoom_out_cb),
  BONOBO_UI_VERB ("Zoom 100",  gael_schematic_view_zoom_100_cb),

  BONOBO_UI_VERB ("Component", gael_schematic_view_component_cb),
  BONOBO_UI_VERB ("Wire",      gael_schematic_view_wire_cb),

  BONOBO_UI_VERB_END
};


GType
gael_schematic_view_get_type (void)
{
  static GtkType gael_schematic_view_type = 0;

  if (!gael_schematic_view_type)
    {
      static const GTypeInfo gael_schematic_view_info =
      {
        sizeof (GaelSchematicViewClass),
        (GBaseInitFunc) NULL,
        (GBaseFinalizeFunc) NULL,
        (GClassInitFunc) gael_schematic_view_class_init,
        (GClassFinalizeFunc) NULL,
        NULL,

        sizeof (GaelSchematicView),
        0, /* n_preallocs */
        (GInstanceInitFunc) gael_schematic_view_init,
      };

      static const GInterfaceInfo view_interface_info =
      {
	(GInterfaceInitFunc) gael_schematic_view_interface_init,
	(GInterfaceFinalizeFunc) NULL,
	NULL,
      };

      gael_schematic_view_type = g_type_register_static (DIA_TYPE_CANVAS_VIEW_GDK,
							 "GaelSchematicView",
							 &gael_schematic_view_info,
							 0);
      g_type_add_interface_static (GAEL_TYPE_SCHEMATIC_VIEW,
				   GAEL_TYPE_VIEW_INTERFACE,
				   &view_interface_info);
    }
  return gael_schematic_view_type;
}

static void
gael_schematic_view_interface_init (GaelViewInterfaceClass *iview)
{
  iview->get_widget = gael_schematic_iview_get_widget;
  iview->init       = gael_schematic_iview_init;
  iview->activate   = gael_schematic_iview_activate;
  iview->deactivate = gael_schematic_iview_deactivate;
  iview->set_model  = gael_schematic_iview_set_model;
  iview->set_parent = gael_schematic_iview_set_parent;
}

static void
gael_schematic_view_class_init (GaelSchematicViewClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS(klass);

  object_class->set_property = gael_schematic_view_set_property;
  object_class->get_property = gael_schematic_view_get_property;
}

static void
gael_schematic_view_init (GaelSchematicView *schematic)
{
  schematic->state = GAEL_SCHEMATIC_NOTHING;

  schematic->component = bonobo_ui_component_new_default ();
}

static void
gael_schematic_view_set_property (GObject *object,
				  guint property_id,
				  const GValue *value,
				  GParamSpec *pspec)
{
  G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
}

static void
gael_schematic_view_get_property (GObject *object,
				  guint property_id,
				  GValue *value,
				  GParamSpec *pspec)
{
  G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
}

/*
 * VIEW INTERFACE
 */

static GtkWidget*
gael_schematic_iview_get_widget (GaelViewInterface *iview)
{
  GtkWidget *swin;

  gtk_widget_show(GTK_WIDGET(iview));

  swin = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(swin),
				 GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(swin), GTK_WIDGET(iview));

  return swin;
}

static void
gael_schematic_iview_init (GaelViewInterface *iview)
{
}

static void
gael_schematic_iview_activate (GaelViewInterface *iview,
			       BonoboUIContainer *container)
{
  GaelSchematicView *view;

  view = GAEL_SCHEMATIC_VIEW(iview);

  bonobo_ui_component_set_container (view->component,
				     BONOBO_OBJREF (container),
				     NULL);
	
  bonobo_ui_component_freeze (view->component, NULL);

  bonobo_ui_component_add_verb_list_with_data (view->component,
					       verbs,
					       iview);
  bonobo_ui_util_set_ui (view->component,
			 GAEL_DATADIR,
			 GAEL_DATADIR "/gnome-2.0/ui/GNOME_Gael_SchematicView.ui",
			 "SchematicView",
			 NULL);

  bonobo_ui_component_thaw (view->component, NULL);
}

static void
gael_schematic_iview_deactivate (GaelViewInterface *iview)
{
  GaelSchematicView *view;

  view = GAEL_SCHEMATIC_VIEW(iview);

  bonobo_ui_component_rm (view->component, "/", NULL);
  bonobo_ui_component_unset_container (view->component, NULL);
}

static void
gael_schematic_iview_set_model (GaelViewInterface  *iview,
				GaelModelInterface *imodel)
{
  dia_canvas_view_set_canvas(DIA_CANVAS_VIEW(iview),DIA_CANVAS(imodel));
  g_object_set(DIA_CANVAS(imodel)->root,"spacing",10.0,NULL);
  g_signal_connect ( DIA_CANVAS(imodel)->root,
		     "event", G_CALLBACK(gael_schematic_utils_grid_event),
		     NULL );
}

static void
gael_schematic_iview_set_parent (GaelViewInterface  *iview,
				 gpointer           *parent)
{
  GaelLibrarian *librarian = gael_main_window_get_librarian(GAEL_MAIN_WINDOW(parent));

  GAEL_SCHEMATIC_VIEW(iview)->parent = parent;

  g_signal_connect_swapped(librarian, "chooser_visible",
			   G_CALLBACK(gael_schematic_view_chooser_visible_cb),
			   GAEL_SCHEMATIC_VIEW(iview) );
  g_signal_connect_swapped(librarian, "comp_selected",
			   G_CALLBACK(gael_schematic_view_component_selected_cb),
			   GAEL_SCHEMATIC_VIEW(iview));
}

/*
 * UI THING
 */
static void
gael_schematic_view_zoom_to_fit_cb (BonoboUIComponent *component, 
				    gpointer           data, 
				    const char        *cname)
{
  g_message("%s(%i) gael_schematic_iview_zoom_to_fit",__FILE__,__LINE__);
}

static void
gael_schematic_view_zoom_in_cb (BonoboUIComponent *component, 
				gpointer           data, 
				const char        *cname)
{
  dia_canvas_view_set_zoom( DIA_CANVAS_VIEW(data),
			    DIA_CANVAS_VIEW(data)->zoom_factor * 1.2 );
}

static void
gael_schematic_view_zoom_out_cb (BonoboUIComponent *component, 
				 gpointer           data, 
				 const char        *cname)
{
  dia_canvas_view_set_zoom( DIA_CANVAS_VIEW(data),
			    DIA_CANVAS_VIEW(data)->zoom_factor / 1.2 );
}

static void
gael_schematic_view_zoom_100_cb (BonoboUIComponent *component, 
				 gpointer           data, 
				 const char        *cname)
{
  dia_canvas_view_set_zoom( DIA_CANVAS_VIEW(data), 1.0 );
}

static void
gael_schematic_view_component_cb (BonoboUIComponent *component, 
				  gpointer           data, 
				  const char        *cname)
{
  gael_librarian_show_chooser(gael_main_window_get_librarian(GAEL_MAIN_WINDOW(GAEL_SCHEMATIC_VIEW(data)->parent)),TRUE);
}

static void
gael_schematic_view_wire_cb (BonoboUIComponent *component, 
			     gpointer           data, 
			     const char        *cname)
{
  GaelComponent     *comp;
  GaelSchematicView *schematic;
  GaelDesign        *design;

  schematic = GAEL_SCHEMATIC_VIEW(data);
  design    = gael_main_window_get_design(GAEL_MAIN_WINDOW(schematic->parent));

  gael_schematic_utils_selection_clear ( DIA_CANVAS_VIEW(data)->canvas );
  comp = gael_component_new (design,
			     NULL,
			     gct_wire,
			     NULL,
			     NULL);
  gael_design_changed( design, TRUE );

  gael_component_interface_place( gael_component_get_interface(comp, (char*)get_name()) );
}

/*
 * Misc callbacks
 */
void
gael_schematic_view_chooser_visible_cb (GObject *object,
					gboolean state)
{
  GaelSchematicView *view;
  gchar             *value;

  view = GAEL_SCHEMATIC_VIEW(object);

  if ( state ) {
    value = "1";
  } else {
    value = "0";
  }

  g_message("%s(%i): check why this won't toggle the component button",__FILE__,__LINE__);
  bonobo_ui_component_set_prop (view->component,
				"/commands/Component",
				"state", value,
				NULL);
}

void
gael_schematic_view_component_selected_cb (GObject *object,
					   guchar  *lib,
					   guchar  *name)
{
  GaelComponent     *component;
  GaelSchematicView *schematic;
  DiaCanvasView     *view;
  GaelLibrarian     *librarian;
  GaelDesign        *design;

  schematic = GAEL_SCHEMATIC_VIEW(object);
  view      = DIA_CANVAS_VIEW(object);
  librarian = gael_main_window_get_librarian(GAEL_MAIN_WINDOW(schematic->parent));
  design    = gael_main_window_get_design(GAEL_MAIN_WINDOW(schematic->parent));

  gael_schematic_utils_selection_clear ( view->canvas );
  component = gael_component_new (design,
				  librarian,
				  gct_component,
				  lib,
				  name);
  gael_design_changed( design, TRUE );
  gael_component_interface_place( gael_component_get_interface(component, (char*)get_name()) );
}

/*
 * Module things.
 */

G_MODULE_EXPORT GaelViewInterface *
get_new_view (void)
{
  return g_object_new(GAEL_TYPE_SCHEMATIC_VIEW, NULL);
}

G_MODULE_EXPORT const gchar *
get_icon (void)
{
  return GAEL_DATADIR "/pixmaps/gael/schematic.png";
}

G_MODULE_EXPORT const gchar *
get_name (void)
{
  return "schematic";
}

G_MODULE_EXPORT const gchar *
get_name_i18n (void)
{
  return _("schematic");
}
