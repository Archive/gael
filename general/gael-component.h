/* gael-component.h
 * Copyright (C) 2001, 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GAEL_COMPONENT_H__
#define __GAEL_COMPONENT_H__

#include <gtk/gtk.h>
#include <dia-newcanvas/dia-newcanvas.h>
#include <libxml/parser.h>
#include "gael-general.h"

#define GAEL_TYPE_COMPONENT_INTERFACE            (gael_component_interface_get_type ())
#define GAEL_COMPONENT_INTERFACE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj),   GAEL_TYPE_COMPONENT_INTERFACE, GaelComponentInterface))
#define GAEL_COMPONENT_INTERFACE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),    GAEL_TYPE_COMPONENT_INTERFACE, GaelComponentInterfaceClass))
#define GAEL_IS_COMPONENT_INTERFACE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj),   GAEL_TYPE_COMPONENT_INTERFACE))
#define GAEL_IS_COMPONENT_INTERFACE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),    GAEL_TYPE_COMPONENT_INTERFACE))
#define GAEL_COMPONENT_INTERFACE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_INTERFACE((obj), GAEL_TYPE_COMPONENT_INTERFACE, GaelComponentInterfaceClass))
#define GAEL_TYPE_COMPONENT                      (gael_component_get_type ())
#define GAEL_COMPONENT(obj)                      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GAEL_TYPE_COMPONENT, GaelComponent))
#define GAEL_COMPONENT_CLASS(klass)              (G_TYPE_CHECK_CLASS_CAST ((klass),  GAEL_TYPE_COMPONENT, GaelComponentClass))
#define GAEL_IS_COMPONENT(obj)                   (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GAEL_TYPE_COMPONENT))
#define GAEL_IS_COMPONENT_CLASS(klass)           (G_TYPE_CHECK_CLASS_TYPE ((klass),  GAEL_TYPE_COMPONENT))
#define GAEL_COMPONENT_GET_CLASS(obj)            (G_TYPE_INSTANCE_GET_CLASS((obj),   GAEL_TYPE_COMPONENT, GaelComponentClass))

typedef struct _GaelComponentInterface      GaelComponentInterface;
typedef struct _GaelComponentInterfaceClass GaelComponentInterfaceClass;
typedef struct _GaelComponent               GaelComponent;
typedef struct _GaelComponentClass          GaelComponentClass;

struct _GaelComponentInterfaceClass
{
  GTypeInterface base_iface;

  void   (*set_parent)       (GaelComponentInterface *component_interface,
			      GaelComponent          *component);
  void   (*property_changed) (GaelComponentInterface *component_interface,
			      gchar                  *name);
  void   (*select)           (GaelComponentInterface *component_interface,
			      gboolean                select);
  void   (*delete)           (GaelComponentInterface *component_interface);
  void   (*place)            (GaelComponentInterface *component_interface);

  gchar *(*save)             (GaelComponentInterface *icomponent, gchar *prepend);

  gboolean (*load)           (GaelComponentInterface *icomponent,
			      xmlDocPtr doc,
			      xmlNodePtr cur);
};

struct _GaelComponent
{
  GObject object;

  gchar *lib;
  gchar *name;

  GHashTable *data;

  GHashTable *properties;
  GPtrArray  *pins;

  gpointer *design; /* GaelDesign */

  GaelComponentType type;

  guint ref;
};

struct _GaelComponentClass
{
  GObjectClass parent_class;
};

GType  gael_component_get_type           (void);
GType  gael_component_interface_get_type (void);

void   gael_component_interface_set_parent       (GaelComponentInterface *component_interface,
						  GaelComponent          *component);
void   gael_component_interface_property_changed (GaelComponentInterface *component_interface,
						  gchar                  *name);
void   gael_component_interface_select           (GaelComponentInterface *component_interface,
						  gboolean                select);
void   gael_component_interface_delete           (GaelComponentInterface *component_interface);
void   gael_component_interface_place            (GaelComponentInterface *component_interface);
gchar *gael_component_interface_save             (GaelComponentInterface *icomp,
						  gchar                  *prepend);
gboolean gael_component_interface_load           (GaelComponentInterface *icomp,
						  xmlDocPtr               doc,
						  xmlNodePtr              cur);

void   gael_component_set_property (GaelComponent *component,
				    gchar         *name,
				    gchar         *value);
void   gael_component_select       (GaelComponent *component,
				    gboolean       select);

gchar *gael_component_save         (GaelComponent *component,
				    gchar         *prepend);
gboolean gael_component_load       (GaelComponent *component,
				    xmlDocPtr      doc,
				    xmlNodePtr     cur);

void gael_component_set_design     (GaelComponent *component,
				    gpointer      *design);

GaelComponentInterface *gael_component_get_interface (GaelComponent *component,
						      gchar         *key);

void gael_component_connect        (GaelComponent *component,
				    gint           pin_number,
				    GaelComponent *wire);

#endif
