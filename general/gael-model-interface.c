/* gael-model-interface.c
 * Copyright (C) 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <gtk/gtk.h>
#include "gael-model-interface.h"
#include "util/gael-intl.h"

GtkType
gael_model_interface_get_type (void)
{
  static GtkType model_interface_type = 0;

  if (!model_interface_type)
    {
      static const GTypeInfo model_interface_info =
      {
	sizeof (GaelModelInterfaceClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
      };

      model_interface_type = g_type_register_static (G_TYPE_INTERFACE,
						    "GaelModelInterface",
						    &model_interface_info, 0);
      g_type_interface_add_prerequisite (model_interface_type, G_TYPE_OBJECT);
    }

  return model_interface_type;
}

GObject *
gael_model_interface_get_component(GaelModelInterface *imodel,
				   GaelComponent      *component,
				   GaelComponentType   type,
				   gchar              *lib,
				   gchar              *name)
{
  return GAEL_MODEL_INTERFACE_GET_CLASS(imodel)->get_component( imodel,
								component,
								type,
								lib,
								name);
}
