/* gael-design.h
 * Copyright (C) 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GAEL_DESIGN_H__
#define __GAEL_DESIGN_H__

#include <gtk/gtk.h>
#include "gael-model-interface.h"
#include <library/gael-librarian.h>

#define GAEL_TYPE_DESIGN                      (gael_design_get_type ())
#define GAEL_DESIGN(obj)                      (G_TYPE_CHECK_INSTANCE_CAST ((obj), GAEL_TYPE_DESIGN, GaelDesign))
#define GAEL_DESIGN_CLASS(klass)              (G_TYPE_CHECK_CLASS_CAST ((klass),  GAEL_TYPE_DESIGN, GaelDesignClass))
#define GAEL_IS_DESIGN(obj)                   (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GAEL_TYPE_DESIGN))
#define GAEL_IS_DESIGN_CLASS(klass)           (G_TYPE_CHECK_CLASS_TYPE ((klass),  GAEL_TYPE_DESIGN))
#define GAEL_DESIGN_GET_CLASS(obj)            (G_TYPE_INSTANCE_GET_CLASS((obj),   GAEL_TYPE_DESIGN, GaelDesignClass))

typedef struct _GaelDesign               GaelDesign;
typedef struct _GaelDesignClass          GaelDesignClass;

struct _GaelDesign
{
  GObject object;

  gboolean changed : 1;

  gchar *name;
  gchar *uri;

  GHashTable *models;
  GList      *components;

  gpointer   *main_window;

  guint       last_ref;
};

struct _GaelDesignClass
{
  GObjectClass parent_class;
};

GType  gael_design_get_type (void);

GaelDesign *gael_design_new(void);
void        gael_design_add_component(GaelDesign *design,
				      gpointer    component);
void        gael_design_rem_component(GaelDesign *design,
				      gpointer    component);

GaelModelInterface *gael_design_get_model(GaelDesign *design, gchar *key);

void   gael_design_load (GaelDesign *design,
			 gchar      *buffer);
gchar *gael_design_save (GaelDesign *design);

void   gael_design_clean(GaelDesign *design);

void   gael_design_changed ( GaelDesign *design,
			     gboolean    changed );

GaelComponent *gael_component_new(GaelDesign *design,
				  GaelLibrarian *librarian,
				  GaelComponentType  type,
				  gchar      *lib,
				  gchar      *name);

guint gael_design_get_ref (GaelDesign *design);

#endif
