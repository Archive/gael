/* gael-main-window.h
 * Copyright (C) 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#ifndef __GAEL_MAIN_WINDOW_H__
#define __GAEL_MAIN_WINDOW_H__

#include <bonobo/bonobo-window.h>
#include <bonobo/bonobo-ui-component.h>
#include <library/gael-librarian.h>
#include "gael-view-interface.h"
#include "gael-design.h"

#define GAEL_TYPE_MAIN_WINDOW            (gael_main_window_get_type ())
#define GAEL_MAIN_WINDOW(obj)            (GTK_CHECK_CAST ((obj),         GAEL_TYPE_MAIN_WINDOW, GaelMainWindow))
#define GAEL_MAIN_WINDOW_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GAEL_TYPE_MAIN_WINDOW, GaelMainWindowClass))
#define GAEL_IS_MAIN_WINDOW(obj)         (GTK_CHECK_TYPE ((obj),         GAEL_TYPE_MAIN_WINDOW))
#define GAEL_IS_MAIN_WINDOW_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GAEL_TYPE_MAIN_WINDOW))
#define GAEL_MAIN_WINDOW_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS ((obj),    GAEL_TYPE_MAIN_WINDOW, GaelMainWindowClass))

typedef struct _GaelMainWindow        GaelMainWindow;
typedef struct _GaelMainWindowClass   GaelMainWindowClass;
typedef struct _GaelMainWindowPriv    GaelMainWindowPriv;

struct _GaelMainWindow
{
  BonoboWindow       parent;
        
  GaelMainWindowPriv  *priv;
};

struct _GaelMainWindowClass
{
  BonoboWindowClass  parent_class;
};

GtkType                gael_main_window_get_type         (void);// G_GNUC_CONST;

GtkWidget             *gael_main_window_new              (void);

BonoboUIContainer     *gael_main_window_get_ui_container ( GaelMainWindow  *window );

GaelLibrarian         *gael_main_window_get_librarian    ( GaelMainWindow  *window );

GaelViewInterface     *gael_main_window_get_view         ( GaelMainWindow  *window,
							   gchar           *name );

GaelDesign            *gael_main_window_get_design       ( GaelMainWindow  *window );

void                   gael_main_window_set_bonobo       ( GaelMainWindow  *window,
							   gchar           *change,
							   gchar           *value );

#endif /* __GAEL_MAIN_WINDOW_H__ */
