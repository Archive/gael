/* gael-view-interface.c
 * Copyright (C) 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <gtk/gtk.h>
#include "gael-view-interface.h"
#include "util/gael-intl.h"

GtkType
gael_view_interface_get_type (void)
{
  static GtkType view_interface_type = 0;

  if (!view_interface_type)
    {
      static const GTypeInfo view_interface_info =
      {
	sizeof (GaelViewInterfaceClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
      };

      view_interface_type = g_type_register_static (G_TYPE_INTERFACE,
						    "GaelViewInterface",
						    &view_interface_info, 0);
      g_type_interface_add_prerequisite (view_interface_type, G_TYPE_OBJECT);
    }

  return view_interface_type;
}

GtkWidget*
gael_view_interface_get_widget (GaelViewInterface *iview)
{
  g_return_val_if_fail (GAEL_IS_VIEW_INTERFACE(iview),NULL);

  return GAEL_VIEW_INTERFACE_GET_CLASS(iview)->get_widget( iview );
}

void
gael_view_interface_init (GaelViewInterface *iview)
{
  g_return_if_fail (GAEL_IS_VIEW_INTERFACE(iview));

  GAEL_VIEW_INTERFACE_GET_CLASS(iview)->init( iview );
}

void
gael_view_interface_activate (GaelViewInterface *iview,
			      BonoboUIContainer *container)
{
  g_return_if_fail (GAEL_IS_VIEW_INTERFACE(iview));

  GAEL_VIEW_INTERFACE_GET_CLASS(iview)->activate( iview, container );
}

void
gael_view_interface_deactivate (GaelViewInterface *iview)
{
  g_return_if_fail (GAEL_IS_VIEW_INTERFACE(iview));

  GAEL_VIEW_INTERFACE_GET_CLASS(iview)->deactivate( iview );
}

void
gael_view_interface_set_model (GaelViewInterface  *iview,
			       GaelModelInterface *imodel)
{
  g_return_if_fail (GAEL_IS_VIEW_INTERFACE(iview));

  GAEL_VIEW_INTERFACE_GET_CLASS(iview)->set_model( iview, imodel );
}

void
gael_view_interface_set_parent (GaelViewInterface  *iview,
				gpointer           *parent)
{
  g_return_if_fail (GAEL_IS_VIEW_INTERFACE(iview));

  GAEL_VIEW_INTERFACE_GET_CLASS(iview)->set_parent( iview, parent );
}
