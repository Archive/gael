/* gael-model-interface.h
 * Copyright (C) 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GAEL_MODEL_INTEFACE_H__
#define __GAEL_MODEL_INTEFACE_H__

#include <gtk/gtk.h>
#include "gael-general.h"
#include "gael-component.h"

#define GAEL_TYPE_MODEL_INTERFACE            (gael_model_interface_get_type ())
#define GAEL_MODEL_INTERFACE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj),   GAEL_TYPE_MODEL_INTERFACE, GaelModelInterface))
#define GAEL_MODEL_INTERFACE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),    GAEL_TYPE_MODEL_INTERFACE, GaelModelInterfaceClass))
#define GAEL_IS_MODEL_INTERFACE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj),   GAEL_TYPE_MODEL_INTERFACE))
#define GAEL_IS_MODEL_INTERFACE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),    GAEL_TYPE_MODEL_INTERFACE))
#define GAEL_MODEL_INTERFACE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_INTERFACE((obj), GAEL_TYPE_MODEL_INTERFACE, GaelModelInterfaceClass))

typedef struct _GaelModelInterface      GaelModelInterface;
typedef struct _GaelModelInterfaceClass GaelModelInterfaceClass;

struct _GaelModelInterfaceClass
{
  GTypeInterface base_iface;

  GObject *(*get_component)(GaelModelInterface *imodel,
			    GaelComponent      *component,
			    GaelComponentType   type,
			    gchar              *lib,
			    gchar              *name);
};

GType  gael_model_interface_get_type (void);

GObject *gael_model_interface_get_component(GaelModelInterface *imodel,
					    GaelComponent      *component,
					    GaelComponentType   type,
					    gchar              *lib,
					    gchar              *name);

#endif
