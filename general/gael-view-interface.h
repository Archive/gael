/* gael-view-interface.h
 * Copyright (C) 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GAEL_VIEW_INTEFACE_H__
#define __GAEL_VIEW_INTEFACE_H__

#include <gtk/gtk.h>
#include <libbonoboui.h>
#include "gael-model-interface.h"

#define GAEL_TYPE_VIEW_INTERFACE            (gael_view_interface_get_type ())
#define GAEL_VIEW_INTERFACE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj),   GAEL_TYPE_VIEW_INTERFACE, GaelViewInterface))
#define GAEL_VIEW_INTERFACE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),    GAEL_TYPE_VIEW_INTERFACE, GaelViewInterfaceClass))
#define GAEL_IS_VIEW_INTERFACE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj),   GAEL_TYPE_VIEW_INTERFACE))
#define GAEL_IS_VIEW_INTERFACE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj),      GAEL_TYPE_VIEW_INTERFACE))
#define GAEL_VIEW_INTERFACE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_INTERFACE((obj), GAEL_TYPE_VIEW_INTERFACE, GaelViewInterfaceClass))

typedef struct _GaelViewInterface      GaelViewInterface;
typedef struct _GaelViewInterfaceClass GaelViewInterfaceClass;

struct _GaelViewInterfaceClass
{
  GTypeInterface base_iface;

  GtkWidget   *(*get_widget) (GaelViewInterface  *iview);
  void         (*init)       (GaelViewInterface  *iview);
  void         (*activate)   (GaelViewInterface  *iview,
			      BonoboUIContainer  *container);
  void         (*deactivate) (GaelViewInterface  *iview);
  void         (*set_model)  (GaelViewInterface  *iview,
			      GaelModelInterface *imodel);
  void         (*set_parent) (GaelViewInterface  *iview,
			      gpointer           *parent);
};

GType  gael_view_interface_get_type (void);

GtkWidget*   gael_view_interface_get_widget (GaelViewInterface *iview);
void         gael_view_interface_init       (GaelViewInterface *iview);
void         gael_view_interface_activate   (GaelViewInterface *iview,
					     BonoboUIContainer *container);
void         gael_view_interface_deactivate (GaelViewInterface *iview);
void         gael_view_interface_set_model  (GaelViewInterface  *iview,
					     GaelModelInterface *imodel);
void         gael_view_interface_set_parent (GaelViewInterface  *iview,
					     gpointer           *parent);

#endif
