/* main.c
 * Copyright (C) 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>
#include <gnome.h>

#include <libgnome/gnome-program.h>
#include <libgnomeui/gnome-ui-init.h>
#include <libbonoboui.h>

#include "config.h"

#include "gael-main-window.h"
#include "util/gael-marshal.h"
#include "gael-module.h"

#include <string.h>

GHashTable *modules = NULL;

int
main(int argc, char **argv)
{
  GaelModule *module;

  bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);

  /*
   * initialize Gnome
   */
  gnome_init("gael","0.1",argc, argv);

  /*
   * module list initialisation
   */
  modules = g_hash_table_new(g_str_hash,g_str_equal);

  module = gael_module_new(GAEL_LIBDIR "/gael/views/libgaelschematic.so");
  g_hash_table_insert(modules, strdup(gael_module_get_name(module)), module);

  /*
   * create and show the main window
   */
  gtk_widget_show_all(gael_main_window_new());

  /*
   * Let's loop
   */
  bonobo_ui_main ();

  return 0;
}
