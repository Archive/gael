/* gael-design.c
 * Copyright (C) 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <gtk/gtk.h>
#include "gael-design.h"
#include "gael-module.h"
#include "util/gael-intl.h"
#include <string.h>
#include <libxml/parser.h>
#include "gael-main-window.h"

extern GHashTable *modules;


/* properties */
enum {
  PROP_NONE,
  PROP_LAST
};

static void gael_design_init       (GaelDesign      *design);
static void gael_design_class_init (GaelDesignClass *klass);
static void gael_design_finalize   (GObject            *object);

static void gael_design_set_internal_property (GObject *object,
					       guint property_id,
					       const GValue *value,
					       GParamSpec *pspec);
static void gael_design_get_internal_property (GObject *object,
					       guint property_id,
					       GValue *value,
					       GParamSpec *pspec);

static GObjectClass *parent_class = NULL;

GtkType
gael_design_get_type (void)
{
  static GtkType design_type = 0;

  if (!design_type)
    {
      static const GTypeInfo design_info =
      {
	sizeof (GaelDesignClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) gael_design_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,
	
	sizeof (GaelDesign),
	0, /* n_preallocs */
	(GInstanceInitFunc) gael_design_init,
      };

      design_type = g_type_register_static (G_TYPE_OBJECT,
					       "GaelDesign",
					       &design_info, 0);
    }

  return design_type;
}

static void
gael_design_class_init (GaelDesignClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS(klass);

  parent_class = g_type_class_ref(G_TYPE_OBJECT);

  object_class->finalize = gael_design_finalize;

  object_class->set_property = gael_design_set_internal_property;
  object_class->get_property = gael_design_get_internal_property;
}

static void
gael_design_init_models_foreach(gchar      *key,
				    GaelModule *module,
				    GHashTable *hash)
{
  g_hash_table_insert(hash,
		      key,
		      gael_module_get_new_model(module));
}

static void
gael_design_init (GaelDesign *design)
{
  design->models = g_hash_table_new(g_str_hash,g_str_equal);
  design->uri        = NULL;
  design->last_ref   = 0;

  gael_design_changed( design, FALSE );

  g_hash_table_foreach(modules,
		       (GHFunc)gael_design_init_models_foreach,
		       design->models);
}

static void
gael_design_delete_models_foreach(gchar    *key,
				      GObject  *data,
				      gpointer  user_data)
{
  g_free(key);
  g_object_unref(data);
}

static void
gael_design_finalize(GObject *object)
{
  GaelDesign *design;

  g_return_if_fail(object != NULL);
  g_return_if_fail(GAEL_IS_DESIGN(object));

  design = GAEL_DESIGN(object);

  g_free(design->uri);

  g_hash_table_foreach_remove(design->models,
			      (GHRFunc)gael_design_delete_models_foreach,
			      NULL);
  g_hash_table_destroy(design->models);

  if (G_OBJECT_CLASS(parent_class)->finalize)
    (* G_OBJECT_CLASS(parent_class)->finalize) (object);
}

static void
gael_design_set_internal_property (GObject *object,
				   guint property_id,
				   const GValue *value,
				   GParamSpec *pspec)
{
  G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
}

static void
gael_design_get_internal_property (GObject *object,
				   guint property_id,
				   GValue *value,
				   GParamSpec *pspec)
{
  G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
}

GaelDesign *
gael_design_new (void)
{
  return g_object_new(GAEL_TYPE_DESIGN,NULL);
}

void
gael_design_add_component (GaelDesign *design,
			   gpointer    component)
{
  g_object_ref(G_OBJECT(component));
  design->components = g_list_append(design->components,component);
}

void
gael_design_rem_component (GaelDesign *design,
			   gpointer    component)
{
  design->components = g_list_remove(design->components,component);
  g_object_unref(G_OBJECT(component));
}

GaelModelInterface *
gael_design_get_model (GaelDesign *design,
		       gchar      *key)
{
  return g_hash_table_lookup(design->models,key);
}

void
gael_design_load (GaelDesign *design,
		  gchar      *buffer)
{
  xmlDocPtr doc;
  xmlNodePtr cur;

  doc = xmlParseFile(buffer);
  cur = xmlDocGetRootElement(doc);

  if ( !strcmp(cur->name,"design") ) {
    gchar *lastref;

    design->name = xmlGetProp(cur,"name");
    lastref      = xmlGetProp(cur,"lastref");
    design->last_ref = atoi(lastref);
    g_free(lastref);
    cur = cur->xmlChildrenNode;

    while (cur != NULL) {

      if ( !strcmp(cur->name,"component") ||
	   !strcmp(cur->name,"wire") ) {
	GaelComponent *component;

	component = g_object_new(GAEL_TYPE_COMPONENT, NULL);
	gael_component_set_design(component,(gpointer)design);
	if ( gael_component_load(component,doc,cur) == FALSE ) {
	  g_message("%s(%i): gotta handle misformated files",__FILE__,__LINE__);
	  break;
	}

	gael_design_add_component(design,component);
      }

      cur = cur->next;
    }
  } else {
    g_message("tag \"%s\" should be \"design\"",cur->name);
  }
  xmlFreeDoc(doc);
}

static void
gael_design_save_foreach(GaelComponent *comp,
			 GString       *string)
{
  g_string_append(string,gael_component_save(comp,"\t"));
}

gchar *
gael_design_save (GaelDesign *design)
{
  GString *string;

  string = g_string_new("");

  g_string_printf(string,"<design lastref=\"%i\" name=\"",design->last_ref);
  if( design->name != NULL ) {
    g_string_append(string,design->name);
  }
  g_string_append(string,"\">\n");

  g_message("%s(%i): save the models ?",__FILE__,__LINE__);

  g_list_foreach(design->components,
		 (GFunc)gael_design_save_foreach,
		 string);

  g_string_append(string,"</design>\n");

  return g_string_free(string,FALSE);
}

void
gael_design_clean ( GaelDesign *design )
{

  if ( design->name != NULL ) {
    g_free(design->name);
  }

  if ( design->uri != NULL ) {
    g_free(design->uri);
  }

  /* GList *components */
  g_list_foreach(design->components,(GFunc)g_object_unref,NULL);
  g_list_free(design->components);
  design->components = NULL;

  /* models */

  /* other things */
  design->last_ref = 0;

  gael_design_changed( design, TRUE );
}

void
gael_design_changed ( GaelDesign *design,
		      gboolean    changed )
{
  gchar *value;

  value = changed ? "1" : "0";
  design->changed = changed;
  if( design->main_window != NULL )
    gael_main_window_set_bonobo(GAEL_MAIN_WINDOW(design->main_window),"sensitive", value);
}

guint
gael_design_get_ref (GaelDesign *design)
{
  design->last_ref++;
  return design->last_ref;
}
