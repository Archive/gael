/* gael-module.c
 * Copyright (C) 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>
#include <gnome.h>

#include <gtk/gtk.h>
#include "gael-module.h"
#include "util/gael-intl.h"
#include <string.h>

/* properties */
enum {
  PROP_NONE,
  PROP_LAST
};

static void gael_module_init       (GaelModule      *module);
static void gael_module_class_init (GaelModuleClass *klass);
static void gael_module_finalize   (GObject            *object);

static void gael_module_set_internal_property (GObject *object,
					       guint property_id,
					       const GValue *value,
					       GParamSpec *pspec);
static void gael_module_get_internal_property (GObject *object,
					       guint property_id,
					       GValue *value,
					       GParamSpec *pspec);

static GObjectClass *parent_class = NULL;

GtkType
gael_module_get_type (void)
{
  static GtkType module_type = 0;

  if (!module_type)
    {
      static const GTypeInfo module_info =
      {
	sizeof (GaelModuleClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) gael_module_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,
	
	sizeof (GaelModule),
	0, /* n_preallocs */
	(GInstanceInitFunc) gael_module_init,
      };

      module_type = g_type_register_static (G_TYPE_OBJECT,
					    "GaelModule",
					    &module_info, 0);
    }

  return module_type;
}

static void
gael_module_class_init (GaelModuleClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS(klass);

  parent_class = g_type_class_ref(G_TYPE_OBJECT);

  object_class->finalize = gael_module_finalize;

  object_class->set_property = gael_module_set_internal_property;
  object_class->get_property = gael_module_get_internal_property;
}


static void
gael_module_init (GaelModule *module)
{
}

static void
gael_module_finalize(GObject *object)
{
  g_return_if_fail(object != NULL);
  g_return_if_fail(GAEL_IS_MODULE(object));

  if (G_OBJECT_CLASS(parent_class)->finalize)
    (* G_OBJECT_CLASS(parent_class)->finalize) (object);
}

static void
gael_module_set_internal_property (GObject *object,
				   guint property_id,
				   const GValue *value,
				   GParamSpec *pspec)
{
  G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
}

static void
gael_module_get_internal_property (GObject *object,
				   guint property_id,
				   GValue *value,
				   GParamSpec *pspec)
{
  G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
}

GaelViewInterface*
gael_module_get_new_view  (GaelModule *module)
{
  return module->get_new_view();
}

GaelModelInterface*
gael_module_get_new_model (GaelModule *module)
{
  return module->get_new_model();
}

GaelLibraryElement*
gael_module_get_new_elem(GaelModule *module,
			 gchar      *name,
			 gchar      *path,
			 gchar      *lib)
{
  return module->get_new_elem(name,path,lib);
}

const gchar*
gael_module_get_name (GaelModule *module)
{
  return module->get_name();
}

const gchar*
gael_module_get_name_i18n (GaelModule *module)
{
  return module->get_name_i18n();
}

const gchar*
gael_module_get_icon (GaelModule *module)
{
  return module->get_icon();
}

GaelModule*
gael_module_new(guchar *path)
{
  GaelModule *module;

  module = g_object_new(GAEL_TYPE_MODULE, NULL);
  module->path = strdup(path);

  module->handle = g_module_open (module->path, G_MODULE_BIND_LAZY);

  if (module->handle != NULL) {

    if( !g_module_symbol (module->handle, "get_new_view",  (gpointer*)&(module->get_new_view)) ) {
      g_warning (_("Could not find the get_new_view symbol '%s'"),
		 g_module_error ());
      g_object_unref(module);
      return NULL;
    }

    if( !g_module_symbol (module->handle, "get_new_model", (gpointer*)&(module->get_new_model)) ) {
      g_warning (_("Could not find the get_new_model symbol '%s'"),
		 g_module_error ());
      g_object_unref(module);
      return NULL;
    }

    if( !g_module_symbol (module->handle, "get_new_elem", (gpointer*)&(module->get_new_elem)) ) {
      g_warning (_("Could not find the get_new_elem symbol '%s'"),
		 g_module_error ());
      g_object_unref(module);
      return NULL;
    }

    if( !g_module_symbol (module->handle, "get_name", (gpointer*)&(module->get_name)) ) {
      g_warning (_("Could not find the get_name symbol '%s'"),
		 g_module_error ());
      g_object_unref(module);
      return NULL;
    }

    if( !g_module_symbol (module->handle, "get_name_i18n", (gpointer*)&(module->get_name_i18n)) ) {
      g_warning (_("Could not find the get_name_i18n symbol '%s'"),
		 g_module_error ());
      g_object_unref(module);
      return NULL;
    }

    if( !g_module_symbol (module->handle, "get_icon", (gpointer*)&(module->get_icon)) ) {
      g_warning (_("Could not find the get_modelicon symbol '%s'"),
		 g_module_error ());
      g_object_unref(module);
      return NULL;
    }

  } else {
    g_warning (_("Could not open view plugin file '%s'"),
	       g_module_error ());
    g_object_unref(module);
    return NULL;
  }

  return module;
}
