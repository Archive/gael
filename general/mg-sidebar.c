/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2002 CodeFactory AB
 * Copyright (C) 2002 Richard Hult <rhult@codefactory.se>
 * Copyright (C) 2002 Mikael Hallendal <micke@codefactory.se>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <math.h>
#include <gtk/gtk.h>
#include <libgnome/gnome-i18n.h>
#include "mg-sidebar.h"

#define DARKEN 1.4

struct _MgSidebarPriv {
	GList     *buttons;
	GtkWidget *event_box;
	GtkWidget *vbox;
};

typedef struct {
	GtkWidget *button;
	GtkWidget *label;
} ButtonEntry;

enum {
	ICON_SELECTED,
	LAST_SIGNAL
};

static void     ms_class_init           (MgSidebarClass *klass);
static void     ms_init                 (MgSidebar      *bar);
static void     ms_finalize             (GObject        *object);
static void     ms_destroy              (GtkObject      *object);
static void     ms_event_box_realize_cb (GtkWidget      *widget,
					 gpointer        user_data);

static void     ms_style_set            (GtkWidget      *widget,
					 GtkStyle       *previous_style);


static GtkVBoxClass *parent_class = NULL;
static guint signals[LAST_SIGNAL];


GtkType
mg_sidebar_get_type (void)
{
	static GtkType mg_sidebar_type = 0;

	if (!mg_sidebar_type) {
		static const GTypeInfo mg_sidebar_info = {
			sizeof (MgSidebarClass),
			NULL,		/* base_init */
			NULL,		/* base_finalize */
			(GClassInitFunc) ms_class_init,
			NULL,		/* class_finalize */
			NULL,		/* class_data */
			sizeof (MgSidebar),
			0,              /* n_preallocs */
			(GInstanceInitFunc) ms_init
		};

		mg_sidebar_type = g_type_register_static (GTK_TYPE_FRAME, "MgSidebar",
							      &mg_sidebar_info, 0);
	}
	
	return mg_sidebar_type;
}

static void
ms_class_init (MgSidebarClass *class)
{
	GObjectClass   *o_class;
	GtkObjectClass *object_class;
	GtkWidgetClass *widget_class;

	parent_class = g_type_class_peek_parent (class);

	o_class = (GObjectClass *) class;
	object_class = (GtkObjectClass *) class;
	widget_class = (GtkWidgetClass *) class;

	o_class->finalize = ms_finalize;

	object_class->destroy = ms_destroy;

	widget_class->style_set = ms_style_set;
	
	/* Signals. */
	signals[ICON_SELECTED] = g_signal_new
		("icon_selected",
		 G_TYPE_FROM_CLASS (class),
		 G_SIGNAL_RUN_LAST,
		 0,
		 NULL, NULL,
		 g_cclosure_marshal_VOID__INT,
		 G_TYPE_NONE, 1, G_TYPE_INT);
}

static void
ms_init (MgSidebar *bar)
{
	bar->priv = g_new0 (MgSidebarPriv, 1);

	bar->priv->event_box = gtk_event_box_new ();
	gtk_widget_show (bar->priv->event_box);
	g_signal_connect (bar->priv->event_box,
			  "realize",
			  G_CALLBACK (ms_event_box_realize_cb),
			  NULL);
	
	bar->priv->vbox = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (bar->priv->vbox);
	
	gtk_container_add (GTK_CONTAINER (bar), bar->priv->event_box);

	gtk_container_add (GTK_CONTAINER (bar->priv->event_box), bar->priv->vbox);
	
	gtk_container_set_border_width (GTK_CONTAINER (bar->priv->vbox), 4);
	gtk_frame_set_shadow_type (GTK_FRAME (bar), GTK_SHADOW_OUT);
}

static void
ms_finalize (GObject *object)
{
	MgSidebar *bar = MG_SIDEBAR (object);

	g_free (bar->priv);

	if (G_OBJECT_CLASS (parent_class)->finalize) {
		(* G_OBJECT_CLASS (parent_class)->finalize) (object);
	}
}

static void
ms_destroy (GtkObject *object)
{
	if (GTK_OBJECT_CLASS (parent_class)->destroy) {
		(* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
	}
}

static gboolean
ms_gdk_color_is_dark (GdkColor *color)
{
	int intensity;

	intensity = (((color->red >> 8) * 77)
		     + ((color->green >> 8) * 150)
		     + ((color->blue >> 8) * 28)) >> 8;

	return intensity < 128;
}

static guint16
ms_shift_color_component (guint16 component, float shift_by)
{
	guint16 result;
	
	if (shift_by > 1.0) {
		result = component * (2 - shift_by);
	} else {
		result = 0xffff - shift_by * (0xffff - component);
	}

	return result & 0xffff;
}

static void
ms_shift_color (GdkColor *color, float shift_by)
{
	color->red = ms_shift_color_component (color->red, shift_by);
	color->green = ms_shift_color_component (color->green, shift_by);
	color->blue = ms_shift_color_component (color->blue, shift_by);
}

static void
ms_event_box_realize_cb (GtkWidget *widget, gpointer user_data)
{
	GdkColor color = widget->style->bg[GTK_STATE_NORMAL];

	ms_shift_color (&color, DARKEN);
		
	gtk_widget_modify_bg (widget, GTK_STATE_NORMAL, &color);
}

static void
ms_style_set (GtkWidget *widget,
	      GtkStyle  *previous_style)
{
	MgSidebar *bar = MG_SIDEBAR (widget);
	GdkColor   color, white = { 0, 0xffff, 0xffff, 0xffff };
	GList     *l;
	gboolean   is_dark;

	if (GTK_WIDGET_CLASS (parent_class)->style_set) {
		GTK_WIDGET_CLASS (parent_class)->style_set (widget,
							    previous_style);
	}

	color = widget->style->bg[GTK_STATE_NORMAL];

	ms_shift_color (&color, DARKEN);
	is_dark = ms_gdk_color_is_dark (&color);
	
	for (l = bar->priv->buttons; l; l = l->next) {
		ButtonEntry *entry = l->data;
		
		gtk_widget_modify_bg (entry->button,
				      GTK_STATE_PRELIGHT,
				      &color);
		
		gtk_widget_modify_bg (entry->button,
				      GTK_STATE_ACTIVE,
				      &color);
		
		if (is_dark) {
			gtk_widget_modify_fg (entry->label,
					      GTK_STATE_NORMAL,
					      &white);
		}
	}
}

GtkWidget *
mg_sidebar_new (void)
{

	return g_object_new (MG_TYPE_SIDEBAR, NULL);
}

static void
ms_button_clicked_cb (GtkWidget *button,
		      MgSidebar *sidebar)
{
	ButtonEntry *entry;
	gint         index;

	entry = g_object_get_data (G_OBJECT (button), "entry");
	
	index = g_list_index (sidebar->priv->buttons, entry);
	
	g_signal_emit (sidebar, signals[ICON_SELECTED], 0, index);
}

void
mg_sidebar_append (MgSidebar   *sidebar,
		   const gchar *icon_filename,
		   const gchar *text)
{
	ButtonEntry *entry;
	GtkWidget   *vbox;
	GtkWidget   *image;

	g_return_if_fail (MG_IS_SIDEBAR (sidebar));

	entry = g_new0 (ButtonEntry, 1);
	
	vbox = gtk_vbox_new (FALSE, 0);

	image = gtk_image_new_from_file (icon_filename);

	entry->button = gtk_button_new ();
	g_object_set_data (G_OBJECT (entry->button), "entry", entry);
	gtk_button_set_relief (GTK_BUTTON (entry->button), GTK_RELIEF_NONE);

	sidebar->priv->buttons = g_list_append (sidebar->priv->buttons,
						entry);
	
	g_signal_connect (entry->button,
			  "clicked",
			  G_CALLBACK (ms_button_clicked_cb),
			  sidebar);

	gtk_container_add (GTK_CONTAINER (entry->button), image);
	
	gtk_box_pack_start (GTK_BOX (vbox),
			    entry->button,
			    FALSE,
			    TRUE,
			    2);

	entry->label = gtk_label_new_with_mnemonic (text);
	gtk_label_set_mnemonic_widget (GTK_LABEL (entry->label), entry->button);

	gtk_box_pack_start (GTK_BOX (vbox),
			    entry->label,
			    FALSE,
			    TRUE,
			    2);

	gtk_widget_show_all (vbox);

	gtk_box_pack_start (GTK_BOX (sidebar->priv->vbox),
			    vbox,
			    FALSE,
			    TRUE,
			    8);
}

gchar *
mg_sidebar_get_label(MgSidebar *sidebar,
		     gint       position)
{
	GtkWidget *label;

	label = ((ButtonEntry*)(g_list_nth(sidebar->priv->buttons,position)->data))->label;

	if (label == NULL) {
		return NULL;
	} else {
		return (gchar*) gtk_label_get_text(GTK_LABEL(label));
	}
}
