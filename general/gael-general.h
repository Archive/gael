/* gael-general.h
 * Copyright (C) 2001-2002 Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GAEL_GENERAL_H__
#define __GAEL_GENERAL_H__

typedef struct {
  guint   type;
  gdouble x, y;
} GaelPoint;

typedef struct {
  guint   type;
  gdouble x1, y1;
  gdouble x2, y2;
} GaelLine;

typedef struct {
  guint  type;
  GList *points;
} GaelLines;

typedef struct {
  guint    type;
  gboolean filled;
  gdouble  left, right;
  gdouble  top,  bottom;
} GaelRectangle;

typedef struct {
  guint    type;
  gboolean filled;
  gdouble  x, y;
  gdouble  width, height;
  gdouble  angle1, angle2;
} GaelArc;

typedef struct {
  guint     type;
  gboolean  filled;
  GList    *points;
} GaelPolygon;

typedef struct {
  guint    type;
  gdouble  x, y;
  gchar   *text;
} GaelText;

typedef struct {
  guint    type;
  gdouble  x, y;
  guint    id;
} GaelConnection;

typedef struct {
  guint    type;
  gdouble  x, y;
  gchar   *name;
  gchar   *value;
} GaelProperty;

typedef union {
  guint          type;
  GaelPoint      point;
  GaelLine       line;
  GaelLines      lines;
  GaelRectangle  rectangle;
  GaelArc        arc;
  GaelPolygon    polygon;
  GaelText       text;
  GaelPoint      attract;
  GaelPoint      attracted;
  GaelConnection connection;
  GaelProperty   property;
} GaelGeom;

typedef enum {
  GAEL_POINT,
  GAEL_LINE,
  GAEL_LINES,
  GAEL_RECTANGLE,
  GAEL_ARC,
  GAEL_POLYGON,
  GAEL_TEXT,
  GAEL_ATTRACT,
  GAEL_ATTRACTED,
  GAEL_CONNECTION,
  GAEL_PROPERTY
} Gaeltype;

typedef enum
{
  gct_nothing,
  gct_component,
  gct_wire
} GaelComponentType;

#endif
