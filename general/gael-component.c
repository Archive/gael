/* gael-component.c
 * Copyright (C) 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <gtk/gtk.h>
#include "gael-component.h"
#include "gael-design.h"
#include <util/gael-intl.h>
#include <string.h>
#include "gael-module.h"

extern GHashTable *modules;

/* properties */
enum {
  PROP_NONE,
  PROP_REF,
  PROP_LAST
};

static void gael_component_init       (GaelComponent      *component);
static void gael_component_class_init (GaelComponentClass *klass);
static void gael_component_finalize   (GObject            *object);

static void gael_component_set_internal_property (GObject *object,
						  guint property_id,
						  const GValue *value,
						  GParamSpec *pspec);
static void gael_component_get_internal_property (GObject *object,
						  guint property_id,
						  GValue *value,
						  GParamSpec *pspec);

static void gael_component_set_property_foreach (gchar                  *key,
						 GaelComponentInterface *icomp,
						 gchar                  *name);
static void gael_component_select_foreach       (gchar                  *key,
						 GaelComponentInterface *icomp,
						 gboolean               *select);


static GObjectClass *parent_class = NULL;

GtkType
gael_component_interface_get_type (void)
{
  static GtkType component_interface_type = 0;

  if (!component_interface_type)
    {
      static const GTypeInfo component_interface_info =
      {
	sizeof (GaelComponentInterfaceClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
      };

      component_interface_type = g_type_register_static (G_TYPE_INTERFACE,
							 "GaelComponentInterface",
							 &component_interface_info, 0);
      g_type_interface_add_prerequisite (component_interface_type, G_TYPE_OBJECT);
    }

  return component_interface_type;
}

GtkType
gael_component_get_type (void)
{
  static GtkType component_type = 0;

  if (!component_type)
    {
      static const GTypeInfo component_info =
      {
	sizeof (GaelComponentClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) gael_component_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,
	
	sizeof (GaelComponent),
	0, /* n_preallocs */
	(GInstanceInitFunc) gael_component_init,
      };

      component_type = g_type_register_static (G_TYPE_OBJECT,
					       "GaelComponent",
					       &component_info, 0);
    }

  return component_type;
}

static void
gael_component_class_init (GaelComponentClass *klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS(klass);

  parent_class = g_type_class_ref(G_TYPE_OBJECT);

  object_class->finalize = gael_component_finalize;

  object_class->set_property = gael_component_set_internal_property;
  object_class->get_property = gael_component_get_internal_property;

  g_object_class_install_property (object_class,
				   PROP_REF,
				   g_param_spec_uint ("ref", _("reference"),
						      _("The reference"),
						      0, G_MAXUINT, 0,
						      G_PARAM_READWRITE));
}


static void
gael_component_init (GaelComponent *component)
{
  component->data = g_hash_table_new_full( (GHashFunc)g_str_hash,
					   (GEqualFunc)g_str_equal,
					   (GDestroyNotify)g_free,
					   (GDestroyNotify)g_object_unref );

  component->properties = g_hash_table_new_full( (GHashFunc)g_str_hash,
						 (GEqualFunc)g_str_equal,
						 (GDestroyNotify)g_free,
						 (GDestroyNotify)g_free );
  component->pins = g_ptr_array_new();
  component->ref  = 0;
}

static void
gael_component_finalize_foreach_data ( gpointer key,
				       gpointer value,
				       gpointer user_data )
{
  gael_component_interface_delete(GAEL_COMPONENT_INTERFACE(value));
}

static void
gael_component_finalize(GObject *object)
{
  g_return_if_fail(object != NULL);
  g_return_if_fail(GAEL_IS_COMPONENT(object));

  g_hash_table_foreach(GAEL_COMPONENT(object)->data,gael_component_finalize_foreach_data,NULL);
  g_hash_table_destroy(GAEL_COMPONENT(object)->data);

  g_hash_table_destroy(GAEL_COMPONENT(object)->properties);
  g_ptr_array_free(GAEL_COMPONENT(object)->pins,FALSE);

  if (G_OBJECT_CLASS(parent_class)->finalize)
    (* G_OBJECT_CLASS(parent_class)->finalize) (object);
}

static void
gael_component_set_internal_property (GObject *object,
				      guint property_id,
				      const GValue *value,
				      GParamSpec *pspec)
{
  GaelComponent *comp;

  comp = GAEL_COMPONENT(object);

  switch (property_id) {

    case PROP_REF:
      comp->ref = g_value_get_uint(value);

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
  }
}

static void
gael_component_get_internal_property (GObject *object,
				      guint property_id,
				      GValue *value,
				      GParamSpec *pspec)
{
  GaelComponent *comp;

  comp = GAEL_COMPONENT(object);

  switch (property_id) {

    case PROP_REF:
      g_value_set_uint(value,comp->ref);

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
  }
}


static void
gael_component_set_property_foreach (gchar                  *key,
				     GaelComponentInterface *icomp,
				     gchar                  *name)
{
  gael_component_interface_property_changed(icomp,name);
}

void
gael_component_set_property (GaelComponent *component,
			     gchar *name,
			     gchar *value)
{
  g_hash_table_remove (component->properties, name);
  g_hash_table_insert (component->properties, strdup(name), strdup(value));

  g_hash_table_foreach(component->data,
		       (GHFunc)gael_component_set_property_foreach,
		       name);
}

static void
gael_component_select_foreach (gchar                  *key,
			       GaelComponentInterface *icomp,
			       gboolean               *select)
{
  gael_component_interface_select(icomp,*select);
}

void
gael_component_select (GaelComponent *component,
		       gboolean select)
{
  g_hash_table_foreach(component->data,
		       (GHFunc)gael_component_select_foreach,
		       &select);
}

static void
gael_component_new_foreach_module (gpointer key,
				   gpointer value,
				   gpointer user_data)
{
  GaelComponentInterface *icomp;
  GaelComponent          *component;
  GaelModelInterface     *imodel;
  GaelDesign             *design;

  component = GAEL_COMPONENT(((gpointer*)user_data)[0]);
  design    = GAEL_DESIGN(((gpointer*)user_data)[1]);
  imodel = gael_design_get_model(design,key);
  icomp =
    GAEL_COMPONENT_INTERFACE(gael_model_interface_get_component(imodel,
								component,
								gct_wire,
								NULL,
								NULL));
  g_hash_table_insert(component->data,strdup(key),icomp);
  g_object_ref(icomp);
}

GaelComponent *
gael_component_new (GaelDesign        *design,
		    GaelLibrarian     *librarian,
		    GaelComponentType  type,
		    gchar             *lib,
		    gchar             *name)
{
  GaelComponent      *component;

  component = g_object_new(GAEL_TYPE_COMPONENT, NULL);
  component->type = type;

  component->ref = gael_design_get_ref(design);

  if ( type == gct_component ) {
    gchar              *gtype;

    gtype = gael_librarian_get_elem_type(librarian,lib,name);

    if( strcmp(gtype,"link") == 0 ) {

      component->lib  = strdup(lib);
      component->name = strdup(name);
      g_message("%s(%i) implement link component",__FILE__,__LINE__);

    } else {
      GaelComponentInterface *icomp;
      GaelModelInterface *imodel;

      component->lib  = NULL;
      component->name = NULL;

      /* Get the component corresponding to the type. */

      imodel = gael_design_get_model(design,gtype);

      icomp = 
	GAEL_COMPONENT_INTERFACE(gael_model_interface_get_component(imodel,
								    component,
								    gct_component,
								    lib,
								    name));
      g_hash_table_insert(component->data,strdup(gtype),icomp);
      g_object_ref(icomp);
    }

  }

  if ( type == gct_wire ) {
    gpointer data[2];

    data[0] = component;
    data[1] = design;
    g_hash_table_foreach(modules,gael_component_new_foreach_module,data);
  }

  component->design = (gpointer)design;
  gael_design_add_component(design,component);
  g_object_unref(component);

  return component;
}

static void
gael_component_save_foreach_subcomp (gchar                  *key,
				     GaelComponentInterface *icomp,
				     gpointer                data[2])
{
  GString *string;
  GString *prepend = g_string_new((gchar*)data[1]);

  string = (GString*)data[0];

  g_string_append(prepend,"\t");
  g_string_append(string,gael_component_interface_save(icomp,prepend->str));
  g_string_free(prepend,TRUE);
}

static void
gael_component_save_foreach_properties (gchar    *name,
					gchar    *value,
					gpointer  data[2])
{
  GString *string  = (GString*)data[0];
  gchar   *prepend = (gchar*)data[1];

  g_string_append_printf(string,"%s\t\t<property name=\"%s\" value=\"%s\"/>\n",
			 prepend, name, value);
}

gchar *
gael_component_save (GaelComponent *component,
		     gchar         *prepend)
{
  gpointer data[2];
  GString *string = g_string_new("");

  if ( component->type == gct_component ) {
    g_string_append(string, prepend);
    g_string_append_printf(string, "<component ref=\"%i\" name=\"",component->ref);
    if( component->name != NULL ) {
      g_string_append(string, component->name);
    }
    g_string_append(string, "\" lib=\"");
    if( component->lib != NULL ) {
      g_string_append(string, component->lib);
    }
    g_string_append(string,"\">\n");

    data[0] = string;
    data[1] = prepend;

    g_hash_table_foreach (component->data,
			  (GHFunc)gael_component_save_foreach_subcomp,
			  data);

    g_string_append(string, prepend);
    g_string_append(string, "\t<properties>\n");
    g_hash_table_foreach (component->properties,
			  (GHFunc)gael_component_save_foreach_properties,
			  data);
    g_string_append(string, prepend);
    g_string_append(string, "\t</properties>\n");

    g_string_append(string, prepend);
    g_string_append(string, "\t<pins>\n");
    g_message("%s(%i): save the pins",__FILE__,__LINE__);
    g_string_append(string, prepend);
    g_string_append(string, "\t</pins>\n");

    g_string_append(string, prepend);
    g_string_append(string, "</component>\n");

  }

  if ( component->type == gct_wire ) {
    g_string_append(string, prepend);
    g_string_append_printf(string, "<wire ref=\"%i\">\n",component->ref);

    data[0] = string;
    data[1] = prepend;

    g_hash_table_foreach (component->data,
			  (GHFunc)gael_component_save_foreach_subcomp,
			  data);

    g_string_append(string, prepend);
    g_string_append(string, "</wire>\n");
  }

  return g_string_free(string,FALSE);
}

gboolean
gael_component_load (GaelComponent *component,
		     xmlDocPtr      doc,
		     xmlNodePtr     cur)
{
  xmlNodePtr base;
  gchar *ref;

  if ( !strcmp(cur->name,"component") ) {

    component->type = gct_component;
    component->name = xmlGetProp(cur,"name");
    component->lib  = xmlGetProp(cur,"lib");
    ref = xmlGetProp(cur,"ref");
    component->ref = atoi(ref);
    g_free(ref);

    if ( component->name == NULL ) {
      g_free(component->name);
      component->name = NULL;
    }

    if ( component->lib == NULL ) {
      g_free(component->lib);
      component->lib = NULL;
    }

    cur = cur->xmlChildrenNode;

    while (cur != NULL) {

      if ( !strcmp(cur->name,"text") ) {

      } else if ( !strcmp(cur->name,"properties") ) {
	base = cur->xmlChildrenNode;
	while (base != NULL) {
	  if ( !strcmp(base->name,"property") ) {
	    gchar *name, *value;

	    name  = xmlGetProp(base,"name");
	    value = xmlGetProp(base,"value");
	    gael_component_set_property(component,name,value);
	    g_free(name);
	    g_free(value);
	  }
	  base = base->next;
	}
      
      } else if ( !strcmp(cur->name,"pins") ) {
	base = cur->xmlChildrenNode;
	while (base != NULL) {
	  if ( !strcmp(base->name,"property") ) {
	    g_message("%s(%i): add pins loading",__FILE__,__LINE__);
	  }
	  base = base->next;
	}

      } else {
	GaelModelInterface *imodel;
	GaelComponentInterface *icomp;
	gchar *name, *lib;

	imodel = gael_design_get_model(GAEL_DESIGN(component->design), (gchar*)cur->name);
	if( imodel == NULL ) {
	  g_message("%s(%i): couldn't find a model for module %s",__FILE__,__LINE__,cur->name);
	}

	name  = xmlGetProp(cur,"name");
	lib   = xmlGetProp(cur,"lib");
	icomp = GAEL_COMPONENT_INTERFACE(gael_model_interface_get_component(imodel,
									    component,
									    gct_component,
									    lib,
									    name));
	g_free(name);
	g_free(lib);

	if ( !gael_component_interface_load(icomp,doc,cur) ) {
	  return FALSE;
	}
	g_hash_table_insert(component->data,strdup((gchar*)cur->name),icomp);
	g_object_ref(icomp);
      }

      cur = cur->next;
    }
    return TRUE;
  }

  if ( !strcmp(cur->name,"wire") ) {

    component->type = gct_wire;
    ref = xmlGetProp(cur,"ref");
    component->ref = atoi(ref);
    g_free(ref);

    cur = cur->xmlChildrenNode;

    while (cur != NULL) {

      if ( !strcmp(cur->name,"text") ) {

      } else if ( !strcmp(cur->name,"properties") ) {
	g_message("%s(%i): TODO add the wire properties",__FILE__,__LINE__);      
      } else if ( !strcmp(cur->name,"pins") ) {
	g_message("%s(%i): TODO add the wire pins",__FILE__,__LINE__);
      } else {
	GaelModelInterface *imodel;
	GaelComponentInterface *icomp;

	imodel = gael_design_get_model(GAEL_DESIGN(component->design), (gchar*)cur->name);
	if( imodel == NULL ) {
	  g_message("%s(%i): couldn't find a model for module %s",__FILE__,__LINE__,cur->name);
	}

	icomp = GAEL_COMPONENT_INTERFACE(gael_model_interface_get_component(imodel,
									    component,
									    gct_wire,
									    NULL,
									    NULL));
	if ( !gael_component_interface_load(icomp,doc,cur) ) {
	  return FALSE;
	}
	g_hash_table_insert(component->data,strdup((gchar*)cur->name),icomp);
	g_object_ref(icomp);
      }

      cur = cur->next;
    }

    return TRUE;
  }

  return FALSE;
}

void
gael_component_set_design(GaelComponent *component,
			  gpointer      *design)
{
  component->design = design;
}

GaelComponentInterface *
gael_component_get_interface (GaelComponent *component,
			      gchar         *key)
{
  return g_hash_table_lookup (component->data,key);
}

void
gael_component_connect (GaelComponent *component,
			gint           pin_number,
			GaelComponent *wire)
{
  g_message("%s(%i): Connected pin %i of component %i with %i",__FILE__,__LINE__,pin_number,component->ref,wire->ref);
}

void
gael_component_interface_set_parent (GaelComponentInterface *component_interface,
				     GaelComponent          *component)
{
  g_return_if_fail (GAEL_IS_COMPONENT_INTERFACE(component_interface));

  GAEL_COMPONENT_INTERFACE_GET_CLASS(component_interface)->set_parent( component_interface, component );
}

void
gael_component_interface_property_changed (GaelComponentInterface *component_interface,
					   gchar                  *name)
{
  g_return_if_fail (GAEL_IS_COMPONENT_INTERFACE(component_interface));

  GAEL_COMPONENT_INTERFACE_GET_CLASS(component_interface)->property_changed( component_interface, name );
}

void
gael_component_interface_select (GaelComponentInterface *component_interface, gboolean select)
{
  g_return_if_fail (GAEL_IS_COMPONENT_INTERFACE(component_interface));

  GAEL_COMPONENT_INTERFACE_GET_CLASS(component_interface)->select( component_interface, select );
}

void
gael_component_interface_delete ( GaelComponentInterface *component_interface )
{
  g_return_if_fail (GAEL_IS_COMPONENT_INTERFACE(component_interface));

  GAEL_COMPONENT_INTERFACE_GET_CLASS(component_interface)->delete( component_interface );
}

void
gael_component_interface_place ( GaelComponentInterface *component_interface )
{
  g_return_if_fail (GAEL_IS_COMPONENT_INTERFACE(component_interface));

  GAEL_COMPONENT_INTERFACE_GET_CLASS(component_interface)->place( component_interface );
}

gchar *
gael_component_interface_save (GaelComponentInterface *icomp,
			       gchar                  *prepend)
{
  g_return_val_if_fail (GAEL_IS_COMPONENT_INTERFACE(icomp),"");

  return GAEL_COMPONENT_INTERFACE_GET_CLASS(icomp)->save( icomp, prepend );
}

gboolean
gael_component_interface_load (GaelComponentInterface *icomp,
			       xmlDocPtr      doc,
			       xmlNodePtr     cur)
{
  g_return_val_if_fail (GAEL_IS_COMPONENT_INTERFACE(icomp),FALSE);

  return GAEL_COMPONENT_INTERFACE_GET_CLASS(icomp)->load( icomp, doc, cur );
}
