/* gael-main-window.c
 * Copyright (C) 2002  Xavier Ordoquy
 * Copyright (C) 2002 CodeFactory AB
 * Copyright (C) 2002 Richard Hult <rhult@codefactory.se>
 * Copyright (C) 2002 Mikael Hallendal <micke@codefactory.se>
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <config.h>
#include <gnome.h>

#include "util/gael-marshal.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <libgnomeui/libgnomeui.h>
#include <gtk/gtk.h>
#include "gael-module.h"
#include "gael-main-window.h"
#include "mg-sidebar.h"
#include "gael-view-interface.h"
#include "gael-design.h"

#include "config.h"

extern GHashTable *modules;

struct _GaelMainWindowPriv {

  BonoboUIContainer *ui_container;
  BonoboUIComponent *ui_component;

  GtkWidget         *notebook;
  GaelViewInterface *current_view;

  GHashTable        *views;

  GaelDesign        *design;

  GaelLibrarian     *librarian;
};

/* Signals */
enum {
  CLOSED,
  NEW_WINDOW,
  EXIT,
  LAST_SIGNAL
};

static void     gael_class_init           (GaelMainWindowClass *klass);
static void     gael_init                 (GaelMainWindow      *window);
static void     gael_finalize             (GObject           *object);
static void     gael_populate             (GaelMainWindow      *window);

static void     gael_main_window_view_selected (MgSidebar      *sidebar,
						gint            index,
						GaelMainWindow *window);
static void     gael_new_cb               (BonoboUIComponent *component, 
					   gpointer           data, 
					   const char        *cname);
static void     gael_open_cb              (BonoboUIComponent *component,
					   gpointer           data,
					   const char        *cname);
static void     gael_save_as_cb           (BonoboUIComponent *component,
					   gpointer           data,
					   const char        *cname);
static void     gael_save_cb              (BonoboUIComponent *component,
					   gpointer           data,
					   const char        *cname);
static void     gael_print_setup_cb       (BonoboUIComponent *component,
					   gpointer           data,
					   const char        *cname);
static void     gael_print_preview_cb     (BonoboUIComponent *component,
					   gpointer           data,
					   const char        *cname);
static void     gael_properties_cb        (BonoboUIComponent *component,
					   gpointer           data, 
					   const char        *cname);
static void     gael_close_cb             (BonoboUIComponent *component,
					   gpointer           data,
					   const char        *cname);
static void     gael_exit_cb              (BonoboUIComponent *component,
					   gpointer           data,
					   const char        *cname);
static void     gael_undo_cb              (BonoboUIComponent *component,
					   gpointer           data,
					   const char        *cname);
static void     gael_redo_cb              (BonoboUIComponent *component,
					   gpointer           data,
					   const char        *cname);
static void     gael_project_props_cb     (BonoboUIComponent *component,
					   gpointer           data,
					   const char        *cname);
static void     gael_preferences_cb       (BonoboUIComponent *component,
					   gpointer           data,
					   const char        *cname);
static void     gael_about_cb             (BonoboUIComponent *component, 
					   gpointer           data,
					   const char        *cname);
static void     gael_window_delete_cb     (GaelMainWindow      *window);

static gboolean gael_confirm_exit_run     (GaelMainWindow      *window);
static gboolean gael_do_save              (GaelMainWindow      *window);
static gboolean gael_do_save_as           (GaelMainWindow      *window);

/* Candidate for moving to src/widgets/GaelDialogButton? */
static GtkWidget * gael_create_dialog_button (const gchar    *icon_name,
					      const gchar    *text);

static BonoboWindowClass *parent_class = NULL;
static guint signals[LAST_SIGNAL];

static BonoboUIVerb verbs[] = {
  BONOBO_UI_VERB ("FileNew",		gael_new_cb),
  BONOBO_UI_VERB ("FileOpen",		gael_open_cb),
  BONOBO_UI_VERB ("FileSave",		gael_save_cb),
  BONOBO_UI_VERB ("FileSaveAs",		gael_save_as_cb),
  BONOBO_UI_VERB ("FileProperties",	gael_properties_cb),
  BONOBO_UI_VERB ("FilePrintSetup",	gael_print_setup_cb),
  BONOBO_UI_VERB ("FilePrint",		gael_print_preview_cb),
  BONOBO_UI_VERB ("FilePrintPreview",	gael_print_preview_cb),
  BONOBO_UI_VERB ("FileClose",		gael_close_cb),
  BONOBO_UI_VERB ("FileExit",		gael_exit_cb),

  BONOBO_UI_VERB ("EditUndo",		gael_undo_cb),
  BONOBO_UI_VERB ("EditRedo",		gael_redo_cb),
  BONOBO_UI_VERB ("EditProjectProps",	gael_project_props_cb),

  BONOBO_UI_VERB ("PreferencesEditPreferences",	gael_preferences_cb),

  BONOBO_UI_VERB ("HelpAbout",		gael_about_cb),

  BONOBO_UI_VERB_END
};

GtkType
gael_main_window_get_type (void)
{
  static GtkType type = 0;

  if (!type) {
    static const GTypeInfo info = {
      sizeof (GaelMainWindowClass),
      NULL,		/* base_init */
      NULL,		/* base_finalize */
      (GClassInitFunc) gael_class_init,
      NULL,		/* class_finalize */
      NULL,		/* class_data */
      sizeof (GaelMainWindow),
      0,              /* n_preallocs */
      (GInstanceInitFunc) gael_init
    };

    type = g_type_register_static (BONOBO_TYPE_WINDOW,
				   "GaelMainWindow", &info, 0);
  }
	
  return type;
}

static void
gael_class_init (GaelMainWindowClass *klass)
{
  GObjectClass *o_class;
	
  parent_class = g_type_class_peek_parent (klass);

  o_class = (GObjectClass *) klass;

  /* GObject functions */
  o_class->finalize = gael_finalize;

  /* Signals */
  signals[CLOSED] = g_signal_new 
    ("closed",
     G_TYPE_FROM_CLASS (klass),
     G_SIGNAL_RUN_LAST,
     0, /*G_STRUCT_OFFSET (GaelMainWindowClass, method), */
     NULL, NULL,
     gael_marshal_VOID__VOID,
     G_TYPE_NONE, 0);
}

static void
gael_init (GaelMainWindow *window)
{
  GaelMainWindowPriv *priv;

  priv = g_new0 (GaelMainWindowPriv, 1);
  window->priv = priv;

  priv->ui_container = bonobo_window_get_ui_container (BONOBO_WINDOW (window));

  priv->ui_component = bonobo_ui_component_new ("Gael");

  bonobo_ui_component_set_container (priv->ui_component,
				     BONOBO_OBJREF (priv->ui_container),
				     NULL);

  priv->design    = gael_design_new();
  priv->librarian = g_object_new(GAEL_TYPE_LIBRARIAN, NULL);
  priv->views     = g_hash_table_new(g_str_hash,g_str_equal);
  priv->design->main_window = (gpointer)window;
}

static void
gael_finalize (GObject *object)
{
  GaelMainWindow *window = GAEL_MAIN_WINDOW (object);

  g_message("%s(%i): free the view hash table",__FILE__,__LINE__);

  g_free (window->priv);

  if (G_OBJECT_CLASS (parent_class)->finalize) {
    (* G_OBJECT_CLASS (parent_class)->finalize) (object);
  }
}

static void
gael_populate_foreach (gchar      *key,
		       GaelModule *module,
		       gpointer    user_data)
{
  GaelMainWindowPriv *priv;
  GaelViewInterface  *iview;
  GtkWidget          *widget;
  GtkWidget          *sidebar;
  GaelMainWindow     *window;

  window  = GAEL_MAIN_WINDOW(((gpointer*)user_data)[0]);
  sidebar = GTK_WIDGET(((gpointer*)user_data)[1]);
  priv = window->priv;

  /*
   * Create the view.
   */
  iview  = gael_module_get_new_view(module);
  g_hash_table_insert(priv->views, key, iview);
  widget = gael_view_interface_get_widget(iview);

  gael_view_interface_set_parent(iview, (gpointer)window);
  gael_view_interface_set_model(iview, gael_design_get_model(priv->design,key));


  gtk_widget_show (widget);

  mg_sidebar_append (MG_SIDEBAR (sidebar),
		     gael_module_get_icon  (module),
		     gael_module_get_name (module));

  gtk_notebook_append_page (GTK_NOTEBOOK (priv->notebook),
			    widget,
			    gtk_label_new (gael_module_get_name (module)));
}

static void
gael_populate (GaelMainWindow *window)
{
  GaelMainWindowPriv *priv;
  GtkWidget          *hbox;
  GtkWidget          *sidebar;
  gpointer            user_data[2];

  g_return_if_fail (GAEL_IS_MAIN_WINDOW (window));

  priv = window->priv;

  bonobo_ui_engine_config_set_path (
				    bonobo_window_get_ui_engine (BONOBO_WINDOW (window)),
				    "/apps/gael/ui_config/bonobo");

  bonobo_ui_component_freeze (priv->ui_component, NULL);

  bonobo_ui_util_set_ui (priv->ui_component,
			 GAEL_DATADIR,
			 GAEL_DATADIR "/gnome-2.0/ui/GNOME_Gael_MainWindow.ui",
			 "gael",
			 NULL);

  bonobo_ui_component_add_verb_list_with_data (priv->ui_component,
					       verbs, window);
	
  bonobo_ui_component_thaw (priv->ui_component, NULL);

  hbox = gtk_hbox_new (FALSE, 0);

  sidebar = mg_sidebar_new ();
  gtk_box_pack_start (GTK_BOX (hbox), sidebar, FALSE, TRUE, 0); 
  g_signal_connect (sidebar, 
		    "icon-selected",
		    G_CALLBACK (gael_main_window_view_selected),
		    window);

  priv->notebook = gtk_notebook_new ();
  gtk_notebook_set_show_tabs (GTK_NOTEBOOK (priv->notebook), FALSE);
  gtk_box_pack_start (GTK_BOX (hbox), priv->notebook, TRUE, TRUE, 0); 

  bonobo_window_set_contents (BONOBO_WINDOW (window), hbox);

  user_data[0] = window;
  user_data[1] = sidebar;

  g_hash_table_foreach(modules, (GHFunc)gael_populate_foreach, user_data);

  gael_main_window_view_selected (MG_SIDEBAR (sidebar), 0, window);
}

static void
gael_main_window_view_selected (MgSidebar    *sidebar,
				gint          index,
				GaelMainWindow *window)
{
  GaelMainWindowPriv *priv;
  GaelViewInterface  *iview;
  gchar *name;

  priv = window->priv;

  name = mg_sidebar_get_label(sidebar,index);
  iview = g_hash_table_lookup(priv->views,name);

  if (priv->current_view != NULL) {
    gael_view_interface_deactivate (GAEL_VIEW_INTERFACE(priv->current_view));
  }

  if ( iview != NULL ) {
    gael_view_interface_activate (GAEL_VIEW_INTERFACE(iview),
				  priv->ui_container);
  }

  gtk_notebook_set_current_page (GTK_NOTEBOOK (priv->notebook),
				 index);

  priv->current_view = iview;
}

void
gael_new_cb (BonoboUIComponent *component,
	     gpointer           data, 
	     const char        *cname)
{
  GaelMainWindow     *window;
  GaelMainWindowPriv *priv;

  g_return_if_fail (GAEL_IS_MAIN_WINDOW (data));

  window = GAEL_MAIN_WINDOW (data);
  priv   = window->priv;

  gael_design_clean(priv->design);
}

void
gael_open_cb (BonoboUIComponent *component,
	      gpointer           data,
	      const char        *cname)
{
  GaelMainWindowPriv *priv;
  GtkWidget        *file_sel;
  gint              response;
  const gchar      *filename = NULL;

  g_return_if_fail (GAEL_IS_MAIN_WINDOW (data));

  priv = GAEL_MAIN_WINDOW(data)->priv;

  file_sel = gtk_file_selection_new (_("Load a file"));

  gtk_window_set_modal (GTK_WINDOW (file_sel), TRUE);

  gtk_widget_show (file_sel);

  response = gtk_dialog_run (GTK_DIALOG (file_sel));

  if (response == GTK_RESPONSE_OK) {
    struct stat sb;

    filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (file_sel));
    if ((stat (filename, &sb) == 0) && S_ISDIR (sb.st_mode)) {
      filename = NULL;
    }
  }

  gtk_widget_destroy (file_sel);

  if (filename != NULL) {
    gchar *uri;

    /* Load the file. */

    uri = g_strconcat ("gael-1:", filename, NULL);

    gael_design_clean(priv->design);
    gael_design_load(priv->design,(gchar*)filename);

    g_free (uri);

  }
}

void
gael_save_cb (BonoboUIComponent *component,
	      gpointer           data,
	      const char        *cname)
{
  GaelMainWindow     *window;

  g_return_if_fail (GAEL_IS_MAIN_WINDOW (data));

  window = GAEL_MAIN_WINDOW(data);

  gael_do_save(window);
}

void
gael_save_as_cb (BonoboUIComponent *component,
		 gpointer           data,
		 const char        *cname)
{
  GaelMainWindow     *window;

  g_return_if_fail (GAEL_IS_MAIN_WINDOW (data));

  window = GAEL_MAIN_WINDOW (data);

  gael_do_save_as(window);
}

void
gael_print_setup_cb (BonoboUIComponent *component,
		     gpointer           data,
		     const char        *cname)
{
  g_return_if_fail (GAEL_IS_MAIN_WINDOW (data));
  g_message("gael_print_setup_cb");
}

void
gael_print_preview_cb (BonoboUIComponent *component,
		       gpointer           data,
		       const char        *cname)
{
  g_return_if_fail (GAEL_IS_MAIN_WINDOW (data));
  g_message("gael_print_preview_cb");
}

void
gael_properties_cb (BonoboUIComponent *component,
		    gpointer           data, 
		    const char        *cname)
{
  g_return_if_fail (GAEL_IS_MAIN_WINDOW (data));
  g_message("gael_properties_cb");
}

void
gael_close_cb (BonoboUIComponent *component,
	       gpointer           data,
	       const char        *cname)
{
  GaelMainWindow     *window;
  GaelMainWindowPriv *priv;
  gboolean            exit = FALSE;

  g_return_if_fail (GAEL_IS_MAIN_WINDOW (data));

  window = GAEL_MAIN_WINDOW (data);
  priv   = window->priv;

  if (gael_confirm_exit_run (window)) {
    g_signal_emit (GAEL_MAIN_WINDOW (data), signals[CLOSED], 0, NULL);
    gtk_widget_destroy (GTK_WIDGET (data));
    exit = TRUE;
  }
  if (exit) {
    gtk_main_quit();
  }
}

void
gael_exit_cb (BonoboUIComponent *component,
	      gpointer           data,
	      const char        *cname)
{
  g_return_if_fail (GAEL_IS_MAIN_WINDOW (data));

  gael_close_cb(component,data,cname);
}

void
gael_undo_cb (BonoboUIComponent *component,
	      gpointer           data,
	      const char        *cname)
{
  g_return_if_fail (GAEL_IS_MAIN_WINDOW (data));
}

void
gael_redo_cb (BonoboUIComponent *component,
	      gpointer           data,
	      const char        *cname)
{
  g_return_if_fail (GAEL_IS_MAIN_WINDOW (data));
}

void
gael_project_props_cb (BonoboUIComponent *component,
		       gpointer           data,
		       const char        *cname)
{
  g_return_if_fail (GAEL_IS_MAIN_WINDOW (data));
}

void
gael_preferences_cb (BonoboUIComponent *component,
		     gpointer           data,
		     const char        *cname)
{
  g_return_if_fail (GAEL_IS_MAIN_WINDOW (data));
}

void
gael_about_cb (BonoboUIComponent *component, 
	       gpointer           data,
	       const char        *cname)
{
    GtkWidget   *about;
    GtkWidget   *hbox;
    GtkWidget   *href;
    GdkPixbuf   *logo;

    const gchar *authors[] = {
    "Xavier Ordoquy <xordoquy@users.sourceforge.net>",
    NULL
    };

    logo = gdk_pixbuf_new_from_file (GAEL_DATADIR "/pixmaps/gael/splashgael2-s.png", NULL);

    about = gnome_about_new ("Gael", VERSION,
    _("(C) 2002 Xavier Ordoquy"),
    _("An electronic design application for the GNOME desktop"),
    authors,
    NULL,
    NULL,
    logo);

    if( logo != NULL )
      gdk_pixbuf_unref (logo);

    hbox = gtk_hbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG (about)->vbox), hbox, FALSE, FALSE, 0);
	
    href = gnome_href_new ("http://gael.sourceforge.net", _("Gael Homepage"));
    gtk_box_pack_start (GTK_BOX (hbox), href, TRUE, TRUE, 0);

    gtk_widget_show_all (about);
}

void
gael_window_delete_cb (GaelMainWindow *window)
{
  g_return_if_fail (GAEL_IS_MAIN_WINDOW (window));

  if (gael_confirm_exit_run(window)) {
    g_signal_emit (window, signals[CLOSED], 0, NULL);
    gtk_widget_destroy(GTK_WIDGET(window));
  }

  gtk_main_quit();
}

static gboolean
gael_confirm_exit_run (GaelMainWindow *window)
{
  GaelMainWindowPriv *priv;
  gchar              *project_name;
  GtkWidget          *dialog;
  GtkWidget          *quit_button, *cancel_button, *save_button;
  gint                ret;
	
  priv = window->priv;

  if ( priv->design->changed == FALSE ) {
    return TRUE;
  }

  project_name = "GAEL";

  dialog = gtk_message_dialog_new (GTK_WINDOW (window),
				   GTK_DIALOG_MODAL |
				   GTK_DIALOG_DESTROY_WITH_PARENT,
				   GTK_MESSAGE_WARNING,
				   GTK_BUTTONS_NONE,
				   _("<b>Save changes to document '%s' before closing?</b>\n"),
				   project_name);
	
  g_free (project_name);
	
  g_object_set (G_OBJECT (GTK_MESSAGE_DIALOG (dialog)->label),
		"use-markup", TRUE,
		"wrap", FALSE,
		NULL);

  quit_button   = gael_create_dialog_button (GTK_STOCK_QUIT, 
					     _("_Don't save"));
  cancel_button = gael_create_dialog_button (GTK_STOCK_CANCEL,
					     _("D_on't close"));
  save_button   = gael_create_dialog_button (GTK_STOCK_SAVE,
					     _("_Save and close"));

  gtk_dialog_add_action_widget (GTK_DIALOG (dialog),
				quit_button,
				GTK_RESPONSE_NO);

  gtk_dialog_add_action_widget (GTK_DIALOG (dialog),
				cancel_button,
				GTK_RESPONSE_CANCEL);

  gtk_dialog_add_action_widget (GTK_DIALOG (dialog),
				save_button,
				GTK_RESPONSE_YES);

  gtk_widget_grab_focus (save_button);

  gtk_widget_show_all (dialog);
	
  ret = gtk_dialog_run (GTK_DIALOG (dialog));

  gtk_widget_destroy (dialog);
        
  switch (ret) {
  case GTK_RESPONSE_NO: /* Don't save */
    return TRUE;
    break;
  case GTK_RESPONSE_DELETE_EVENT:
  case GTK_RESPONSE_CANCEL: /* Don't quit */
    return FALSE;
    break;
  case GTK_RESPONSE_YES: /* Save and quit */
    return gael_do_save (window);
    break;
  default:
    g_assert_not_reached ();
  };

  return FALSE;
}

static gboolean
gael_do_save (GaelMainWindow *window)
{
  GaelMainWindowPriv *priv;
  const gchar        *uri = NULL;
	
  g_return_val_if_fail (GAEL_IS_MAIN_WINDOW (window), FALSE);

  priv = window->priv;
	
  uri = priv->design->uri; //mrp_project_get_uri (priv->project);

  if (uri == NULL) {
    return gael_do_save_as (window);
  } else {
    gchar *buffer;
    if( (buffer=gael_design_save (priv->design)) == NULL ) {
      GtkWidget *dialog;
			
      dialog = gtk_message_dialog_new (GTK_WINDOW (window),
				       GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
				       GTK_MESSAGE_ERROR,
				       GTK_BUTTONS_OK,
				       "error->message");
      gtk_dialog_run (GTK_DIALOG (dialog));
      gtk_widget_destroy (dialog);

      return FALSE;
    } else {
      g_message("%s(%i): implement the file save",__FILE__,__LINE__);
      gael_design_changed(priv->design,FALSE);
    }
  }
	
  return TRUE;
}

static gboolean
gael_do_save_as (GaelMainWindow *window)
{
  GaelMainWindowPriv *priv;
  GtkWidget        *file_sel;
  gint              response;
  const gchar      *filename = NULL;

  g_return_val_if_fail (GAEL_IS_MAIN_WINDOW(window), FALSE);

  priv = window->priv;

  file_sel = gtk_file_selection_new (_("Save a file"));

  gtk_window_set_modal (GTK_WINDOW (file_sel), TRUE);

  gtk_widget_show (file_sel);

  response = gtk_dialog_run (GTK_DIALOG (file_sel));

  if (response == GTK_RESPONSE_OK) {
    struct stat sb;

    filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (file_sel));
    if ((stat (filename, &sb) == 0) && S_ISDIR (sb.st_mode)) {
      filename = NULL;
    }
  }

  gtk_widget_destroy (file_sel);

  if (filename != NULL) {
    gchar *uri;
    gchar *buffer;

    /* Save the file. */
    uri = g_strconcat ("gael-1:", filename, NULL);
    buffer = gael_design_save(priv->design);

    if( buffer == NULL ) {
      g_message("%s(%i) TODO: error handling",__FILE__,__LINE__);
    } else {
      FILE *output;

      g_message("%s(%i) TODO: call the loaders to save",__FILE__,__LINE__);

      output = fopen(filename,"w");
      fwrite(buffer,sizeof(char),strlen(buffer),output);
      fclose(output);
      g_free(buffer);
      gael_design_changed(priv->design,FALSE);
    }

    g_free (uri);

    return TRUE;
  } else {
    return FALSE;
  }
}

static GtkWidget *
gael_create_dialog_button (const gchar *icon_name, const gchar *text)
{
  GtkWidget *button;
  GtkWidget *image;
  GtkWidget *label;
  GtkWidget *align;
  GtkWidget *hbox;

  button = gtk_button_new ();

  image = gtk_image_new_from_stock (icon_name, GTK_ICON_SIZE_BUTTON);
  label = gtk_label_new_with_mnemonic (text);

  gtk_label_set_mnemonic_widget (GTK_LABEL (label), GTK_WIDGET (button));
      
  hbox = gtk_hbox_new (FALSE, 2);

  align = gtk_alignment_new (0.5, 0.5, 0.0, 0.0);
      
  gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
      
  gtk_container_add (GTK_CONTAINER (button), align);
  gtk_container_add (GTK_CONTAINER (align), hbox);
  gtk_widget_show_all (align);

  return button;
}

GtkWidget*
gael_main_window_new(void)
{
  GaelMainWindow     *window;
  GaelMainWindowPriv *priv;
	
  window = GAEL_MAIN_WINDOW (g_object_new (GAEL_TYPE_MAIN_WINDOW, NULL));
  priv   = window->priv;
	
  gtk_window_set_default_size (GTK_WINDOW (window), 800, 600);

  g_signal_connect (window, "delete-event", 
		    G_CALLBACK (gael_window_delete_cb),
		    NULL);

  gael_populate (window);

  return GTK_WIDGET (window);
}

BonoboUIContainer *
gael_main_window_get_ui_container (GaelMainWindow *window)
{
  g_return_val_if_fail (GAEL_IS_MAIN_WINDOW (window), NULL);
	
  return window->priv->ui_container;
}

GaelLibrarian *
gael_main_window_get_librarian (GaelMainWindow  *window)
{
  return window->priv->librarian;
}

GaelViewInterface *
gael_main_window_get_view (GaelMainWindow  *window,
			   gchar           *name)
{
  return g_hash_table_lookup(window->priv->views,name);
}

GaelDesign *
gael_main_window_get_design (GaelMainWindow  *window)
{
  return window->priv->design;
}


void
gael_main_window_set_bonobo ( GaelMainWindow  *window,
			      gchar           *change,
			      gchar           *value )
{
  bonobo_ui_component_set_prop (window->priv->ui_component,
				"/commands/FileSave",
				change, value,
				NULL);
}
