/* gael-module.h
 * Copyright (C) 2002  Xavier Ordoquy
 *
 * This file is part of Gael.
 *
 * Gael is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gael is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gael; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef __GAEL_MODULE_H__
#define __GAEL_MODULE_H__

#include <gtk/gtk.h>
#include <gmodule.h>
#include <general/gael-view-interface.h>
#include <general/gael-model-interface.h>
#include <library/gael-library-element.h>

#define GAEL_TYPE_MODULE            (gael_module_get_type ())
#define GAEL_MODULE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GAEL_TYPE_MODULE, GaelModule))
#define GAEL_MODULE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  GAEL_TYPE_MODULE, GaelModuleClass))
#define GAEL_IS_MODULE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GAEL_TYPE_MODULE))
#define GAEL_IS_MODULE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  GAEL_TYPE_MODULE))
#define GAEL_MODULE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj),   GAEL_TYPE_MODULE, GaelModuleClass))

typedef struct _GaelModule      GaelModule;
typedef struct _GaelModuleClass GaelModuleClass;

struct _GaelModule
{
  GObject object;

  guchar             *path;

  GaelViewInterface      *(*get_new_view)  (void);
  GaelModelInterface     *(*get_new_model) (void);
  GaelLibraryElement     *(*get_new_elem)  (gchar *name,
					    gchar *path,
					    gchar *lib);
  const gchar            *(*get_name)      (void);
  const gchar            *(*get_name_i18n) (void);
  const gchar            *(*get_icon)      (void);

  GModule                *handle;
};

struct _GaelModuleClass
{
  GObjectClass parent_class;
};

GType  gael_module_get_type (void);

GaelModule*         gael_module_new       (guchar *path);

GaelViewInterface*      gael_module_get_new_view  (GaelModule *module);
GaelModelInterface*     gael_module_get_new_model (GaelModule *module);
GaelLibraryElement*     gael_module_get_new_elem  (GaelModule *module,
						   gchar      *name,
						   gchar      *path,
						   gchar      *lib);
const gchar*            gael_module_get_name      (GaelModule *module);
const gchar*            gael_module_get_name_i18n (GaelModule *module);
const gchar*            gael_module_get_icon      (GaelModule *module);

#endif
