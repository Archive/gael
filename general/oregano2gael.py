#!/usr/bin/env python2.2

import string, sys
import xml.dom.minidom

class symbol:
    def __init__(self,node):
        self.objects = []
        self.name = ''
        for subnode in node.childNodes:
            if subnode.nodeType == subnode.ELEMENT_NODE:
                if subnode.nodeName == 'ogo:name':
                    for subsubnode in subnode.childNodes:
                        self.name = subsubnode.toxml()
                elif subnode.nodeName == 'ogo:objects':
                    self.set_objects(subnode)
                elif subnode.nodeName == 'ogo:connections':
                    self.set_connection(subnode)
                else:
                    print 'unknown tag %s' % (subnode.nodeName)

    def set_connection(self,node):
        for subnode in node.childNodes:
            if subnode.nodeType == subnode.ELEMENT_NODE and \
                   subnode.nodeName == 'ogo:connection':
                for subsubnode in subnode.childNodes:
                    str = subsubnode.toxml()
                    str = string.strip(str)
                    str = str[1:]
                    str = str[:-1]
                    data = tuple(map(float,str.split ()))
                    self.objects.append(('connection',data))
                    #print '>'
                    #print self.name,str
                    #print '>'
                    #print self.name,data
                    #print '>'

    def set_objects(self,node):
        for subnode in node.childNodes:
            if subnode.nodeType == subnode.ELEMENT_NODE:
                if subnode.nodeName == 'ogo:line':
                    self.line(subnode.toxml())
                elif subnode.nodeName == 'ogo:arc':
                    self.arc(subnode.toxml())
                elif subnode.nodeName == 'ogo:text':
                    print 'do the text'
                    print subnode.toxml()
                else:
                    print 'unknown tag %s' % (subnode.nodeName)

    def arc(self,str):
        str = string.replace(str,'\n',' ')
        str = string.replace(str,'\r',' ')
        str = string.replace(str,'\t',' ')
        str = string.strip(str)
        str = string.replace(str,'<ogo:arc>','')
        str = string.replace(str,'</ogo:arc>','')
        str = string.strip(str)
        str = str[1:]
        str = str[:-1]
        while string.find(str,') ') != -1:
            pos = string.find(str,') ')
            str = str[:pos+1] + str[pos+2:]
        points = []
        for elem in string.split(str,')('):
            points.append(tuple(map(float,elem.split ())))
        points[0] = (points[0][0]+points[1][0]/2,points[0][1]+points[1][1]/2)
        points[1] = (points[1][0],points[1][1])
        self.objects.append(('arc',points))

    def line(self,str):
        str = string.replace(str,'\n',' ')
        str = string.replace(str,'\r',' ')
        str = string.replace(str,'\t',' ')
        str = string.strip(str)
        str = string.replace(str,'<ogo:line>','')
        str = string.replace(str,'</ogo:line>','')
        str = string.strip(str)
        str = str[1:]
        str = str[:-1]
        #print '>'
        #print self.name,str
        #print '>'
        while string.find(str,') ') != -1:
            pos = string.find(str,') ')
            str = str[:pos+1] + str[pos+2:]
        points = []
        for elem in string.split(str,')('):
            points.append(tuple(map(float,elem.split ())))
        self.objects.append(('multiline',points))

    def show(self):
        out = open(self.name+'.xml','w')
        out.write('<gael:symbol name="%s">\n' % (self.name))
        for obj in self.objects:
            if obj[0] == 'multiline':
                #print self.name,obj[1]
                for i in range(len(obj[1])-1):
                    out.write('\t<gael:line x1="%f" y1="%f" x2="%f" y2="%f"/>\n' % (obj[1][i][0],obj[1][i][1],obj[1][i+1][0],obj[1][i+1][1]))
            elif obj[0] == 'connection':
                out.write('\t<gael:connection x="%f" y="%f"/>\n' %(obj[1][0],obj[1][1]))
                out.write('\t<gael:attract x="%f" y="%f"/>\n' %(obj[1][0],obj[1][1]))
                out.write('\t<gael:attracted x="%f" y="%f"/>\n' %(obj[1][0],obj[1][1]))
            elif obj[0] == 'arc':
                out.write('\t<gael:arc filled="FALSE" x="%f" y="%f" width="%f" height="%f" angle1="0" angle2="360"/>\n' %(obj[1][0][0],obj[1][0][1],obj[1][1][0],obj[1][1][1]))
            else:
                print 'Unknown shape: %s' % (obj[0])
        out.write('</gael:symbol>\n')
        out.close()

    def get_name(self):
        return self.name

class library:
    def __init__(self):
        self.name   = ""
        self.symbol = {}

    def set_name(self,name):
        self.name = name

    def set_symbol(self,symbol):
        self.symbol[symbol.get_name] = symbol

    def show(self):
        out = open(self.name+'.xml','w')
        out.write('<gael:library name="%s">\n' % (self.name))
        out.write('\t<gael:summary>\n')
        for sym in self.symbol.values():
            out.write('\t\t<gael:Schematic name="%s"/>\n' % (sym.get_name()))
        out.write('\t</gael:summary>\n')
        out.write('</gael:library>\n')
        for sym in self.symbol.values():
            sym.show()

def read_lib():
    filename = sys.argv[1]
    lib = library()
    document = xml.dom.minidom.parse(filename)
    for node in document.documentElement.childNodes:
        if node.nodeType == node.ELEMENT_NODE and \
               node.nodeName == 'ogo:name':
            for subnode in node.childNodes:
                lib.set_name(subnode.toxml())
        if node.nodeType == node.ELEMENT_NODE and \
               node.nodeName == 'ogo:symbols':
            for subnode in node.childNodes:
                if subnode.nodeType == subnode.ELEMENT_NODE and \
                       subnode.nodeName == 'ogo:symbol':
                    sym = symbol(subnode)
                    lib.set_symbol(sym)

    document.unlink()
    lib.show()

if __name__ == '__main__':
    read_lib()
