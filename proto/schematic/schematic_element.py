#
# Copyright (C) 2002 Xavier Ordoquy <mcarkan@users.sourceforge.net>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.
#

class element:
    def __init__(self,view):
        self.view  = view
        self.group = view.canvas.root().add('GnomeCanvasGroup')

    #
    # Select segments and points within the area.
    #
    def select_area(self,area):
        pass

    #
    # Clears the selected segments/points
    #
    def unselect(self,position=None):
        pass

    #
    # Returns segments and points that are in the area
    #
    def is_on(self,area):
        pass

    #
    # outputs the wire data
    #
    def debug(self):
        pass

    #
    # Loads an element (override this)
    #
    def load(self):
        pass

    #
    # Saves an element (override this)
    #
    def save(self):
        pass

    #
    # Correspond to the button_press event.
    #
    def mouse_click(self,x,y):
        pass

    #
    # Event motion_move
    #
    def motion_move(self,x,y):
        pass
