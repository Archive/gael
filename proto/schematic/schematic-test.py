#!/usr/bin/env python
#
# Copyright (C) 2003 Xavier Ordoquy <mcarkan@users.sourceforge.net>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.
#

import pygtk; pygtk.require("2.0")
import commands
import gtk
import bonobo
import bonobo.ui

import sys
import ORBit
ORBit.load_typelib('/usr/lib/orbit-2.0/Gael_module')
#ORBit.load_typelib('Gael_module')

import CORBA
import GNOME__POA.Gael

orb = CORBA.ORB_init(sys.argv)
bonobo.activate()


class IDesign(GNOME__POA.Gael.IDesign, bonobo.UnknownBaseImpl):
    def __init__(self):
        bonobo.UnknownBaseImpl.__init__(self)
##         self.ref_count = 1

    def test(self):
        print 'test invoked'

##     def ref(self):
##         self.ref_count += 1
##         print 'ref:',self.ref_count

##         def ref(self):
##             bonobo.UnknownBaseImpl.ref(self)
##         def unref(self):
##             bonobo.UnknownBaseImpl.unref(self)

##     def unref(self):
##         self.ref_count -= 1
##         print 'unref:',self.ref_count
##         if self.ref_count == 0:
##             print 'Object should get destroyed !!'


def schematic_factory(factory, iid):
    design = IDesign()
    bobo = design.get_bonobo_object()
    return bobo


## ps = IDesign()
## def foo(*args):
##     print args
## listener = bonobo.Listener(foo)
## listener.add_interface(ps.get_bonobo_object())
## print listener.corba_objref().queryInterface("IDL:Bonobo/Unknown:1.0")


design = bonobo.GenericFactory("OAFIID:GNOME_Gael_Schematic_Factory",
                               schematic_factory)

bonobo.running_context_auto_exit_unref(design)
bonobo.main()
