#
# Copyright (C) 2004 Xavier Ordoquy <mcarkan@users.sourceforge.net>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.
#

class cursor:
    def __init__(self,view):
        self.radius = 5.0
        self.view = view
        self.cursor = self.view.canvas.root().add('GnomeCanvasEllipse',
                                             x1=self.radius, y1=self.radius,
                                             x2=-self.radius, y2=-self.radius,
                                             outline_color='black',
                                             width_units=1.0)

    #
    # Move the mouse cursor to (x,y)
    #
    def move(self,x,y):
        self.cursor.set_property('x1',x+self.radius)
        self.cursor.set_property('y1',y+self.radius)
        self.cursor.set_property('x2',x-self.radius)
        self.cursor.set_property('y2',y-self.radius)

    #
    # Translate (x,y) to the canvas coordinates
    #
    def w2i(self,x,y):
        return self.cursor.w2i(x,y)

    #
    # Translate (x,y) to the canvas coordinates with grid snapping
    #
    def w2g(self,x,y):
        x1, y1 = self.cursor.w2i(x,y)
        grid_spacing = self.view.grid_spacing
        zoom = self.view.zoom
        x1 = round(x1/zoom/grid_spacing)*grid_spacing
        y1 = round(y1/zoom/grid_spacing)*grid_spacing
        return (x1,y1)


class view(gtk.Table):

    def __init__(self):

        #
        # Here are defined some colors
        # FIXME: get the colors from the configuration
        #
        self.colors = {}
        self.colors['outline'] = 'black'
        self.colors['outline_selected'] = 'red'

        #
        # special canvas state (component placement, wire placement, ...)
        # default, first_wire_point, wire_point
        #
        self.canvas_mode  = 'default'

        #
        # This view is a table with a canvas and 2 scroll bars.
        # Of course it's also a bonobo component
        #
        gtk.Table.__init__(self,2,2)
        self.ui_comp = bonobo.ui.Component ('schematic-ui')
        self.grid_spacing  = 50.0
        self.current_item  = None
        self.selected      = []
        self.elements      = []

        # canvas
        self.zoom   = 1.0
        self.canvas = gnome.canvas.Canvas()
        #self.canvas.set_size_request(640,400)
        self.canvas.set_scroll_region(0,0,4096,4096)
        self.attach(self.canvas,
                    0,1,0,1,
                    gtk.EXPAND | gtk.FILL | gtk.SHRINK,
                    gtk.EXPAND | gtk.FILL | gtk.SHRINK,
                    0, 0)
        self.canvas.set_flags(gtk.CAN_FOCUS)
        #self.canvas.grab-focus()
        self.canvas.connect('motion_notify_event',  self.__event_motion_move)
        self.canvas.connect('button_press_event',   self.__event_button_press)
        self.canvas.connect('button_release_event', self.__event_button_release)

        # scrolls
        self.hscroll = gtk.HScrollbar(self.canvas.get_property('hadjustment'))
        self.attach(self.hscroll,
                    0,1,1,2,
                    gtk.FILL, gtk.FILL,
                    0, 0)
        self.vscroll = gtk.VScrollbar(self.canvas.get_property('vadjustment'))
        self.attach(self.vscroll,
                    1,2,0,1,
                    gtk.FILL, gtk.FILL,
                    0, 0)

        # create mouse cursor
        self.cursor = cursor(self)

        self.show_all()

    #
    # Called on view activation.
    # It merges the menu and toolbar.
    #
    def activate(self, ui_container):
        self.ui_comp.set_container (ui_container.corba_objref ())
        self.ui_comp.freeze()
        bonobo.ui.util_set_ui (self.ui_comp, gael.core.SHARE_PATH,
                               'GNOME_Gael_SchematicView.ui',
                               'schematic')
                #
        # Bind the command verbs from the BonoboUI-XML to action handlers.
        # The first argument is a list of (verb_name, action_handler) tuples.
        # The second argument is the window of this user interface.
        #
        self.ui_comp.add_verb_list([('ZoomIn',    self.__verb_zoom_in),
                                    ('ZoomOut',   self.__verb_zoom_out),
                                    ('ZoomToFit', self.__verb_zoom_to_fit),
                                    ('Zoom100',   self.__verb_zoom_100),
                                    ('Component', self.__verb_component),
                                    ('Wire',      self.__verb_wire),
                                    ('Debug',     self.__verb_debug)],
                                   self)
        self.ui_comp.thaw()

    #
    # Called on view deactivation
    # It removes the menu and toolbar previously merged
    #
    def deactivate(self, ui_container):
        self.ui_comp.rm('/')
        self.ui_comp.unset_container()

    #
    # When the mouse is moved we process the position to fit the grid
    # and move the objects.
    #
    def __event_motion_move(self, widget, event=None):
        if event == None or event.is_hint:
            return gtk.TRUE
        x, y = self.cursor.w2i(event.x,event.y)
        grid_spacing = self.grid_spacing
        gx = round(x/self.zoom/grid_spacing)*grid_spacing
        gy = round(y/self.zoom/grid_spacing)*grid_spacing

        if self.canvas_mode == 'wire_point':
            self.current_item.motion_move(gx,gy)
        elif self.canvas_mode == 'selection':
            x_init = self.current_item.get_data('initial_x')
            y_init = self.current_item.get_data('initial_y')
            if x < x_init:
                x1 = x
                x2 = x_init
            else:
                x1 = x_init
                x2 = x
            if y < y_init:
                y1 = y
                y2 = y_init
            else:
                y1 = y_init
                y2 = y
            self.current_item.set_property('x1',x1)
            self.current_item.set_property('y1',y1)
            self.current_item.set_property('x2',x2)
            self.current_item.set_property('y2',y2)
        else:
            pass

        self.cursor.move(gx,gy)
        return gtk.TRUE


    #
    # Whenever a button is released
    #
    def __event_button_release(self, widget, event):
        if self.canvas_mode == 'selection':
            #
            # We have finished our selection
            #

            #
            # Get the selection area
            #
            x1 = self.current_item.get_property('x1')
            y1 = self.current_item.get_property('y1')
            x2 = self.current_item.get_property('x2')
            y2 = self.current_item.get_property('y2')

            #
            # Remove the selection rectangle
            #
            self.current_item.destroy()
            self.current_item = None
            self.canvas_mode = 'default'

            for element in self.elements:
                if element.select_area((x1,y1,x2,y2)) == gtk.TRUE:
                    self.selected.append(element)


    #
    # Whenever a button is pressed
    #
    def __event_button_press(self, widget, event):

        if self.canvas_mode == 'default':
            #
            # We don't do anything special. This means the user tries to select
            # something.
            #

            #
            # First of all, deselect any selected item
            # FIXME: unselect only if shift key isn't pressed.
            #
            for sel in self.selected:
                sel.unselect()
            self.selected = []

            #
            # Get the cursor position
            #
            x, y = self.cursor.w2i(event.x,event.y)

            #
            # Create the selection rectangle
            #
            self.current_item = self.canvas.root().add('GnomeCanvasRect',
                                                       x1=x, y1=y,
                                                       x2=x, y2=y,
                                                       outline_color='black',
                                                       width_units=1.0)
            self.current_item.set_data('initial_x',x)
            self.current_item.set_data('initial_y',y)
            self.canvas_mode = 'selection'

        elif self.canvas_mode == 'first_wire_point':
            #
            # We are placing a line first point.
            #

            #
            # Get the cursor in the canvas coordinates after grid effect
            #
            x, y = self.cursor.w2g(event.x,event.y)

            #
            # Find if we are on a wire
            #
            best_wire = None
            for element in self.elements:
                if element.is_on((x,y)) != ([],[]):
                    best_wire = element
                    break

            if best_wire == None:
                #
                # We aren't on a wire so create a new one.
                #
                self.current_item = gael.schematic.wire(self)
                self.elements.append(self.current_item)
                self.current_item.mouse_click(x,y)
            else:
                #
                # We are on a wire so give it the mouse_click event.
                #
                self.current_item = best_wire
                self.current_item.mouse_click(x,y)

            self.canvas_mode = 'wire_point'
            return gtk.TRUE

        elif self.canvas_mode == 'wire_point':
            #
            # notice the change
            x, y = self.cursor.w2g(event.x,event.y)
            if self.current_item.mouse_click(x,y) == gtk.FALSE:
                # if it's at the same location, clean the current things
                self.canvas_mode = 'default'
                self.current_item = None
                #self.cursor = None
                #self.canvas.window.set_cursor(None)
            else:
                # Need to check whether or not it connects two different wires
                best_wire = None
                for element in self.elements:
                    if element != self.current_item and element.is_on((x,y)) != ([],[]):
                        best_wire = element
                        break
                if best_wire != None:
                    for i in range(len(self.elements)):
                        if self.elements[i] == best_wire:
                            del self.elements[i]
                            break
                    self.current_item.merge(best_wire)
            return gtk.TRUE

        return gtk.FALSE

    def print_(self):
        config = gnomeprint.config_default()
        config.set('Printer','GENERIC')
        job = gnomeprint.Job(config)
        job.print_to_file('output.ps')
        context = job.get_context()
        context.beginpage('1')
        for element in self.elements:
            element.print_(context)
        context.showpage()
        job.close()
        job.print_()


    def __verb_zoom_to_fit (self, uic, verbname, win):
        print 'ZoomToFit'

    def __verb_zoom_in (self, uic, verbname, win):
        win.zoom = win.zoom*1.2
        win.canvas.set_pixels_per_unit(win.zoom)

    def __verb_zoom_out (self, uic, verbname, win):
        win.zoom = win.zoom/1.2
        win.canvas.set_pixels_per_unit(win.zoom)

    def __verb_zoom_100 (self, uic, verbname, win):
        win.zoom = 1.0
        win.canvas.set_pixels_per_unit(win.zoom)

    def __verb_component (self, uic, verbname, win):
        print 'Component'

    def __verb_wire (self, uic, verbname, win):
        if win.canvas_mode == 'default':
            win.canvas_mode = 'first_wire_point'
            #win.mouse_cursor = gtk.gdk.Cursor(gtk.gdk.CROSSHAIR)
            #win.canvas.window.set_cursor(win.mouse_cursor)
        
    def __verb_debug (self, uic, verbname, win):
        for element in self.elements:
            element.debug()
