#!/usr/bin/env python
#
# Copyright (C) 2003 Xavier Ordoquy <mcarkan@users.sourceforge.net>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.
#

import bonobo
import Bonobo
 
bonobo.activate ()

## print '----------------------------------------'
## obj = bonobo.get_object ('OAFIID:GNOME_Gael_Schematic', 'Bonobo/Unknown')
## #print obj.get_bonobo_object().corba_objref().queryInterface("IDL:Bonobo/Unknown:1.0")
## print obj.queryInterface("IDL:Bonobo/Unknown:1.0")
## print '----------------------------------------'

## obj = bonobo.get_object ('OAFIID:GNOME_Gael_Schematic', 'Bonobo/Unknown')
obj = bonobo.get_object ('OAFIID:GNOME_Gael_Schematic', 'GNOME/Gael/IDesign')
obj.test()
obj.unref()
## obj.queryInterface("IDL:Bonobo/Unknown:1.0")
## obj.test()
## obj.ref()
## obj.test()
## obj.unref()
## obj.test()
