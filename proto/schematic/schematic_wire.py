#
# Copyright (C) 2002 Xavier Ordoquy <mcarkan@users.sourceforge.net>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.
#


#
# Utility function to make the top left corner at the
# first position.
#
def RECT((x1,y1,x2,y2)):
    if x1 > x2:
        x  = x1
        x1 = x2
        x2 = x
    if y1 > y2:
        y  = y1
        y1 = y2
        y2 = y
    return (x1,y1,x2,y2)


class wire(element):
    def __init__(self,view):
	element.__init__(self,view)
        self.segments          = []
        self.points            = []
        self.selected_segments = []
        self.selected_points   = []
        self.R = 5.0
        #
        # The 2 followings needs to be removed once code migration will be finished
        #
        self.curr_seg = None
        self.curr_point = None


    #
    # Clears the selected segments/points
    #
    def unselect(self,position=None):
        if position == None:
            for seg in self.selected_segments:
                seg.set_property('fill_color', self.view.colors['outline'])
            for pt in self.selected_points:
                pt.set_property('fill_color', self.view.colors['outline'])
            self.selected_segments = []
            self.selected_points   = []
        else:
            (segments,points) = self.is_on(position)
            for seg in segments:
                seg.set_property('fill_color', self.view.colors['outline'])
                for i in range(len(self.selected_segments)):
                    if seg == self.selected_segments[i]:
                        del self.selected_segments[i]
                        break
            for pt in points:
                pt.set_property('fill_color', self.view.colors['outline'])
                for i in range(len(self.selected_points)):
                    if seg == self.selected_points[i]:
                        del self.selected_points[i]
                        break


    #
    # Select segments and points within the area.
    #
    def select_area(self,area):
        if area[0] == area[2] and area[1] == area[3]:
            area = (area[0],area[1])
        (segments,points) = self.is_on(area)
        for seg in segments:
            self.selected_segments.append(seg)
            seg.set_property('fill_color', self.view.colors['outline_selected'])
        for pt in points:
            self.selected_points.append(pt)
            pt.set_property('fill_color', self.view.colors['outline_selected'])
        if segments != [] or points != []:
            return gtk.TRUE
        return gtk.FALSE
        

    #
    # Returns segments and points that are in the area
    #
    def is_on(self,area):
        selected_segments = []
        selected_points   = []
        #
        # Test if area is a point or an area
        #
        if len(area) == 2:
            #
            # area is just a point
            #
            x = area[0]
            y = area[1]
            #
            # Find matching segments
            #
            for seg in self.segments:
                points = seg.get_property('points')

                #
                # Check (x,y) is on the line
                #
                if ((points[2]-points[0])*(y-points[1])) == \
                   ((points[3]-points[1])*(x-points[0])):
                    #
                    # Now see if it's on the segent
                    #
                    if points[0] != points[2]:
                        g = (x-points[0])/(points[2]-points[0])
                    elif points[1] != points[3]:
                        g = (y-points[1])/(points[3]-points[1])
                    else:
                        g = 0
                    if (0 <= g) and (g <= 1):
                        selected_segments.append(seg)
            #
            # Now finds the matching points
            #
            for pt in self.points:
                if pt.get_property('x1') >= x and \
                   pt.get_property('y1') >= y and \
                   pt.get_property('x2') <= x and \
                   pt.get_property('y2') <= y:
                    selected_points.append(pt)

        elif len(area) == 4:
            #
            # area is an area
            # Find matching segments
            #
            for seg in self.segments:
                points = RECT(seg.get_property('points'))

                if points[0] >= area[0] and \
                   points[1] >= area[1] and \
                   points[2] <= area[2] and \
                   points[3] <= area[3]:
                    #
                    # The segment is within the area
                    #
                    selected_segments.append(seg)

            #
            # Now finds the matching points
            # Note that we take the center of the point as reference
            #
            for pt in self.points:
                (x,y) = ((pt.get_property('x1')+pt.get_property('x2'))/2,
                         (pt.get_property('y1')+pt.get_property('y2'))/2)
                if x >= area[0] and \
                   y >= area[1] and \
                   x <= area[2] and \
                   y <= area[3]:
                    #
                    # The point is within the area
                    #
                    selected_points.append(pt)

        else:
            pass
        
        return (selected_segments,selected_points)
        #
        # End of is_on
        #


    #
    # outputs the wire data
    #
    def debug(self):
        print '========== wire =========='
        print 'Segments:'
        for seg in self.segments:
            points = seg.get_property('points')
            print '  -',points
        print 'Points:'
        for point in self.points:
            x = (point.get_property('x1')+point.get_property('x2'))/2
            y = (point.get_property('y1')+point.get_property('y2'))/2
            print '  - (%f,%f)' % (x,y)
        print '===================='

    #
    # Prints the wire in the context
    #
    def print_(self,context):
        for point in self.points:
            x = (point.get_property('x1'))
            y = (point.get_property('y1'))
            width  = point.get_property('x2') - point.get_property('x1')
            height = point.get_property('y2') - point.get_property('y1')
            context.rect_filled(x,y,width,height)
        for seg in self.segments:
            points = seg.get_property('points')
            context.line_stroked(points[0],points[1],points[2],points[3])


    #
    # Loads a wire
    #
    def load(self):
        pass

    #
    # Saves a wire
    #
    def save(self):
        pass

    #
    # Merges two wires: self and other
    #
    def merge(self,other):
        for point in other.points:
            x = (point.get_property('x1')+point.get_property('x2'))/2
            y = (point.get_property('y1')+point.get_property('y2'))/2
            self.add_point(x,y)
        for seg in other.segments:
            points = seg.get_property('points')
            self.add_segment(points[0],points[1],points[2],points[3])
        if other.curr_point != None:
            print 'CURRENT POINT'
            x = (point.get_property('x1')+point.get_property('x2'))/2
            y = (point.get_property('y1')+point.get_property('y2'))/2
            self.curr_point = self.add_point(x,y)
            self.curr_seg   = self.group.add('GnomeCanvasLine',
                                             points=(x,y,x,y),
                                             fill_color=self.view.colors['outline'],
                                             width_units=1.0)
            other.curr_point.destroy()
        if other.curr_seg != None:
            other.curr_seg.destroy()
        for point in other.points:
            point.destroy()
        for seg in other.segments:
            seg.destroy()
        del other

    #
    # Correspond to the button_press event.
    #
    def mouse_click(self,x,y):
	# Get the wires we are over
        (segs,pts) = self.is_on((x,y))

	# Check wether we are at the beginning of a
	# segment or if it's already started
        if self.curr_seg != None:
            points = self.curr_seg.get_property('points')
            self.curr_seg.destroy()
            self.curr_seg = None
            if self.curr_point != None:
                self.curr_point = None
            if (points[0]==points[2]) and (points[1]==points[3]):
                return gtk.FALSE
            self.add_segment(points[0],points[1],points[2],points[3])
        else:
            self.curr_point = self.add_point(x,y)
        seg = self.group.add('GnomeCanvasLine',
                             points=(x,y,x,y),
                             fill_color=self.view.colors['outline'],
                             width_units=1.0)
        self.curr_seg = seg
        return gtk.TRUE

    #
    # Event motion_move
    #
    def motion_move(self,x,y):
        points = self.curr_seg.get_property('points')
        points[2] = x
        points[3] = y
        self.curr_seg.set_property('points',points)


    #
    # Adds a point and splits segments if needed
    # This shouldn't be called from outside wire.
    #
    def add_point(self,x,y):
        # If the point already exists, returns it
        for point in self.points:
            pos_x = (point.get_property('x1')+point.get_property('x2'))/2
            pos_y = (point.get_property('y1')+point.get_property('y2'))/2
            if (pos_x == x) and (pos_y == y):
                return point

        # Create the point
        point = self.group.add('GnomeCanvasRect',
                               x1 = x+self.R, y1 = y+self.R,
                               x2 = x-self.R, y2 = y-self.R,
                               fill_color=self.view.colors['outline'],
                               width_units=1.0)
        self.points.append(point)

        # Check that the point isn't on the segment
        (wires,pts) = self.is_on((x,y))
        # If it is, split the segment it's on on two sub segments
        for wire in wires:
            pos = wire.get_property('points')
            if ((pos[0] != x) or (pos[1] != y)) and ((pos[2] != x) or (pos[3] != y)):
                wire.set_property('points',(pos[0],pos[1],x,y))
                self.segments.append(self.group.add('GnomeCanvasLine',
                                                    points=(x,y,pos[2],pos[3]),
                                                    fill_color=self.view.colors['outline'],
                                                    width_units=1.0))
        return point

    def del_point(self,x,y):
        # Don't forget to merge segments splitted by the point !!!
        pass

    #
    # Adds a segment with the corresponding points.
    #
    def add_segment(self,x1,y1,x2,y2):
        self.segments.append(self.group.add('GnomeCanvasLine',
                                           points=(x1,y1,x2,y2),
                                           fill_color=self.view.colors['outline'],
                                           width_units=1.0))
        self.add_point(x1,y1)
        self.add_point(x2,y2)
