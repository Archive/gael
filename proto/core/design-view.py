#
# Copyright (C) 2003-2004 Xavier Ordoquy <mcarkan@users.sourceforge.net>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.
#

class view(bonobo.ui.Window):

    def __init__(self):

        #
        # Window creation
        #
        bonobo.ui.Window.__init__(self, "Gael", "Gael")
        self.connect("delete_event", self.__event_delete)
        self.set_default_size(400, 400)
        self.set_size_request(10, 10)  # make the window shrinkable
        self.show()

        #
        # A BonoboUI component takes a name in its constructor.
        # The 'set(filename, string)' method feeds the BonoboUI component with
        # either a XML file or with XML data contained in a string.
        #
        container = self.get_ui_container()
        component = bonobo.ui.Component("component")
        component.set_container(container.corba_objref())
        bonobo.ui.util_set_ui (component, SHARE_PATH,
                               'GNOME_Gael_DesignView.ui',
                               'Gael')

        #
        # Bind the command verbs from the BonoboUI-XML to action handlers.
        # The first argument is a list of (verb_name, action_handler) tuples.
        # The second argument is the window of this user interface.
        #
        component.add_verb_list([('FileNew',          self.__verb_new),
                                 ('FileOpen',         self.__verb_open),
                                 ('FileSave',         self.__verb_save),
                                 ('FileSaveAs',       self.__verb_saveas),
                                 ('FilePrintSetup',   self.__verb_print_setup),
                                 ('FilePrint',        self.__verb_print),
                                 ('FilePrintPreview', self.__verb_print_preview),
                                 ('FileClose',        self.__verb_close),
                                 ('FileExit',         self.__verb_exit),
 
                                 ('EditUndo',         self.__verb_undo),
                                 ('EditRedo',         self.__verb_redo),
                                 ('EditProjectProps', self.__verb_project_props),
                                 ('EditLibs',         self.__verb_libs),
 
                                 ('HelpAbout',        self.__verb_about)],
                                self)
        #
        # Now initialise the class members
        #
        self.current_view = None
        self.views        = []
        self.design       = design()
        self.librarian    = None

        #
        # Import the views
        #
        import gael.schematic
        view = gael.schematic.view()
        self.views.append(view)
        self.set_contents(view)
        view.activate(self.get_ui_container())


    #
    # Action handler for the "FileNew" verb.
    #
    def __verb_new(self, uic, verbname, win):
        print verbname

    #
    # Action handler for the "FileOpen" verb.
    #
    def __verb_open(self, uic, verbname, win):
        print verbname

        # Handles the OK button of the file selector
        def on_ok(button, self, fsel):
            print 'Loading %s' % (fsel.get_filename())
            fsel.destroy()

        # Handles the CANCEL button.
        def on_cancel(button, fsel):
            fsel.destroy()

        fsel = gtk.FileSelection("Open an Image:")
        fsel.ok_button.connect("clicked", on_ok, self, fsel)
        fsel.cancel_button.connect("clicked", on_cancel, fsel)
        fsel.show()

    #
    # Action handler for the "FileSave" verb.
    #
    def __verb_save(self, uic, verbname, win):
        print verbname

    #
    # Action handler for the "FileSaveAs" verb.
    #
    def __verb_saveas(self, uic, verbname, win):
        print verbname

    #
    # Action handler for the "FilePrintSetup" verb.
    #
    def __verb_print_setup(self, uic, verbname, win):
        print verbname

    #
    # Action handler for the "FilePrint" verb.
    #
    def __verb_print(self, uic, verbname, win):
        print verbname

    #
    # Action handler for the "FilePrintPreview" verb.
    #
    def __verb_print_preview(self, uic, verbname, win):
        print verbname

    #
    # Action handler for the "FileClose" verb.
    #
    def __verb_close(self, uic, verbname, win):
        sys.exit()

    #
    # Action handler for the "FileExit" verb.
    #
    def __verb_exit(self, uic, verbname, win):
        sys.exit()

    #
    # Action handler for the "EditUndo" verb.
    #
    def __verb_undo(self, uic, verbname, win):
        print verbname

    #
    # Action handler for the "EditRedo" verb.
    #
    def __verb_redo(self, uic, verbname, win):
        print verbname

    #
    # Action handler for the "EditProjectProps" verb.
    #
    def __verb_project_props(self, uic, verbname, win):
        print verbname

    #
    # Action handler for the "EditLibs" verb.
    #
    def __verb_libs(self, uic, verbname, win):
        print verbname

    #
    # Action handler for the "HelpAbout" verb.
    #
    def __verb_about(self, uic, verbname, win):
        about = gnome.ui.About(name='Gael',
                               version="??",
                               copyright='(C) 2002-2004 Xavier Ordoquy',
                               comments='An electronic design application for the GNOME desktop',
                               authors=['Xavier Ordoquy <mcarkan@users.sourceforge.net>'])
        about.show()


    #
    # Event handler for "delete_event"
    #
    def __event_delete(self,object,event):
        sys.exit()


if __name__ == '__main__':
    app = view()
    app.show_all()
    bonobo.main()
